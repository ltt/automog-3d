import numpy as np
import copy
import pandas as pd
from modeling.Hinging_Hyperplane.prepare_constraints import calc_convex_hull
from pathlib import Path
# contains data preprocessing methods for the op_opt_package

def generate_input_dict(model, instance, output_dir):
    # extract data from model
    products, input_params, components, curves, linear_sections, storages = \
        model.products, model.input_params, model.components, model.curves, model.sections, model.storages

    # extract data from instance
    params, cycles = copy.deepcopy(instance.params), instance.cycles
    
    # preprocess data and save to input dictionaries
    input_dict              = {}
    univariate_input_dict   = {}
    multivariate_input_dict = {}
    get_timesteps(input_dict, params, cycles)
    get_products(input_dict, params, products)
    get_ex_params(input_dict, params, input_params)
    get_components(input_dict, univariate_input_dict, multivariate_input_dict, 
                   components, curves, linear_sections) 
    get_storages(input_dict, storages)
    get_supplementary_input(input_dict, components, output_dir, with_init=False)
    
    return {None: input_dict}, univariate_input_dict, multivariate_input_dict

def get_timesteps(input_dict: dict, params: dict, cycles: int):
    # SETS: TIMESTEPS
    input_dict["TIMESTEPS"]      = {None: list(range(len(params)))}
    
    # PARAMS: previous_timestep
    input_dict['previous_timestep'] = {}
    T = len(input_dict["TIMESTEPS"][None])
    cycle_length = T // cycles
    for c in range(cycles+1):
        start = c*cycle_length
        if c < cycles:
            end   = (c+1)*cycle_length
        else:
            end = T
        timesteps          = list(range(start,end))
        previous_timesteps = [end-1] + list(range(start,end-1))
        for t,p in zip(timesteps, previous_timesteps):
            input_dict['previous_timestep'][t] = p  

    
def get_products(input_dict: dict, params: dict, products: dict):
    # SETS: PRODUCTS
    input_dict["PRODUCTS"]       = {None: list(products)}
    
    # PARAMS: has_source, has_sink, price, compensation, demand
    input_dict["has_source"]   = {product: products[product]['price'] != None 
                                        for product in products}
    input_dict["has_sink"]     = {product: products[product]['compensation'] != None 
                                        for product in products}
    
    for series in ["price", "compensation", "demand"]:
        input_dict[series]        = {}
        for product in products:
            if products[product][series] != None:
                # if timeseries is defined by multiple timeseries: sum over all series
                if type(products[product][series]) == list:
                    series_data = params[products[product][series][0]]
                    for col in products[product][series][1:]: 
                        series_data += params[col]
                # if timeseries consists only of one series
                else:
                    series_data = params[products[product][series]]
                for t, value in series_data.to_dict().items() :
                    if type(t) == int:
                        input_dict[series][product,t] =  value   

def get_ex_params(input_dict: dict,
                  params: dict, 
                  input_params: dict):
    # SETS: EX_PARAMS
    input_dict["EX_PARAMS"] = {None: [param 
                                      for param in input_params
                                      if input_params[param] in params.columns]}
    
    # PARAMS: ex_param 
    input_dict["ex_param"]  = {(ex_param,t): float(params[input_params[ex_param]][t])
                               for ex_param in input_dict["EX_PARAMS"][None]
                               for t in input_dict["TIMESTEPS"][None]}

def get_components(input_dict: dict, 
                   univariate_input_dict: dict, 
                   multivariate_input_dict: dict, 
                   components: dict, curves: dict, linear_sections: dict):
    
    # separate uni- and multivariate components
    subcomps_uni   = []
    subcomps_multi = []
    for subcomp in components:
        operating_points = components[subcomp].operating_points
        any_state = list(operating_points)[0]
        
        if len(operating_points[any_state].columns) == 2:
            subcomps_uni.append(subcomp)
        elif len(operating_points[any_state].columns) > 2:
            subcomps_multi.append(subcomp)
        else:
            raise ValueError(f'{subcomp}: Operating points must consist out of at least two columns.')
        
    # SETS: COMPONENTS, SUBCOMPONENT, SUBCOMPONENTS_UNIVARIATE, SUBCOMPONENTS_MULTIVARIATE, STATES, INPUT_PRODUCTS, OUTPUT_PRODUCTS
    # components (non-indexed)
    comps = {None:list(set([comp.split('_')[0] 
                            for comp in components]))}
    
    # subcomponents (indexed)
    subcomps = {comp: [subcomp 
                       for subcomp in components
                       if comp in subcomp]
                for comp in comps[None]}
    
    # univariate subcomponents (indexed)
    subcomps_uni = {comp: [subcomp 
                           for subcomp in subcomps_uni
                           if comp in subcomp]
                    for comp in comps[None]}
    
    # multivariate subcomponents (indexed)
    subcomps_multi = {comp: [subcomp 
                             for subcomp in subcomps_multi
                             if comp in subcomp]
                      for comp in comps[None]}

    # operating states, input products, input params and output products (double-indexed)
    states          = {}
    input_products  = {}
    input_params    = {}
    output_products = {}
    
    for comp in comps[None]:
        for subcomp in subcomps[comp]:
            subcomp_object                = components[subcomp]
            operating_points              = subcomp_object.operating_points
            states[comp,subcomp]          = list(operating_points)
            input_products[comp,subcomp]  = list(set(subcomp_object.inputs)-set(subcomp_object.params))
            input_params[comp,subcomp]    = subcomp_object.params
            output_products[comp,subcomp] = subcomp_object.outputs
            
    input_dict["COMPONENTS"]                 = comps 
    input_dict["SUBCOMPONENTS"]              = subcomps
    input_dict["SUBCOMPONENTS_UNIVARIATE"]   = subcomps_uni
    input_dict["SUBCOMPONENTS_MULTIVARIATE"] = subcomps_multi 
    input_dict["STATES"]                     = states
    input_dict["INPUT_PRODUCTS"]             = input_products
    input_dict["INPUT_PARAMS"]               = input_params
    input_dict['OUTPUT_PRODUCTS']            = output_products
    
    # univariate components: domain_pts, range_pts
    domain_pts = {}
    range_pts  = {}
    
    # multivariate components: ... 
    var_mapper         = {}
    hull_elements      = {}
    input_dim          = {}
    num_lin_el         = {}
    coefficients       = {}
    boundary_equations = {}
    boundary_sign      = {}
    glover_lower       = {}
    glover_upper       = {}
    
    for subcomp in components:
        comp = subcomp.split('_')[0]
        
        operating_points = components[subcomp].operating_points
        any_state = list(operating_points)[0]
        
        # check for univariety
        if len(operating_points[any_state].columns) == 2:  
            for state in curves[subcomp]:
                sections = linear_sections[subcomp][state]
                breakpoints = curves[subcomp][state][sections]['v']
                
                for time_step in input_dict["TIMESTEPS"][None]:
                    domain_pts[comp, subcomp, state, time_step] = [0]
                    range_pts[comp, subcomp, state, time_step]  = [0]
                    
                    for domain_pt,range_pt in breakpoints.items():
                        domain_pts[comp, subcomp, state, time_step].append(domain_pt)
                        range_pts[comp, subcomp, state, time_step].append(range_pt)
        
        # check for multivariety
        elif len(operating_points[any_state].columns) > 2:
            # variable mapper
            input_counter  = 0
            # iterate over outputs
            for c in operating_points[any_state]:
                if c in output_products[comp,subcomp]:
                    var_mapper[comp, subcomp, c] = ('input',input_counter)
                    input_counter += 1
                    
            output_found = False
            # iterate over inputs
            for c in operating_points[any_state]:    
                if c in input_products[comp,subcomp]+input_params[comp,subcomp]:
                    if not output_found:
                        var_mapper[comp, subcomp, c] = ('output',0)
                        output_found=True
                    else:
                        var_mapper[comp, subcomp, c] = ('input',input_counter)
                        input_counter += 1
                                        
            # for all operating states
            for state in curves[subcomp]:
                
                # get data from automog
                sections = linear_sections[subcomp][state]
                model    = copy.deepcopy(curves[subcomp][state][sections]['model'])
                
                # calculate convex hull
                calc_convex_hull(model)

                # prepare optimization input
                hull_elements[comp,subcomp,state] = model.milp.hull_lin_elements

                if type(model.data) != list:
                    model.data = [model.data]
                input_dim[comp,subcomp,state]     = len(model.data[0]['X'][0,:])
                
                num_lin_el[comp,subcomp,state]    = linear_sections[subcomp][state]
                
                if type(model.theta) != list:
                    model.theta = [model.theta]
                coefficients[comp,subcomp,state]  = model.theta
                
                # calculate boundary equations
                boundary_equations[comp,subcomp,state] = {}
                for key in hull_elements[comp,subcomp,state]:
                    boundary_equations[comp,subcomp,state][key] = np.zeros(
                        (len(hull_elements[comp,subcomp,state][key].equations[:, 0]), 
                         input_dim[comp,subcomp,state]))
                    count = 0
                    for i in range(len(hull_elements[comp,subcomp,state][key].equations[0, :])):
                        if i != input_dim[comp,subcomp,state] - 1:
                            boundary_equations[comp,subcomp,state][key][:, count] = -np.divide(
                                hull_elements[comp,subcomp,state][key].equations[:, i],
                                hull_elements[comp,subcomp,state][key].equations[:,
                                input_dim[comp,subcomp,state] - 1])
                            count += 1
                  
                # calculate boundary sign
                boundary_sign[comp,subcomp,state] = {}
                for key in boundary_equations[comp,subcomp,state]:
                    mean_input = np.mean(hull_elements[comp,subcomp,state][key].points, axis=0)
                    boundary_sign[comp,subcomp,state][key] = []
                    for k in range(len(boundary_equations[comp,subcomp,state][key][:, 0])):
                        if mean_input[-1] <= (boundary_equations[comp,subcomp,state][key][k, -1] + mean_input[:-1].dot(
                                boundary_equations[comp,subcomp,state][key][k, :-1])):
                            # upper boundary
                            boundary_sign[comp,subcomp,state][key].append(1)
                        else:
                            boundary_sign[comp,subcomp,state][key].append(-1)
                            
                # calculate glover upper and lower
                glover_lower[comp,subcomp,state] = {}
                glover_upper[comp,subcomp,state] = {}
                for lin_el in model.milp.points:
                    glover_lower[comp,subcomp,state][lin_el] = np.min(model.milp.points[key]['X'], axis=0)
                    glover_upper[comp,subcomp,state][lin_el] = np.max(model.milp.points[key]['X'], axis=0)
                                     
    # write data dicts
    # univariate
    univariate_input_dict['domain_pts'] = domain_pts
    univariate_input_dict['range_pts']  = range_pts
    # multivariate
    multivariate_input_dict['var_mapper']         = var_mapper
    multivariate_input_dict['hull_elements']      = hull_elements 
    multivariate_input_dict['input_dim']          = input_dim   
    multivariate_input_dict['num_lin_el']         = num_lin_el    
    multivariate_input_dict['coefficients']       = coefficients   
    multivariate_input_dict['boundary_equations'] = boundary_equations
    multivariate_input_dict['boundary_sign']      = boundary_sign     
    multivariate_input_dict['glover_lower']       = glover_lower    
    multivariate_input_dict['glover_upper']       = glover_upper  
        
def get_storages(input_dict: dict, storages: dict):
    # SETS: STORAES
    input_dict["STORAGES"] = {None: list(storages.keys())}
    
    # PARAMS: s. .optimization.operational_optimization.setupParametersStorages
    if len(storages) >0:
        for attr in storages[list(storages.keys())[0]]:
            if not attr in input_dict:
                input_dict[attr] = {}
                for s in storages:
                    input_dict[attr][s] = storages[s][attr] 

def get_supplementary_input(input_dict: dict, components: dict, output_dir: str, with_init: bool):
    # check which features can be read
    match = {'ramping': (['rr'],1),
             'startup': (['sc'],1),
             'updowntimes': (['mdt', 'mut'],1),
             'revision': (['r_length', 'nr_length'],1)}
    
    for cat in match:
        for comp_name, comp in components.items():
            for data in match[cat][0]:
                if not data in comp.opt_params:
                    match[cat] = (match[cat][0], 0)
                    break
            if match[cat][1] == 0:
                break
            
    # read features into input_dict
    # implicit-built elements
    input_dict['REVISIONS'] = {}

    for cat in match:
        # check if features is available
        if match[cat][1]:
            for p in match[cat][0]:
                input_dict[p] = {}
                for comp in input_dict['COMPONENTS'][None]:
                    # take any subcomponent --> all have the same parameters
                    subcomp = input_dict['SUBCOMPONENTS'][comp][0]
                    if cat == 'ramping' and p == 'rr':
                        for param in components[subcomp].opt_params['rr']:
                                input_dict['rr'][comp,param] = components[subcomp].opt_params['rr'][param]
# =============================================================================
#                         if with_init:
#                             if comp != 'CC1' and comp != 'CC2':
#                                 for param in components[subcomp].opt_params['rr']:
#                                     input_dict['rr'][comp,param] = components[subcomp].opt_params['rr'][param]
#                         else:
#                             for param in components[subcomp].opt_params['rr']:
#                                 input_dict['rr'][comp,param] = components[subcomp].opt_params['rr'][param]
# =============================================================================
                    elif cat == 'revision' and p == 'r_length':
                        input_dict['REVISIONS'][comp] =  []
                        for rev,length in enumerate(components[subcomp].opt_params['r_length']):
                            input_dict['REVISIONS'][comp].append(rev)
                            input_dict['r_length'][comp,rev] = length     
                    else:
                        input_dict[p][comp] = components[subcomp].opt_params[p]
  
    #Add initialization parameters
# =============================================================================
#     if with_init:
#         output_dir_path=Path(output_dir)
#         working_dir=output_dir_path.parents[2]
#         init_param_df = pd.read_excel(working_dir / 'init_values.xlsx', header=0, index_col=0)
#         storage_init_param_df=pd.read_excel(working_dir / 'storage_init_values.xlsx', header=0, index_col=0)
# 
#         for param in init_param_df.columns:
#             input_dict[param]={}
#         for param in storage_init_param_df.columns:
#             input_dict[param]={}         
#     
#     
#         for comp in input_dict['COMPONENTS'][None]:
#             
#             if init_param_df['active_init'][comp] or init_param_df['active_init'][comp]==0:
#                 input_dict['active_init'][comp] = init_param_df['active_init'][comp]
#                 
#             if init_param_df['state_active_init'][comp] or init_param_df['state_active_init'][comp]==0:
#                 z=eval(init_param_df['state_active_init'][comp])
#                 for subcomp in z.keys():
#                     for state in z[subcomp].keys():
#                         input_dict['state_active_init'][comp,subcomp,state] = z[subcomp][state]
#                 
#             if init_param_df['input_init'][comp] or init_param_df['input_init'][comp]==0 :
#                 x=eval(init_param_df['input_init'][comp])
#                 for param in x.keys():
#                     input_dict['input_init'][comp,param] = x[param]
#                     
#             if init_param_df['output_init'][comp] or init_param_df['output_init'][comp]==0:
#                 y=eval(init_param_df['output_init'][comp])
#                 for param in y.keys():
#                     input_dict['output_init'][comp,param] = y[param]
#                     
#             if init_param_df['dt_init'][comp] or init_param_df['dt_init'][comp]==0:
#                 input_dict['dt_init'][comp] = init_param_df['dt_init'][comp]   
#                 
#             if init_param_df['ut_init'][comp] or init_param_df['ut_init'][comp]==0:
#                 input_dict['ut_init'][comp] = init_param_df['ut_init'][comp]
#        
#         for storage in input_dict['STORAGES'][None]:
#             if storage_init_param_df['vol_init'][storage] or storage_init_param_df['vol_init'][storage]==0:
#                 input_dict['vol_init'][storage] = storage_init_param_df['vol_init'][storage]
# =============================================================================
    #first timestep (needed for later process in Machine Learning Alg)
    #TODO: Proper prarameter without storage
            #input_dict['first_TS']={}
            #input_dict['first_TS'][storage]=0
    # check feasibility of revision scheduling a-priori
    # TODO: implement autoscaling of revision slots
    # T = len(input_dict["TIMESTEPS"][None])
    # r_total = 0
    # for comp in input_dict['COMPONENTS'][None]:
    #     if match['revision'][1]:
    #         nr_length_total = (len(input_dict['REVISIONS'][comp])-1) * input_dict['nr_length'][comp]
    #         r_length_total = sum(input_dict['r_length'][comp,rev] 
    #                              for rev in input_dict['REVISIONS'][comp])
    #         r_total_current = r_length_total + nr_length_total 
    #         if r_total_current > r_total:
    #             r_total = r_total_current
                
    #         assert r_total <= T, f'Current time horizon ({T} timesteps) is too short. At least {r_total} timesteps are needed.'
    