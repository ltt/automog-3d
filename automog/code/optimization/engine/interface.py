import pyomo.environ as pe
import pickle
import os

class OperationalOptimizationInterface(object):
    def __init__(self, model, instance, 
                 solver = 'glpk', 
                 solver_options: dict = None,
                 config: dict = None):
        
        self.model    = model
        self.instance = instance
        self.solver   = solver
        self.solver_options = solver_options
        self.config = config
        
    def optimize(self, output_dir):
        from .data_processing import generate_input_dict
        from .optimization import operational_optimization

        # preprocess data
        input_dict, univariate_input_dict, multivariate_input_dict = generate_input_dict(model=self.model, instance=self.instance, output_dir=output_dir)

        if output_dir:
            with open(output_dir + '/input_dict.pickle', 'wb') as f:
                pickle.dump((input_dict, univariate_input_dict, multivariate_input_dict), f)
                
        # create model and solve it
        OperationalOptimization = operational_optimization(config = self.config,
                                                           solver = self.solver,
                                                           univariate_input_dict = univariate_input_dict, 
                                                           multivariate_input_dict = multivariate_input_dict)
        
        OperationalOptimization.run(input_dict     = input_dict, 
                                    solver         = self.solver, 
                                    solver_options = self.solver_options,
                                    output_dir     = output_dir,
                                    )
        
        # get results
        self.results = {}
        
        for attr in dir(OperationalOptimization.model_instance):
            # skip indices
            if not '_index' in attr:
                component = getattr(OperationalOptimization.model_instance, attr)
                
                # get sets
                if isinstance(component, pe.Set):
                    if component.is_indexed():
                        self.results[attr] = {}
                        for idx in component:
                            self.results[attr][idx] = list(component[idx])
                    else:
                        self.results[attr] = list(component)
                # get variable values
                elif isinstance(component, pe.Var):
                    self.results[attr] = OperationalOptimization.get_dataframe_from_result(component)
                # get parameter values
                elif isinstance(component, pe.Param):
                    self.results[attr] = OperationalOptimization.get_dataframe_from_parameter(component)
                # get objective (nothing happens so far)
                elif isinstance(component, pe.Objective):
                    pass
                # get constraints (nothing happens so far)
                elif isinstance(component, pe.Constraint):
                    pass

        # get objective value
        self.results['obj']  = OperationalOptimization.get_objective()
        
        # get solution time
        self.results['time'] = OperationalOptimization.get_solution_time()
        
# =============================================================================
#     def create_pyomo_model(self, output_dir, abstract=True):
#         from .data_processing import generate_input_dict
#         from .optimization_with_Init import operational_optimization
#         
#         # preprocess data
#         self.input_dictionary, self.input_dictionary_univariate, self.input_dictionary_multivariate = generate_input_dict(self.model, 
#                                                                                           self.instance, output_dir=output_dir)
# 
#                 
#         
#         # create model and return it with input dictionary
#         OperationalOptimization = operational_optimization(config = self.config,
#                                                            solver = 'gurobi',
#                                                            univariate_input_dict=self.input_dictionary_univariate, 
#                                                            multivariate_input_dict=self.input_dictionary_multivariate)
#         if abstract==True:
#             return OperationalOptimization.model, self.input_dictionary
#         
#         OperationalOptimization.instantiate(self.input_dictionary)
#         instance=OperationalOptimization.model_instance
#         
#         return instance, self.input_dictionary
# =============================================================================

