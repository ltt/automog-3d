import pyomo.environ as pe
import pyomo.core.base.piecewise as pw
from pyomo.opt import TerminationCondition
from pyomo.util.infeasible import log_infeasible_constraints
import pandas as pd
import sys
import logging
import dill as pickle

class operational_optimization:
    
    def __init__(self, config: dict, solver:str, 
                 univariate_input_dict: dict   = {}, 
                 multivariate_input_dict: dict = {}):
        
        """This method initializes the model as an abstract pyomo model."""
 
        self.solver = solver
        
        global CONFIG 
        CONFIG = config
        self.config = config
        
        self.model = pe.AbstractModel()
        self.setupModelAttributes(univariate_input_dict, multivariate_input_dict)
        self.setupSets()
        self.setupParameters()
        self.setupVariables()
        self.setupObjective()
        self.setupConstraints()
        
    def setupModelAttributes(self, univariate_input_dict: dict, multivariate_input_dict: dict):
        for input_dict in [univariate_input_dict, 
                           multivariate_input_dict]:
            for attr in input_dict:
                setattr(self.model,attr,input_dict[attr])
        
    def setupSets(self):
        """This method sets up all sets required by all optimization models."""
        
        self.setupSetsGeneral()
        self.setupSetsComponentsUnivariate()
        self.setupSetsComponentsMultivariate()
        self.setupSetsComponentsSupplement()
        
    def setupSetsGeneral(self):
        """ This method sets up sets required by all optimization models. """
        
        ### GENERAL
        self.model.PRODUCTS = pe.Set(
            doc="set of used products"
            )
        self.model.TIMESTEPS = pe.Set(
            within=pe.NonNegativeIntegers,
            doc="set of timesteps"
            )
        self.model.EX_PARAMS = pe.Set(
            doc="set of exogenous parameters")
        ### PRODUCTION COMPONENTS
        # components
        self.model.COMPONENTS = pe.Set(
            doc="set of available conversion components"
            )
        self.model.SUBCOMPONENTS = pe.Set(
            self.model.COMPONENTS,
            doc="set of available subcomponents per component"
            ) 
        self.model.IDX_COMPONENTS_SUBCOMPONENTS = pe.Set(
            dimen=2,
            initialize=self.init_idx_components_subcomponents,
            doc="tuple index: (component,sucomponent)"
            )
        # states
        self.model.STATES = pe.Set(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS,
            doc="set of available operating states per (component,sucomponent)-tuple"
            )
        self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES = pe.Set(
            dimen=3,
            initialize=self.init_idx_components_subcomponents_states,
            doc="triple index: (component,subcomponent,state)"
            )
        # output and input products/params
        self.model.INPUT_PRODUCTS = pe.Set(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS,
            within=self.model.PRODUCTS,
            doc="set of input products for each (component, subcomponent)-tuple"
            )
        self.model.INPUT_PARAMS = pe.Set(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS,
            doc="set of input parameters (non-product-based-inputs) for each (component,subcomponent)-tuple"
            )
        self.model.OUTPUT_PRODUCTS = pe.Set(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS,
            within=self.model.PRODUCTS,
            doc="set of output products for each (component, subcomponent)-tuple"
            )
        
        self.model.IDX_COMPONENTS_INPUTS = pe.Set(
            dimen=2,
            initialize=self.init_idx_components_input,
            doc="tuple index: (component,input)")
        
        self.model.IDX_COMPONENTS_OUTPUTS = pe.Set(
            dimen=2,
            initialize=self.init_idx_components_output,
            doc="tuple index: (component,output)"
            )
        
        self.model.IDX_COMPONENTS_SUBCOMPONENTS_INPUTS = pe.Set(
            dimen=3,
            initialize=self.init_idx_components_subcomponents_input,
            doc="triple index: (component,subcomponent,input)")
        
        self.model.IDX_COMPONENTS_SUBCOMPONENTS_OUTPUTS = pe.Set(
            dimen=3,
            initialize=self.init_idx_components_subcomponents_output,
            doc="triple index: (component,subcomponent,output)"
            )
        
        
        ### STORAGE COMPONENTS
        self.model.STORAGES = pe.Set(
            initialize=[],
            doc="set of storages")
        
    def setupSetsComponentsUnivariate(self):
        """ This method sets up sets required by all optimization models 
            containing univariate components. """
            
        self.model.SUBCOMPONENTS_UNIVARIATE = pe.Set(
            self.model.COMPONENTS
            )
        self.model.IDX_COMPONENTS_SUBCOMPONENTS_UNIVARIATE = pe.Set(
            dimen=2,
            initialize=self.init_idx_components_subcomponents_univariate,
            doc="tuple index: (component,sucomponent) for univariate components"
            )
        
        self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_UNIVARIATE = pe.Set(
            dimen=3,
            initialize=self.init_idx_components_subcomponents_states_univariate,
            doc="triple index: (component, subcomponent, state) for univariate components"
            )
    
    def setupSetsComponentsMultivariate(self):
        """ This method sets up sets required by all optimization models 
            containing multivariate components. """
            
        self.model.SUBCOMPONENTS_MULTIVARIATE = pe.Set(
            self.model.COMPONENTS
            )
        self.model.IDX_COMPONENTS_SUBCOMPONENTS_MULTIVARIATE = pe.Set(
            dimen=2,
            initialize=self.init_idx_components_subcomponents_multivariate,
            doc="tuple index: (component,sucomponent) for univariate components"
            )
        
        # create triple index: (component, subcomponent, state)
        self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_MULTIVARIATE = pe.Set(
            dimen=3,
            initialize=self.init_idx_components_subcomponents_states_multivariate
            )
        
        self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_MULTIVARIATE_LIN_EL = pe.Set(
            dimen=4,
            initialize=self.init_idx_components_subcomponents_states_multivariate_lin_el
            )
        
        self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_MULTIVARIATE_LIN_EL_INPUT = pe.Set(
            dimen=5,
            initialize=self.init_idx_components_subcomponents_states_multivariate_lin_el_input
            )
        
        self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_MULTIVARIATE_LIN_EL_BOUNDARIES = pe.Set(
            dimen=5,
            initialize=self.init_idx_components_subcomponents_states_multivariate_lin_el_boundaries
            )
        
        self.model.IDX_COMPONENTS_SUBCOMPONENTS_MULTIVARIATE_INPUT_PARAMS = pe.Set(
            dimen=3,
            initialize=self.init_idx_components_subcomponents_multivariate_input_params)
    
    def setupSetsComponentsSupplement(self):
        """ This method sets up sets required by optimization models 
            containing supplementary features. """
            
        if CONFIG['revision']:
            # set of revisions
            self.model.REVISIONS = pe.Set(
                self.model.COMPONENTS
                )
            
            # create tuple index: (component, revision)
            self.model.IDX_COMPONENTS_REVISIONS = pe.Set(
                dimen=2,
                initialize=self.init_idx_components_revisions
                )
        
    def setupParameters(self):
        """This method sets up all parameters required by all optimization models."""
        
        self.setupParametersGeneral()
        self.setupParametersComponentsSupplement()
        self.setupParametersStorages()
        
    def setupParametersGeneral(self):
        # time coupling parameters
        self.model.previous_timestep = pe.Param(
            self.model.TIMESTEPS,
            within=self.model.TIMESTEPS)
        ### product specific parameters
        # has source
        self.model.has_source = pe.Param(
            self.model.PRODUCTS,
            within=pe.Binary,
            default=0)
        # has sink 
        self.model.has_sink = pe.Param(
            self.model.PRODUCTS,
            within=pe.Binary,
            default=0)
        # source price timeseries
        self.model.price = pe.Param(
            self.model.PRODUCTS,
            self.model.TIMESTEPS,
            within=pe.NonNegativeReals,
            default=0)
        # sink compensation timeseries
        self.model.compensation = pe.Param(
            self.model.PRODUCTS,
            self.model.TIMESTEPS,
            within=pe.NonNegativeReals,
            default=0)
        # demand timeseries for all products
        self.model.demand = pe.Param(
            self.model.PRODUCTS,
            self.model.TIMESTEPS,
            within=pe.NonNegativeReals,
            default=0)
        
        # parameter timeseries for all exogenous parameters
        self.model.ex_param = pe.Param(
            self.model.EX_PARAMS,
            self.model.TIMESTEPS,
            within=pe.Reals)
        
    def setupParametersComponentsSupplement(self):
        """ parameters for more detailed modeling """
        
        if CONFIG['ramping']:
            # Maximum ramp-rate
            self.model.rr = pe.Param(
                self.model.IDX_COMPONENTS_INPUTS | self.model.IDX_COMPONENTS_OUTPUTS,
                within=pe.NonNegativeReals,
                default=float("inf"))
        
        if CONFIG['updowntimes']:
            # Minimum downtime
            self.model.mdt = pe.Param(
                self.model.COMPONENTS,
                within=pe.NonNegativeReals,
                default=0)
            # Minimum uptime
            self.model.mut = pe.Param(
                self.model.COMPONENTS,
                within=pe.NonNegativeReals,
                default=0)
            
        if CONFIG['startup']:
            # Startcost
            self.model.sc = pe.Param(
                self.model.COMPONENTS,
                within=pe.NonNegativeReals,
                default=0)
            
        if CONFIG['revision']:
            # Revision length per unit and revision
            self.model.r_length = pe.Param(
                self.model.IDX_COMPONENTS_REVISIONS,
                within=pe.NonNegativeIntegers,
                default=0)
            # Time between revisions per unit
            self.model.nr_length = pe.Param(
                self.model.COMPONENTS,
                within=pe.NonNegativeIntegers,
                default=0)
    
    def setupParametersStorages(self):
        """ storage parameters """
        # Maximum storage fillevel
        self.model.max_vol = pe.Param(
            self.model.STORAGES,
            within=pe.NonNegativeReals,
            default=float("inf"))
        # Minimum storage filllevel
        self.model.min_vol = pe.Param(
            self.model.STORAGES,
            within=pe.NonNegativeReals,
            default=0)
        # Maximum storage loading
        self.model.max_load = pe.Param(
            self.model.STORAGES,
            within=pe.NonNegativeReals,
            default=float("inf"))
        # Maximum storage unloading
        self.model.max_unload = pe.Param(
            self.model.STORAGES,
            within=pe.NonNegativeReals,
            default=float("inf"))
        # storage loading efficiency
        self.model.load_eff = pe.Param(
            self.model.STORAGES,
            within=pe.NonNegativeReals,
            default=1)
        # storage unloading efficiency
        self.model.unload_eff = pe.Param(
            self.model.STORAGES,
            within=pe.NonNegativeReals,
            default=1)
        # storage product
        self.model.storage_product = pe.Param(
            self.model.STORAGES,
            within=self.model.PRODUCTS)
    
    def setupVariables(self):
        self.setupVariablesProduct()
        self.setupVariablesComponents()
        self.setupVariablesComponentsUnivariate()
        self.setupVariablesComponentsMultivariate()
        self.setupVariablesComponentsSupplement()
        self.setupVariablesStorages()

    def setupVariablesProduct(self):
        # sink use
        self.model.USED_SINK = pe.Var(
            self.model.PRODUCTS,
            self.model.TIMESTEPS,
            within=pe.NonNegativeReals)
        # source use
        self.model.USED_SOURCE = pe.Var(
            self.model.PRODUCTS,
            self.model.TIMESTEPS,
            within=pe.NonNegativeReals)
        # non-served demand
        self.model.NON_SERVED_DEMAND = pe.Var(
            self.model.PRODUCTS,
            self.model.TIMESTEPS,
            within=pe.NonNegativeReals)
    
    def setupVariablesComponents(self):
        # component active
        self.model.ACTIVE = pe.Var(
            self.model.COMPONENTS,
            self.model.TIMESTEPS,
            within=pe.Binary)
        # component state active
        self.model.STATE_ACTIVE = pe.Var(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES,
            self.model.TIMESTEPS,
            within=pe.Binary)
        # component output
        self.model.OUTPUT = pe.Var(
            self.model.IDX_COMPONENTS_OUTPUTS,
            self.model.TIMESTEPS,
            within=pe.NonNegativeReals)
        # component input
        self.model.INPUT = pe.Var(
            self.model.IDX_COMPONENTS_INPUTS,
            self.model.TIMESTEPS,
            within=pe.NonNegativeReals)
        
    def setupVariablesComponentsUnivariate(self):
        # subcomponent output
        self.model.OUTPUT_SUB_UNIVARIATE = pe.Var(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_UNIVARIATE,
            self.model.TIMESTEPS,
            within=pe.NonNegativeReals,
            bounds=self.bounds_output_rule)
        # subcomponent input
        self.model.INPUT_SUB_UNIVARIATE = pe.Var(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_UNIVARIATE,
            self.model.TIMESTEPS,
            within=pe.NonNegativeReals,
            bounds=self.bounds_input_rule)
        
    def setupVariablesComponentsMultivariate(self):
        self.model.INPUT_SUB_MULTIVARIATE_LIN_EL = pe.Var(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_MULTIVARIATE_LIN_EL_INPUT,
            self.model.TIMESTEPS,
            within=pe.Reals)
        
        self.model.INPUT_SUB_MULTIVARIATE_LIN_EL_AUX = pe.Var(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_MULTIVARIATE_LIN_EL_INPUT,
            self.model.TIMESTEPS,
            within=pe.Reals)
        
        self.model.OUTPUT_SUB_MULTIVARIATE_LIN_EL = pe.Var(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_MULTIVARIATE_LIN_EL,
            self.model.TIMESTEPS,
            within=pe.Reals)
        
        self.model.OUTPUT_SUB_MULTIVARIATE_TOTAL = pe.Var(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_MULTIVARIATE,
            self.model.TIMESTEPS,
            within=pe.Reals)
        
        self.model.DELTA = pe.Var(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_MULTIVARIATE_LIN_EL,
            self.model.TIMESTEPS,
            within=pe.Binary)
        
    def setupVariablesComponentsSupplement(self):
        if CONFIG['startup']:          
            # Start cost [€]
            self.model.S_COST = pe.Var(
                self.model.COMPONENTS,
                self.model.TIMESTEPS,
                within=pe.NonNegativeReals) 
        if CONFIG['startup'] or CONFIG['updowntimes']:
            # startup-indicator [0/1]
            self.model.SUP_IND = pe.Var(
                self.model.COMPONENTS,
                self.model.TIMESTEPS,
                within=pe.Binary) 
            # shutdown indicator [0/1]   
            self.model.SDN_IND = pe.Var(
                self.model.COMPONENTS,
                self.model.TIMESTEPS,
                within=pe.Binary)
            
        if CONFIG['revision']:
            # revision on/off unit [0/1]    
            self.model.ACTIVE_REV = pe.Var(
                self.model.COMPONENTS,
                self.model.TIMESTEPS,
                within=pe.Binary) 
            # revision indicator [0/1]   
            self.model.ACTIVE_IND_REV = pe.Var(
                self.model.IDX_COMPONENTS_REVISIONS,
                self.model.TIMESTEPS,
                within=pe.Binary)
          
    def setupVariablesStorages(self):
        # Storage Loading
        self.model.LOADING = pe.Var(   
            self.model.STORAGES,
            self.model.TIMESTEPS,
            within=pe.NonNegativeReals)        
        # Storage Unloading
        self.model.UNLOADING = pe.Var( 
            self.model.STORAGES,
            self.model.TIMESTEPS,
            within=pe.NonNegativeReals)  
        # Storage Filllevel
        self.model.FILLLEVEL = pe.Var(   
            self.model.STORAGES,
            self.model.TIMESTEPS,
            within=pe.NonNegativeReals) 
        # storage load indicator [0/1]
        self.model.ACTIVE_LOAD = pe.Var(
            self.model.STORAGES,
            self.model.TIMESTEPS,
            within=pe.Binary)
        # storage unload indicator [0/1]
        self.model.ACTIVE_UNLOAD = pe.Var(
            self.model.STORAGES,
            self.model.TIMESTEPS,
            within=pe.Binary)        
             
    def setupObjective(self):
        """This method declares the pe.Objective required by all optimization models."""

        self.model.Objective = pe.Objective(rule = self.objective_rule, 
                                            sense = pe.minimize)
        
    def setupConstraints(self):
        self.setupConstraintsGeneral()
        self.setupConstraintsComponentsUnivariate()
        self.setupConstraintsComponentsMultivariate()
        self.setupConstraintsComponentsSupplement()
        self.setupConstraintsStorages()
        
    def setupConstraintsGeneral(self):
        """This method sets up all Constraints required by all optimization models."""
        ### PRODUCTS
        # product balance
        self.model.constr_product_balance = pe.Constraint(
            self.model.PRODUCTS,
            self.model.TIMESTEPS,
            rule=self.constr_product_balance_rule)
        
        ### PRODUCTION COMPONENTS
        # couple activity of subcomponents
        self.model.constr_activity_coupling = pe.Constraint(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS,
            self.model.TIMESTEPS,
            rule=self.constr_activity_coupling_rule)
        # couple output of component and its subcomponents
        self.model.constr_output_coupling = pe.Constraint(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_OUTPUTS,
            self.model.TIMESTEPS,
            rule=self.constr_output_coupling_rule)
        # couple input of component and its subcomponents
        self.model.constr_input_coupling = pe.Constraint(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_INPUTS,
            self.model.TIMESTEPS,
            rule=self.constr_input_coupling_rule)
        
    def setupConstraintsComponentsUnivariate(self):
        # coupled component output and activation (upper rule)
        self.model.constr_output_activation_upper = pe.Constraint(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_UNIVARIATE,
            self.model.TIMESTEPS,
            rule=self.constr_output_activation_upper_rule)
        # coupled component input and activation (upper rule)
        self.model.constr_input_activation_upper = pe.Constraint(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_UNIVARIATE,
            self.model.TIMESTEPS,
            rule=self.constr_input_activation_upper_rule)
        # coupled component output and activation (lower rule)
        self.model.constr_output_activation_lower = pe.Constraint(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_UNIVARIATE,
            self.model.TIMESTEPS,
            rule=self.constr_output_activation_lower_rule)
        # coupled component input and activation (lower rule)
        self.model.constr_input_activation_lower = pe.Constraint(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_UNIVARIATE,
            self.model.TIMESTEPS,
            rule=self.constr_input_activation_lower_rule)
        
        # piecewise linear coupling of component input and output
        if self.solver != 'glpk':
            pw_repn = 'SOS2'
        else:
            pw_repn = 'DCC'
            
        self.model.constr_component_input_output = pw.Piecewise(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_UNIVARIATE, 
            self.model.TIMESTEPS,
            self.model.INPUT_SUB_UNIVARIATE,
            self.model.OUTPUT_SUB_UNIVARIATE,
            pw_pts = self.model.domain_pts,
            pw_constr_type = 'EQ',
            pw_repn = pw_repn,
            f_rule = self.model.range_pts,
            force_pw = False,
            warn_domain_coverage = False)
        
    def setupConstraintsComponentsMultivariate(self):
        self.model.constr_output_multivariate = pe.Constraint(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_MULTIVARIATE_LIN_EL,
            self.model.TIMESTEPS,
            rule=self.constr_output_multivariate_rule)
        
        self.model.constr_output_multivariate_total = pe.Constraint(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_MULTIVARIATE,
            self.model.TIMESTEPS,
            rule=self.constr_output_multivariate_total_rule)
        
        self.model.constr_delta = pe.Constraint(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_MULTIVARIATE,
            self.model.TIMESTEPS,
            rule=self.constr_delta_rule)
        
        self.model.constr_element_boundary = pe.Constraint(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_MULTIVARIATE_LIN_EL_BOUNDARIES,
            self.model.TIMESTEPS,
            rule=self.constr_element_boundary_rule)
        
        self.model.constr_glover_upper1 = pe.Constraint(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_MULTIVARIATE_LIN_EL_INPUT,
            self.model.TIMESTEPS,
            rule=self.constr_glover_upper1_rule)
        
        self.model.constr_glover_upper2 = pe.Constraint(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_MULTIVARIATE_LIN_EL_INPUT,
            self.model.TIMESTEPS,
            rule=self.constr_glover_upper2_rule)
        
        self.model.constr_glover_lower1 = pe.Constraint(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_MULTIVARIATE_LIN_EL_INPUT,
            self.model.TIMESTEPS,
            rule=self.constr_glover_lower1_rule)
        
        self.model.constr_glover_lower2 = pe.Constraint(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_STATES_MULTIVARIATE_LIN_EL_INPUT,
            self.model.TIMESTEPS,
            rule=self.constr_glover_lower2_rule)
        
        # couple variables with exogenous parameters where needed
        self.model.constr_ex_param_coupling = pe.Constraint(
            self.model.IDX_COMPONENTS_SUBCOMPONENTS_MULTIVARIATE_INPUT_PARAMS,
            self.model.TIMESTEPS,
            rule=self.constr_ex_param_coupling_rule)
        
    def setupConstraintsComponentsSupplement(self):
        if CONFIG['ramping']:
            # maximum ramp-up-rate
            self.model.constr_max_rampup_rate = pe.Constraint(
                self.model.IDX_COMPONENTS_INPUTS| self.model.IDX_COMPONENTS_OUTPUTS,
                self.model.TIMESTEPS,
                rule=self.constr_max_rampup_rate_rule)
            # maximum ramp-down rate
            self.model.constr_max_rampdown_rate = pe.Constraint(
                self.model.IDX_COMPONENTS_INPUTS| self.model.IDX_COMPONENTS_OUTPUTS,
                self.model.TIMESTEPS,
                rule=self.constr_max_rampdown_rate_rule)
            
        if CONFIG['startup']:
            # startup/shutdown of unit  
            self.model.constr_startup_shutdown = pe.Constraint(
                self.model.COMPONENTS,
                self.model.TIMESTEPS,
                rule=self.constr_startup_shutdown_rule) 
            # startup cost of unit   
            self.model.constr_startup_cost = pe.Constraint(
                self.model.COMPONENTS,
                self.model.TIMESTEPS,
                rule=self.constr_startup_cost_rule)
            
        if CONFIG['updowntimes']:
            # minimum up-time of unit   
            self.model.constr_min_uptime = pe.Constraint(
                self.model.COMPONENTS,
                self.model.TIMESTEPS,
                rule=self.constr_min_uptime_rule) 
            # minimum down-time of unit   
            self.model.constr_min_downtime = pe.Constraint(
                self.model.COMPONENTS,
                self.model.TIMESTEPS,
                rule=self.constr_min_downtime_rule)
            
        if CONFIG['revision']:
            # revision activity  
            self.model.constr_revision_activity = pe.Constraint(
                self.model.COMPONENTS,
                self.model.TIMESTEPS,
                rule=self.constr_revision_activity_rule) 
            # start/stop revision of unit 
            self.model.constr_revision_coupling = pe.Constraint(
                self.model.COMPONENTS,
                self.model.TIMESTEPS,
                rule=self.constr_revision_coupling_rule) 
            # minimum revision length of unit  
            self.model.constr_revision_length = pe.Constraint(
                self.model.COMPONENTS,
                self.model.TIMESTEPS,
                rule=self.constr_revision_length_rule) 
            # yearly revisions per unit (maybe >= possible due to optimization)   
            self.model.constr_yearly_revision = pe.Constraint(
                self.model.IDX_COMPONENTS_REVISIONS,
                rule=self.constr_yearly_revision_rule) 
            # revision order
            self.model.constr_revision_order = pe.Constraint(
                self.model.IDX_COMPONENTS_REVISIONS,
                rule=self.constr_revision_order_rule)
        
    def setupConstraintsStorages(self):
        # filllevel definition
        self.model.constr_filllevel = pe.Constraint(
            self.model.STORAGES,
            self.model.TIMESTEPS,
            rule=self.constr_filllevel_rule)
        # max filllevel
        self.model.constr_max_filllevel = pe.Constraint(
            self.model.STORAGES,
            self.model.TIMESTEPS,
            rule=self.constr_max_filllevel_rule)
        # max filllevel
        self.constr_min_filllevel = pe.Constraint(
            self.model.STORAGES,
            self.model.TIMESTEPS,
            rule=self.constr_min_filllevel_rule)
        # max loading
        self.model.constr_max_loading = pe.Constraint(
            self.model.STORAGES,
            self.model.TIMESTEPS,
            rule=self.constr_max_loading_rule) 
        # max unloading
        self.model.constr_max_unloading = pe.Constraint(
            self.model.STORAGES,
            self.model.TIMESTEPS,
            rule=self.constr_max_unloading_rule) 
        # storage activity
        self.model.constr_activity_storage = pe.Constraint(
            self.model.STORAGES,
            self.model.TIMESTEPS,
            rule=self.constr_activity_storage_rule)
        

    def instantiate(self, input_dict: dict = None, filepath: str = None, skip_instantiation: bool = False):
        if not skip_instantiation:
            if input_dict:
                self.model_instance = self.model.create_instance(data=input_dict)
            elif filepath:
                self.model_instance = self.model.create_instance(filename=filepath)
            else:
                raise AttributeError("Please provide input data either as input dictionary for pyomo or as filepath to an input file.")
    
    def run(self, 
            input_dict: dict = None, 
            filepath: str = None, 
            solver: str = 'gurobi', 
            solver_options: dict = None, 
            executable: str = None, 
            debug: bool = True, 
            write: bool = False,
            skip_instantiation: bool = False, 
            output_dir: str = None):
        
        """This method is used to start an optimization.
    
        To start an optimization it is necessary to provide a file with the necessary input data.
        This file includes all information required to instantiate the optimization problem.
    
        Args:
            input_dict (dict): Input dictionary that contains all the necessary data for the optimization
            filename (str): Filename of the previously generated input file
            solver (str): Name of the solver to be used. Standard is 'gurobi'
        """
        
        if not hasattr(self, 'model_instance'):
            self.instantiate(input_dict, filepath, skip_instantiation)
            
        try:
            self.mysolver = pe.SolverFactory(solver, options=solver_options)
        except: 
            raise AttributeError('Solver \'{}\' has not been found.'.format(solver))
        
        if output_dir != None:
            logfile = output_dir + "/solver_output.log"
        else:
            logfile = None
            
        if (solver == "gurobi_persistent"):
            self.mysolver.set_instance(self.model_instance, symbolic_solver_labels=True)
            self.results = self.mysolver.solve(tee=debug, logfile=logfile)
        else:
            self.results = self.mysolver.solve(self.model_instance, tee=debug, logfile=logfile)
    
        if self.results.solver.termination_condition == TerminationCondition.optimal:
            self.model_instance.solutions.load_from(self.results)
            
        # else: #TODO: work-in-progress, infeasibility logging in case of infeasible or unbounded model
        #     write=1  
        #     logger = logging.getLogger('pyomo.util.infeasible')
        #     logger.setLevel(logging.DEBUG)
        #     # create file handler which logs even debug messages
        #     fh = logging.FileHandler(output_dir + '/infeasibility.log')
        #     fh.setLevel(logging.DEBUG)
        #     logger.addHandler(fh)
        #     log_infeasible_constraints(self.model_instance, log_expression=1, log_variables=1)
            
            
        #if write:
            #self.write_model_to_file(output_dir + '/model.txt')
        
    # Debug methods
    def write_model_to_file(self, filename):
        with open(filename, 'w') as output:
            sys.stdout = output
            self.model_instance.pprint()
            sys.stdout = sys.__stdout__
        
    # Evaluation methods
    def get_objective(self):
        return self.model_instance.Objective()
    
    def get_solution_time(self):
        return self.results.Solver._list[0]['Time']
    
    @staticmethod
    def get_dataframe_from_result(model_instance_variable, unstack: list = None):
        """Returns a dataframe with the results from the optimization model."""

        # Create empty dictionary
        variable = {}
        # for all indices in the model_instance_variable put its key-value pair in the dictionary
        for index in model_instance_variable:
            variable[index] = model_instance_variable[index].value
        
        # Create pandas Series from 
        variable = pd.Series(variable)
        if unstack:
            variable = variable.unstack(unstack)

        return variable

    @staticmethod
    def get_dataframe_from_parameter(model_instance_variable, unstack: list = None):
        """Returns a dataframe with the results from the optimization model."""

        # Create empty dictionary
        variable = {}
        # for all indices in the model_instance_variable put its key-value pair in the dictionary
        for index in model_instance_variable:
            variable[index] = model_instance_variable[index]
        
        # Create pandas Series from 
        variable = pd.Series(variable)
        if unstack:
            variable = variable.unstack(unstack)

        return variable
    
    @staticmethod
    def init_idx_components_subcomponents(model) -> "pyomo rule":
        return ((comp, subcomp) 
                for comp in model.COMPONENTS 
                for subcomp in model.SUBCOMPONENTS[comp])
    
    @staticmethod
    def init_idx_components_subcomponents_univariate(model) -> "pyomo rule":
        return ((comp, subcomp) 
                for comp in model.COMPONENTS 
                for subcomp in model.SUBCOMPONENTS_UNIVARIATE[comp])
    
    @staticmethod
    def init_idx_components_subcomponents_multivariate(model) -> "pyomo rule":
        return ((comp, subcomp) 
                for comp in model.COMPONENTS 
                for subcomp in model.SUBCOMPONENTS_MULTIVARIATE[comp])
    
    @staticmethod
    def init_idx_components_subcomponents_states(model) -> "pyomo rule":
        return ((comp, subcomp, state) 
                for comp in model.COMPONENTS 
                for subcomp in model.SUBCOMPONENTS[comp]
                for state in model.STATES[comp,subcomp])
    
    @staticmethod
    def init_idx_components_input(model) -> "pyomo rule":
        return set((comp,input_)
                   for comp in model.COMPONENTS
                   for subcomp in model.SUBCOMPONENTS[comp]   
                   for input_ in model.INPUT_PRODUCTS[comp,subcomp] | model.INPUT_PARAMS[comp,subcomp]
                   )
    
    @staticmethod
    def init_idx_components_output(model) -> "pyomo rule":
        return set((comp,output)
                   for comp in model.COMPONENTS
                   for subcomp in model.SUBCOMPONENTS[comp]   
                   for output in model.OUTPUT_PRODUCTS[comp,subcomp]
                )
    
    @staticmethod
    def init_idx_components_subcomponents_input(model) -> "pyomo rule":
        return ((comp,subcomp,input_)
                for comp in model.COMPONENTS
                for subcomp in model.SUBCOMPONENTS[comp]   
                for input_ in model.INPUT_PRODUCTS[comp,subcomp] | model.INPUT_PARAMS[comp,subcomp]
                )
    
    @staticmethod
    def init_idx_components_subcomponents_output(model) -> "pyomo rule":
        return ((comp,subcomp,output)
                for comp in model.COMPONENTS
                for subcomp in model.SUBCOMPONENTS[comp]   
                for output in model.OUTPUT_PRODUCTS[comp,subcomp]
                )
    
    @staticmethod
    def init_idx_components_subcomponents_states_univariate(model) -> "pyomo rule":
        return ((comp, subcomp, state) 
                for comp in model.COMPONENTS 
                for subcomp in model.SUBCOMPONENTS_UNIVARIATE[comp]
                for state in model.STATES[comp,subcomp])
    
    @staticmethod
    def init_idx_components_subcomponents_states_multivariate(model) -> "pyomo rule":
        return ((comp, subcomp, state) 
                for comp in model.COMPONENTS 
                for subcomp in model.SUBCOMPONENTS_MULTIVARIATE[comp]
                for state in model.STATES[comp,subcomp])
        
    @staticmethod
    def init_idx_components_subcomponents_states_multivariate_lin_el(model) -> "pyomo rule":
        return ((comp, subcomp, state, lin_el)
                for comp in model.COMPONENTS 
                for subcomp in model.SUBCOMPONENTS_MULTIVARIATE[comp]
                for state in model.STATES[comp,subcomp]
                for lin_el in range(model.num_lin_el[comp,subcomp,state]))
        
    @staticmethod
    def init_idx_components_subcomponents_states_multivariate_lin_el_input(model) -> "pyomo rule":
        return ((comp, subcomp, state, lin_el, input_idx)
                for comp in model.COMPONENTS 
                for subcomp in model.SUBCOMPONENTS_MULTIVARIATE[comp]
                for state in model.STATES[comp,subcomp]
                for lin_el in range(model.num_lin_el[comp,subcomp,state])
                for input_idx in range(model.input_dim[comp,subcomp,state]))
    
    @staticmethod
    def init_idx_components_subcomponents_states_multivariate_lin_el_boundaries(model) -> "pyomo rule":
        return ((comp, subcomp, state, lin_el, bound)
                for comp in model.COMPONENTS 
                for subcomp in model.SUBCOMPONENTS_MULTIVARIATE[comp]
                for state in model.STATES[comp,subcomp]
                for lin_el in range(model.num_lin_el[comp,subcomp,state])
                for bound in range(len(model.boundary_equations[comp,subcomp,state][lin_el][:, 0])))
    
    @staticmethod
    def init_idx_components_subcomponents_multivariate_input_params(model) -> "pyomo rule":
        return ((comp, subcomp, input_param)
                for comp in model.COMPONENTS
                for subcomp in model.SUBCOMPONENTS_MULTIVARIATE[comp]
                for input_param in model.INPUT_PARAMS[comp,subcomp])
    
    @staticmethod
    def init_idx_components_revisions(model) -> "pyomo rule":
        return ((comp, rev) 
                for comp in model.COMPONENTS 
                for rev in model.REVISIONS[comp])
    
    @staticmethod
    def bounds_output_rule(model, component: str, subcomponent: str, state: int, t: int) -> "pyomo rule":
        return(0, 
               max(model.domain_pts[component, subcomponent, state, t][1:]))
    
    @staticmethod
    def bounds_input_rule(model, component: str, subcomponent: str, state: int, t: int) -> "pyomo rule":
        return(0, 
               max(model.range_pts[component, subcomponent, state, t][1:]))
    
    # Model rule methods
    @staticmethod
    def objective_rule(model):  
        expr = (
            # sum over timesteps
            sum(
                # sum over products
                sum(
                    # source use
                    model.USED_SOURCE[prod,t]*model.price[prod,t]
                    # sink use
                    - model.USED_SINK[prod,t]*model.compensation[prod,t]
                    # non-served demand
                    + model.NON_SERVED_DEMAND[prod,t]*1000
                    for prod in model.PRODUCTS)
                for t in model.TIMESTEPS)
            )
        
        if CONFIG['startup']:
            expr += sum(sum(model.S_COST[comp,t]
                            for comp in model.COMPONENTS) 
                        for t in model.TIMESTEPS)
            
        return expr
    
    @staticmethod
    def constr_product_balance_rule(model, prod: str, t: int) -> "pyomo rule":
        """ Demand == Production + Storage + Source Use - Sink Use """
        return(
            # satisfy demand
            model.demand[prod,t] + model.NON_SERVED_DEMAND[prod,t] == 
            # production
            # component output
            sum(model.OUTPUT[comp,prod,t] 
                for comp in model.COMPONENTS
                if (comp,prod) in model.IDX_COMPONENTS_OUTPUTS
                )
            # component input
            - sum(model.INPUT[comp,prod,t] 
                  for comp in model.COMPONENTS
                  if (comp,prod) in model.IDX_COMPONENTS_INPUTS
                  )
            # storages
            + sum(
                model.unload_eff[storage]*model.UNLOADING[storage, t]
                -model.LOADING[storage, t]
                for storage in model.STORAGES
                if model.storage_product[storage] == prod)
            # sources
            + model.has_source[prod]*model.USED_SOURCE[prod,t]
            # sinks
            - model.has_sink[prod]*model.USED_SINK[prod,t]
            )
    
    @staticmethod
    def constr_activity_coupling_rule(model, comp: str, subcomp: str, t: int) -> "pyomo rule":
        """ ACTIVE == 0 -> no state active 
            ACTIVE == 1 -> exactly one state active 
        """
        force_GUD = False
        
        if not force_GUD or not comp == 'GUD':
            return(
                sum(model.STATE_ACTIVE[comp,subcomp,state,t]
                    for state in model.STATES[comp,subcomp]) == 
                model.ACTIVE[comp,t]
                )
        else:
            return(
                sum(model.STATE_ACTIVE[comp,subcomp,state,t]
                    for state in model.STATES[comp,subcomp]) == 
                1
                )
            

    @staticmethod
    def constr_output_coupling_rule(model, comp: str, subcomp: str, prod: str, t: int) -> "pyomo rule":
        """ couples output of subcomponents with output of component """

        # univariate component
        if subcomp in model.SUBCOMPONENTS_UNIVARIATE[comp]:
            return(
                model.OUTPUT[comp,prod,t] ==
                sum(model.OUTPUT_SUB_UNIVARIATE[comp,subcomp,state,t] 
                    for state in model.STATES[comp,subcomp])
                )
        
        # multivariate component
        elif subcomp in model.SUBCOMPONENTS_MULTIVARIATE[comp]:
            if model.var_mapper[comp,subcomp,prod][0] == 'input':
                idx = model.var_mapper[comp,subcomp,prod][1]
                rhs = sum(model.INPUT_SUB_MULTIVARIATE_LIN_EL_AUX[comp,subcomp,state,lin_el,idx,t]
                          for state in model.STATES[comp,subcomp]
                          for lin_el in range(model.num_lin_el[comp,subcomp,state]))
            elif model.var_mapper[comp,subcomp,prod][0] == 'output':
                rhs = sum(model.OUTPUT_SUB_MULTIVARIATE_TOTAL[comp,subcomp,state,t]
                          for state in model.STATES[comp,subcomp])
                
            return model.OUTPUT[comp,prod,t] == rhs
     
    @staticmethod
    def constr_input_coupling_rule(model, comp: str, subcomp: str, param: str, t: int) -> "pyomo rule":
        """ couples input of subcomponents with input of component """
            
        # univariate component
        if subcomp in model.SUBCOMPONENTS_UNIVARIATE[comp]:
            return(
                model.INPUT[comp,param,t] ==
                sum(model.INPUT_SUB_UNIVARIATE[comp,subcomp,state,t]
                    for state in model.STATES[comp,subcomp])
                )
        
        # multivariate component
        elif subcomp in model.SUBCOMPONENTS_MULTIVARIATE[comp]:
            if model.var_mapper[comp,subcomp,param][0] == 'input':
                idx = model.var_mapper[comp,subcomp,param][1]
                rhs = sum(model.INPUT_SUB_MULTIVARIATE_LIN_EL_AUX[comp,subcomp,state,lin_el,idx,t]
                          for state in model.STATES[comp,subcomp]
                          for lin_el in range(model.num_lin_el[comp,subcomp,state]))
            elif model.var_mapper[comp,subcomp,param][0] == 'output':
                rhs = sum(model.OUTPUT_SUB_MULTIVARIATE_TOTAL[comp,subcomp,state,t]
                          for state in model.STATES[comp,subcomp])
                
            return model.INPUT[comp,param,t] == rhs
    
    
    
    @staticmethod
    def constr_output_activation_upper_rule(model, comp: str, subcomp: str, state: int, t: int) -> "pyomo rule":
        return(
            model.OUTPUT_SUB_UNIVARIATE[comp,subcomp,state,t] <= 
            max(model.domain_pts[comp,subcomp,state,t][1:]) * 
                model.STATE_ACTIVE[comp,subcomp,state,t]
            )
    
    @staticmethod
    def constr_input_activation_upper_rule(model, comp: str, subcomp: str, state: int, t: int) -> "pyomo rule":
        return(
            model.INPUT_SUB_UNIVARIATE[comp,subcomp,state,t] <= 
            max(model.range_pts[comp,subcomp,state,t][1:]) * 
                model.STATE_ACTIVE[comp,subcomp,state,t]
                )
        
    @staticmethod
    def constr_output_activation_lower_rule(model, comp: str, subcomp: str, state: int, t: int) -> "pyomo rule":
        return(
            model.OUTPUT_SUB_UNIVARIATE[comp,subcomp,state,t] >= 
            min(model.domain_pts[comp,subcomp,state,t][1:]) * 
                model.STATE_ACTIVE[comp,subcomp,state,t]
                )
             
    
    @staticmethod
    def constr_input_activation_lower_rule(model, comp: str, subcomp: str, state: int, t: int) -> "pyomo rule":
        return(
            model.INPUT_SUB_UNIVARIATE[comp,subcomp,state,t] >= 
            min(model.range_pts[comp,subcomp,state,t][1:]) * 
                model.STATE_ACTIVE[comp,subcomp,state,t]
            )
        
        
    @staticmethod
    def constr_output_multivariate_rule(model, comp: str, subcomp: str, state: int, lin_el: int, t: int) -> "pyomo rule":
        return(
            model.OUTPUT_SUB_MULTIVARIATE_LIN_EL[comp,subcomp,state,lin_el,t] == 
            model.DELTA[comp,subcomp,state,lin_el,t] * 
            model.coefficients[comp,subcomp,state][lin_el][0] + 
            sum(model.coefficients[comp,subcomp,state][lin_el][input_idx+1] * 
                model.INPUT_SUB_MULTIVARIATE_LIN_EL_AUX[comp,subcomp,state,lin_el,input_idx,t] 
                for input_idx in range(model.input_dim[comp,subcomp,state]))
            )
            
    @staticmethod
    def constr_output_multivariate_total_rule(model, comp: str, subcomp: str, state: int, t: int) -> "pyomo rule":
        return(
            model.OUTPUT_SUB_MULTIVARIATE_TOTAL[comp,subcomp,state,t] == 
            sum(model.OUTPUT_SUB_MULTIVARIATE_LIN_EL[comp,subcomp,state,lin_el,t]
                for lin_el in range(model.num_lin_el[comp,subcomp,state]))
               )
    
    @staticmethod
    def constr_delta_rule(model, comp: str,subcomp: str, state: int, t: int) -> "pyomo rule":
        return( 
            sum(model.DELTA[comp,subcomp,state,lin_el,t] 
                for lin_el in range(model.num_lin_el[comp,subcomp,state])) == 
            model.STATE_ACTIVE[comp,subcomp,state,t]
            )
    
    @staticmethod
    def constr_element_boundary_rule(model, comp: str, subcomp: str, state: int, lin_el: int, bound: int, t: int) -> "pyomo rule":
        if model.boundary_sign[comp,subcomp,state][lin_el][bound] > 0:
            return(
                model.DELTA[comp,subcomp,state,lin_el,t] * model.boundary_equations[comp,subcomp,state][lin_el][bound, -1] + 
                sum(model.boundary_equations[comp,subcomp,state][lin_el][bound,input_idx] * 
                    model.INPUT_SUB_MULTIVARIATE_LIN_EL_AUX[comp,subcomp,state,lin_el,input_idx,t]
                    for input_idx in range(model.input_dim[comp,subcomp,state] - 1)) >= 
                model.INPUT_SUB_MULTIVARIATE_LIN_EL_AUX[comp,subcomp,state,lin_el,model.input_dim[comp,subcomp,state]-1,t]
                )
        elif model.boundary_sign[comp,subcomp,state][lin_el][bound] < 0:
            return(
                model.DELTA[comp,subcomp,state,lin_el,t] * model.boundary_equations[comp,subcomp,state][lin_el][bound, -1] + 
                sum(model.boundary_equations[comp,subcomp,state][lin_el][bound,input_idx] * 
                    model.INPUT_SUB_MULTIVARIATE_LIN_EL_AUX[comp,subcomp,state,lin_el,input_idx,t]
                    for input_idx in range(model.input_dim[comp,subcomp,state] - 1)) <= 
                model.INPUT_SUB_MULTIVARIATE_LIN_EL_AUX[comp,subcomp,state,lin_el,model.input_dim[comp,subcomp,state]-1,t]
                )
    
    @staticmethod
    def constr_glover_upper1_rule(model, comp: str, subcomp: str, state: int, lin_el: int, input_idx: int, t: int) -> "pyomo rule":
        return (model.DELTA[comp,subcomp,state,lin_el,t] * 
                model.glover_upper[comp,subcomp,state][lin_el][input_idx] >= 
                model.INPUT_SUB_MULTIVARIATE_LIN_EL_AUX[comp,subcomp,state,lin_el,input_idx,t]
                )
    
    @staticmethod
    def constr_glover_lower1_rule(model, comp: str, subcomp: str, state: int, lin_el: int, input_idx: int, t: int) -> "pyomo rule":
        return (model.DELTA[comp,subcomp,state,lin_el,t] * 
                model.glover_lower[comp,subcomp,state][lin_el][input_idx] <=
                model.INPUT_SUB_MULTIVARIATE_LIN_EL_AUX[comp,subcomp,state,lin_el,input_idx,t]
                )
    
    @staticmethod
    def constr_glover_upper2_rule(model, comp: str, subcomp: str, state: int, lin_el: int, input_idx: int, t: int) -> "pyomo rule":
        return (model.INPUT_SUB_MULTIVARIATE_LIN_EL[comp,subcomp,state,lin_el,input_idx,t] - 
                model.glover_lower[comp,subcomp,state][lin_el][input_idx] * 
                (1 - model.DELTA[comp,subcomp,state,lin_el,t]) >=
                model.INPUT_SUB_MULTIVARIATE_LIN_EL_AUX[comp,subcomp,state,lin_el,input_idx,t]
               )
    
    @staticmethod
    def constr_glover_lower2_rule(model, comp: str, subcomp: str, state: int, lin_el: int, input_idx: int, t: int) -> "pyomo rule":
        return (model.INPUT_SUB_MULTIVARIATE_LIN_EL[comp,subcomp,state,lin_el,input_idx,t] - 
                model.glover_upper[comp,subcomp,state][lin_el][input_idx] * 
                (1 - model.DELTA[comp,subcomp,state,lin_el,t]) <=
                model.INPUT_SUB_MULTIVARIATE_LIN_EL_AUX[comp,subcomp,state,lin_el,input_idx,t]
               )
    
    @staticmethod
    def constr_ex_param_coupling_rule(model, comp: str, subcomp: str, input_param: str, t: int) -> "pyomo rule":
        if input_param in model.EX_PARAMS:
            if model.var_mapper[comp,subcomp,input_param][0] == 'input':
                idx = model.var_mapper[comp,subcomp,input_param][1]
                rhs = sum(model.INPUT_SUB_MULTIVARIATE_LIN_EL_AUX[comp,subcomp,state,lin_el,idx,t]
                          for state in model.STATES[comp,subcomp]
                          for lin_el in range(model.num_lin_el[comp,subcomp,state]))
            elif model.var_mapper[comp,subcomp,input_param][0] == 'output':
                rhs = sum(model.OUTPUT_SUB_MULTIVARIATE_TOTAL[comp,subcomp,state,t]
                          for state in model.STATES[comp,subcomp])
            
            return model.ACTIVE[comp,t] * model.ex_param[input_param,t] == rhs
            
        else:
            return pe.Constraint.Skip
    
    @staticmethod
    def constr_max_rampup_rate_rule(model, comp: str, param: str, t: int) -> "pyomo rule":
        if model.rr[comp,param] < float("inf") and t > 0:
            #TODO: pmax - get max output of comp
            if (comp,param) in model.IDX_COMPONENTS_OUTPUTS:
                var_t   = model.OUTPUT[comp,param,t]
                var_t_1 = model.OUTPUT[comp,param,t-1]
            elif (comp,param) in model.IDX_COMPONENTS_INPUTS:
                var_t   = model.INPUT[comp,param,t]
                var_t_1 = model.INPUT[comp,param,t-1]
            
            return(
                var_t-var_t_1 <= 
                model.rr[comp,param] * model.ACTIVE[comp,t-1] + 100000 * 
                (1 - model.ACTIVE[comp,t-1]) 
                )
        else:
            return pe.Constraint.Skip
    
    @staticmethod
    def constr_max_rampdown_rate_rule(model, comp: str, param: str, t: int) -> "pyomo rule":
        if model.rr[comp,param] < float("inf") and t > 0:
            #TODO: pmax - get max output of comp
            if (comp,param) in model.IDX_COMPONENTS_OUTPUTS:
                var_t   = model.OUTPUT[comp,param,t]
                var_t_1 = model.OUTPUT[comp,param,t-1]
            elif (comp,param) in model.IDX_COMPONENTS_INPUTS:
                var_t   = model.INPUT[comp,param,t]
                var_t_1 = model.INPUT[comp,param,t-1]
            return(
                var_t_1-var_t <=
                model.rr[comp,param] * model.ACTIVE[comp,t] + 100000 * (1 - model.ACTIVE[comp,t])
                )
        else:
            return pe.Constraint.Skip 
        
    @staticmethod
    def constr_startup_shutdown_rule(model, comp: str, t: int) -> "pyomo rule":
        """
        Variable coupling for activity binaries modeling state changes
            
            ACTIVE[comp, t-1] == 0 and ACTIVE[comp, t] == 1 --> SUP_IND[comp, t] == 1
            ACTIVE[comp, t-1] == 1 and ACTIVE[comp, t] == 0 --> SDN_IND[comp, t] == 1
        """
        if t > 0:
            return(
                model.ACTIVE[comp,t-1] - model.ACTIVE[comp,t] 
                - model.SDN_IND[comp,t] + model.SUP_IND[comp,t] == 0
                )
        else:
            return(
                model.SUP_IND[comp,0] >= 
                model.ACTIVE[comp,0]
            )

    @staticmethod
    def constr_startup_cost_rule(model, comp: str, t: int) -> "pyomo rule":
        return(
            model.S_COST[comp,t] == 
            model.SUP_IND[comp,t] * model.sc[comp]
            )

    @staticmethod
    def constr_min_uptime_rule(model, comp: str, t: int) -> "pyomo rule":
        if t - model.mut[comp] + 1 >= 0:
            return(
                sum(model.SUP_IND[comp,i] 
                    for i in range(t - model.mut[comp] + 1,t+1)) <= 
                    model.ACTIVE[comp,t]
                    )
        else:
            return pe.Constraint.Skip 
    
    @staticmethod
    def constr_min_downtime_rule(model, comp: str, t: int) -> "pyomo rule":
        if t - model.mdt[comp] + 1 >= 0:
            return(
                sum(model.SDN_IND[comp,i] 
                    for i in range(t - model.mdt[comp] + 1,t+1)) <= 
                    1 - model.ACTIVE[comp,t]
                )
        else:
            return pe.Constraint.Skip 
    
    @staticmethod
    def constr_revision_activity_rule(model, comp: str, t: int) -> "pyomo rule":
        """
        Integer cut: 
            A component comp cannot be in active state and in revision in the same timestep t.
        """
        return(
            model.ACTIVE[comp,t] + model.ACTIVE_REV[comp,t] <= 1
            )
    
    @staticmethod
    def constr_revision_coupling_rule(model, comp: str, t: int) -> "pyomo rule":
        """
        Variable coupling for revision binaries modeling state changes
            
            ACTIVE_REV[comp, t-1] == 0 and ACTIVE_REV[comp, t] == 1 --> sum(ACTIVE_IND_REV[comp, rev, t] for rev in model.REVISIONS[comp]) == 1
        """
        if model.REVISIONS[comp]:
                if t == 0:
                    return(
                        sum(model.ACTIVE_IND_REV[comp, rev, 0] for rev in model.REVISIONS[comp]) ==
                        model.ACTIVE_REV[comp,0]
                        )
                elif 0 < t <= max(model.TIMESTEPS) - model.r_length[comp,max(model.REVISIONS[comp])] + 1:
                    return(
                        sum(model.ACTIVE_IND_REV[comp,rev,t] for rev in model.REVISIONS[comp]) >=
                        model.ACTIVE_REV[comp,t] - model.ACTIVE_REV[comp,t-1]
                        )
                else: # t > max(model.TIMESTEPS) - model.r_length[comp,max(model.REVISIONS)] + 1:
                    return(
                        sum(model.ACTIVE_IND_REV[comp,rev,t] for rev in model.REVISIONS[comp]) == 0
                          )
        else:
            return pe.Constraint.Skip
    
    @staticmethod
    def constr_revision_length_rule(model, comp: str, t: int) -> "pyomo rule":
        return(
            sum(
                sum(model.ACTIVE_IND_REV[comp,rev,i] 
                    for i in range(t - model.r_length[comp,rev] + 1, t+1)
                    if i >= 0)
                for rev in model.REVISIONS[comp]) ==
            model.ACTIVE_REV[comp,t]
        )  
    
    @staticmethod
    def constr_yearly_revision_rule(model, comp: str, rev: int) -> "pyomo rule":
        """ 
        Each revision is scheduled.
        """
        return(
            sum(model.ACTIVE_IND_REV[comp,rev,t] 
                for t in model.TIMESTEPS) == 1
            )
    
    @staticmethod
    def constr_revision_order_rule(model, comp: str, rev: int) -> "pyomo rule":
        if model.REVISIONS[comp] and rev < max(model.REVISIONS[comp]):
            return(
               sum(model.ACTIVE_IND_REV[comp,rev,t]*t 
                   for t in model.TIMESTEPS)  + model.r_length[comp,rev] + model.nr_length[comp] <=  
               sum(model.ACTIVE_IND_REV[comp,rev+1,t]*t 
                   for t in model.TIMESTEPS)
            )
        else: 
            return pe.Constraint.Skip 
        
    @staticmethod
    def constr_filllevel_rule(model, s: str, t: int) -> "pyomo rule":
        """ Storages are operated in a way such that the first filllevel or a cycle equals the last. """
        return( 
            model.FILLLEVEL[s,t] ==
            # Loading
            model.LOADING[s,model.previous_timestep[t]] * model.load_eff[s] 
            # Unloading
            - model.UNLOADING[s,model.previous_timestep[t]]
            # Last filllevel
            + model.FILLLEVEL[s,model.previous_timestep[t]]
            )
    
    @staticmethod
    def constr_max_filllevel_rule(model, s: str, t: int) -> "pyomo rule":
        return( 
            model.FILLLEVEL[s,t] <= 
            model.max_vol[s]
            )
    
    @staticmethod
    def constr_min_filllevel_rule(model, s: str, t: int) -> "pyomo rule":
        return( 
            model.FILLLEVEL[s,t] >= 
            model.min_vol[s]
            )

    @staticmethod
    def constr_max_loading_rule(model, s: str, t: int) -> "pyomo rule":
        return( 
            model.LOADING[s,t] <= model.max_load[s] * model.ACTIVE_LOAD[s,t]
            )

    @staticmethod
    def constr_max_unloading_rule(model, s: str, t: int) -> "pyomo rule":
        return( 
            model.UNLOADING[s,t] <= model.max_unload[s] * model.ACTIVE_UNLOAD[s,t]
            )
    
    @staticmethod
    def constr_activity_storage_rule(model, s: str, t: int) -> "pyomo rule":
        return( 
            model.ACTIVE_LOAD[s,t] + model.ACTIVE_UNLOAD[s,t] <= 1
            )
        