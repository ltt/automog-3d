settings = {'gurobi': {'threads': '4', 'mipgap': '0.05'}, 'cplex': {'threads': 4, 'mipgap': 0.02, 'timelimit': 5400},
            'glpk': {'mipgap': '0.05'}}