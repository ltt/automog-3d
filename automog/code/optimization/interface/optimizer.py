import pandas as pd
import pickle
import copy
import os
from datetime import datetime
import pyomo.environ as pe
from optimization.engine import OperationalOptimizationInterface

class model(object):
    
    def __init__(self, components: dict, curves: dict, sections: dict, storages: dict = {}, name: str = ''):
        self.components = copy.deepcopy(components)
        self.curves     = copy.deepcopy(curves)
        self.sections   = copy.deepcopy(sections)
        self.storages   = copy.deepcopy(storages)
        self.name       = name
        self.nstorages  = len(self.storages)
        
    @classmethod
    def from_file(cls, file_path: str):
        with open(file_path, 'rb') as f:
            m = cls.from_dict(pickle.load(f))
            if not hasattr(m,'name'):
                m.name = os.path.basename(os.path.dirname(file_path))
        return m
    
    @classmethod 
    def from_dict(cls, init_dict: dict): 
        from modeling.automog import MOG_Component
        from modeling.Hinging_Hyperplane.hinging_hyperplane import HHmodel
        
        init_dict['components'] = {comp: MOG_Component.from_dict(init_dict['components'][comp]) 
                                   for comp in init_dict['components'] }
        
        # initialize model objects
        for comp in init_dict['curves']:
            for state in init_dict['curves'][comp]:
                for sect in init_dict['curves'][comp][state]:
                    if 'model' in init_dict['curves'][comp][state][sect]:
                        init_dict['curves'][comp][state][sect]['model'] = HHmodel.from_dict(init_dict['curves'][comp][state][sect]['model'])
                        
        return cls(**init_dict)
    
    def to_file(self, save_path: str):
        with open(save_path, 'wb') as f:
            pickle.dump(self.to_dict(), f)
            
    def to_dict(self):
        output_dict = {attr: getattr(self, attr) 
                       for attr in ['name', 'components', 'curves', 'sections', 'storages']}
        output_dict['components'] = {comp: output_dict['components'][comp].to_dict()
                                     for comp in output_dict['components']}
       
        # convert model attribute to dict
        for comp in output_dict['curves']:
            for state in output_dict['curves'][comp]:
                for sect in output_dict['curves'][comp][state]:
                    if 'model' in output_dict['curves'][comp][state][sect]:
                        if not type(output_dict['curves'][comp][state][sect]['model']) == dict:
                            output_dict['curves'][comp][state][sect]['model'] = output_dict['curves'][comp][state][sect]['model'].to_dict()
        return output_dict

    @property
    def components(self):
        return self._components
    
    @components.setter
    def components(self, components: dict):
        self._components = components
        self.products    = {}
        self.input_params      = {}
        
        for comp in self._components:
            for attr in ['inputs', 'outputs']:
                for param in getattr(self._components[comp], attr):
                    # check if param is coupled with product
                    if not param in self._components[comp].params: 
                        # check that product is not listed yet
                        if not param in self.products:
                            self.products[param] = {'demand'      : None, 
                                                    'price'       : None,
                                                    'compensation': None}
                    # param is not coupled with product
                    else:
                        # check that param is not listed yet
                        if not param in self.input_params:
                            self.input_params[param] = None
                                   
        self.ncomponents = len(components)
        self.nproducts   = len(self.products)
                        
    @property
    def curves(self):
        return self._curves
    
    @curves.setter
    def curves(self, c: dict):
        self._curves = c
        
    @property
    def sections(self):
        return self._sections
    
    @sections.setter
    def sections(self, sect: dict):
        self._sections = sect
        self.nsections = 0
        for comp in sect:
            if type(sect[comp]) == dict:
                self.nsections += sum(sect[comp].values())
                
            # downwards compatible for models without different operating states
            else:
                self.nsections += sect[comp] 
                self._sections[comp] = {0: self._sections[comp]}
                self._curves[comp]   = {0: self._curves[comp]}
                               
class instance(object):
    
    def __init__(self, params: pd.DataFrame):
        self.params = copy.deepcopy(params)
    
    @staticmethod
    def from_file():
        pass
    
    @classmethod
    def from_csv(cls, path: str):
        return cls(pd.read_csv(path))
    
    @classmethod
    def from_excel(cls, path: str):
        return cls(pd.read_excel(path))
    
    def to_excel(self, path: str):
        self.params.to_excel(path, index=False)
    
    @property
    def params(self):
        return self._params
    
    @params.setter
    def params(self, p: pd.DataFrame):
        self._params = p
        self.ntimesteps = len(p)
        self.nsources  = sum([1 for col in p.columns if 'price' in col]) 
        self.nsinks    = sum([1 for col in p.columns if 'compensation' in col])
        self.ndemands  = sum([1 for col in p.columns if 'demand' in col])
        self.nexparams = sum([1 for col in p.columns if 'Params' in col])
        self.cycles = 1 
        
    def upscale(self, scale_factor: int):
        if not hasattr(self, 'initial_params'):
            self.initial_params = copy.deepcopy(self.params)
        
        self.params = pd.concat([self.initial_params] * int(scale_factor) +
                                [self.initial_params.head(int(len(self.initial_params.index) * (scale_factor - int(scale_factor))))])
        self.params = self.params.reset_index().drop('index',axis=1)
            
    def downscale(self, from_: int, to: int):
        if not hasattr(self, 'initial_params'):
            self.initial_params = copy.deepcopy(self.params)
            
        self.params = self.initial_params.iloc[from_:to+1, :].reset_index().drop('index',axis=1)
        
class results(object):
    
    def __init__(self, results: dict):
        self.all_ = results
        
        ### table frames
        self.balance_frames = {}
        self.cost_frames    = {}
  
        categories = ['demand','NON_SERVED_DEMAND','USED_SOURCE','USED_SINK','produced', 'storaged']
        categories_cost = [None, 1000, '+price', '-compensation', None]
        cat_cost_match = dict(zip(categories,categories_cost))
        
        for time_step in range(len(results['TIMESTEPS'])):
            # init balance data
            balance_data = {}
            for c in categories+results['COMPONENTS']+results['STORAGES']:
                balance_data[c]=[]
            
            # init cost data
            cost_data={cat : [] 
                       for cat,cat_cost in cat_cost_match.items()
                       if cat_cost != None}
            
            for product in results['PRODUCTS']:
                # get product-specific results from optimization problem
                for cat in categories[:-2]:
                    # get product balance values
                    balance_data[cat].append(results[cat][product, time_step])
                    
                    # get cost values
                    if cat_cost_match[cat] != None:
                        if type(cat_cost_match[cat]) == int:
                            cost_data[cat].append(results[cat][product, time_step]*cat_cost_match[cat])
                        elif type(cat_cost_match[cat]) == str:
                            sign = cat_cost_match[cat][0]
                            if sign == '-':
                                cost_data[cat].append(-results[cat][product, time_step]*results[cat_cost_match[cat][1:]][product, time_step])
                            elif sign == '+':
                                cost_data[cat].append(results[cat][product, time_step]*results[cat_cost_match[cat][1:]][product, time_step])
                     
                # get component-specific data and calculate total production amount
                production_balance = 0
                for comp in results['COMPONENTS']:
                    is_output = (comp,product) in results['IDX_COMPONENTS_OUTPUTS']
                    is_input  = (comp,product) in results['IDX_COMPONENTS_INPUTS']
                    
                    comp_balance = 0                        
                         
                    if is_output:
                        comp_balance += results['OUTPUT'][comp, product, time_step]
                    if is_input:
                        comp_balance -= results['INPUT'][comp, product, time_step]
                    
                    balance_data[comp].append(comp_balance)
                    production_balance += comp_balance
                        
                balance_data['produced'].append(production_balance)
                
                # get storage-specific data and calculate total storage amount
                storage_balance = 0
                for s in results['STORAGES']:
                    if results['storage_product'][s] == product:
                        s_balance = results['UNLOADING'][s, time_step]\
                                    -results['LOADING'][s, time_step]
                    else:
                        s_balance = 0
                    balance_data[s].append(s_balance)
                    storage_balance += s_balance
                balance_data['storaged'].append(storage_balance)
                
            balance_frame = pd.DataFrame.from_dict(balance_data, orient="index", columns=results['PRODUCTS'])
            balance_frame = balance_frame.reindex(categories+sorted(results['COMPONENTS']+sorted(results['STORAGES'])))
            balance_frame = balance_frame.fillna(0)

            cost_frame = pd.DataFrame.from_dict(cost_data, orient="index", columns=results['PRODUCTS'])
            cost_frame = cost_frame.fillna(0)
            cost_frame.loc["total"] = cost_frame.sum()
            cost_frame ["total"] = cost_frame.sum(axis=1)
            
            self.balance_frames[time_step] = balance_frame
            self.cost_frames[time_step] = cost_frame
        
        ### plot frames
        self.lines = {}
        
        for product in results['PRODUCTS']:
            self.lines[product] = {'price': None,
                                   'compensation': None,
                                   'demand': None,
                                   'OUTPUT': None,
                                   'INPUT': None,
                                   'USED_SOURCE': None,
                                   'USED_SINK': None,
                                   'NON_SERVED_DEMAND': None,
                                   'LOADING': None,
                                   'UNLOADING': None}
            
            for line in self.lines[product]:
                if line not in ['LOADING', 'UNLOADING']:
                    for idx_level in range(results[line].index.nlevels):
                        if product in results[line].index.get_level_values(idx_level):
                            break
                    filter_ = results[line].index.get_level_values(idx_level) == product
                else:
                    filter_ = (results['storage_product'][results[line].index.get_level_values(0)] == product).values
                
                self.lines[product][line] = results[line][filter_]
        
    @classmethod
    def from_file(cls, file_path: str):
        
        with open(file_path, 'rb') as f:
            res,mod,inst = pickle.load(f)
        return cls(res),model.from_dict(mod),instance(inst)
        
    def to_file(self, save_path: str, mod: model, inst:instance):
        with open(save_path, 'wb') as f:
            pickle.dump((self.all_,
                         copy.deepcopy(mod).to_dict(),
                         inst.params), f)
        
class optimizer(object):
    
    def __init__(self, m: model = None, inst: instance = None,
                 components: dict = None, curves: dict = None, linear_sections: dict = None,
                 params: pd.DataFrame = None,
                 solver: str = 'glpk', 
                 solver_options: dict = None,
                 config: dict = None):
        
        # assert (m != None) != (components != None and curves != None and linear_sections != None)
        # assert (inst != None) != (isinstance(params, pd.DataFrame))
        
        if m != None:
            self.model = m
        elif components != None and curves != None and linear_sections != None:
            self.model = model(components, curves, linear_sections)
            
        if inst != None:
            self.instance = inst
        elif isinstance(params, pd.DataFrame):
            self.instance = instance(params)
            
        self.solver = solver
        self.solver_options = solver_options
        self.config = config
         
    @staticmethod
    def from_file():
        pass
    
    @property
    def model(self):
        return self._model
    
    @model.setter
    def model(self, m: model):
        if hasattr(self,'_instance'):
            inst = self._instance
        
        self._model = m
        
        if hasattr(self,'_instance'):
            self.instance = inst
    
    @property
    def instance(self):
        if hasattr(self, '_instance'):
            return self._instance
    
    @instance.setter
    def instance(self, inst: instance):
        self._instance = inst
        
        # reset self.model.products
        for prod in self.model.products:
            for series_type in self.model.products[prod]:
                self.model.products[prod][series_type] = None
        
        for col in self._instance.params.columns:
            # check if column belongs to products
            if col.split('_')[0] != 'Params':
                product     = col.split('_')[0]
                series_type = col.split('_')[1]
                
                # if already one timeseries specified, total timeseries is defined by multiple series 
                # (will be summated later)
                if self.model.products[product][series_type] != None:
                    if not type(self.model.products[product][series_type]) == list:
                        self.model.products[product][series_type] = [self.model.products[product][series_type]]
                    self.model.products[product][series_type].append(col) 
                else:
                    self.model.products[product][series_type] = col
                    
            else:
                param = col.split('_')[1]
                self.model.input_params[param] = col
       
    @property
    def solver(self):
        return self._solver
    
    @solver.setter
    def solver(self, solver: str):
        try:
            pe.SolverFactory(solver)
        except:
            'Solver \'{}\' is not available.'.format(solver)
            
        self._solver = solver

    @property
    def solver_options(self):
        return self._solver_options
    
    @solver_options.setter
    def solver_options(self, s: dict):
        if s != None:
            self._solver_options = s
        else:
            from optimization.settings.solver_settings import settings
            self._solver_options = settings
            
    @property
    def config(self):
        return self._config
    
    @config.setter
    def config(self, config: dict):
        if config != None:
            self._config = config
        else:
            from optimization.settings.config import config
            self._config = config
    
    @property
    def results(self):
        return self._results
    
    @results.setter
    def results(self, r: results):
        self._results = r
        
    def optimize(self, save:bool=False, output_dir:str=None):
        for attr in ['model','instance']:
            if not hasattr(self, attr):
                raise AttributeError('Can not start optimization without \'{}\' being specified.'.format(attr))  
        
        if output_dir:
            output_dir += '/' +  datetime.now().strftime("%Y_%m_%d_%H-%M-%S")
                
            if not os.path.isdir(output_dir):
                os.makedirs(output_dir)
        
        
        opt = OperationalOptimizationInterface(self.model, self.instance, 
                                                solver = self.solver,
                                                solver_options = self.solver_options[self.solver],
                                                config = self.config)
            
        opt.optimize(output_dir)
        self.results = results(opt.results)
        
        if save:
            self.results.to_file(output_dir + '/results.pkl', mod=self.model, inst=self.instance)
            self.output_dir = output_dir
        return opt.results
    
    def reset(self):
        if hasattr(self, '_results'):
            delattr(self, '_results')
            
    def export_concrete_model(self, output_dir=None):

        output_dir += '/' +  datetime.now().strftime("%Y_%m_%d_%H-%M-%S")
            
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        
        opt = OperationalOptimizationInterface(self.model, self.instance, 
                                                solver = self.solver,
                                                solver_options = self.solver_options[self.solver],
                                                config = self.config)
            
        concrete_model=opt.create_concrete_model(output_dir)
        #TODO: Find way to export in reasonable way
        #self.results.to_file(output_dir + '/results.pkl', mod=self.model, inst=self.instance)
        #self.output_dir = output_dir
