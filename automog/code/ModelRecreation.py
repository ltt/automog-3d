# -*- coding: utf-8 -*-
"""
Created on Sun Apr 18 12:32:55 2021

@author: niels
"""

from optimization.engine import OperationalOptimizationInterface
from pathlib import Path

class ModelRecreation:
    def __init__(self,model=None,data=None, model_path=None, data_path=None, config=None):
        if model:
            self.model=model
        elif model_path:
            from optimization.interface.optimizer import model as model_opt
            self.model=model_opt.from_file(model_path)
        else:
            raise AttributeError("Please provide input data either as input dictionary for pyomo or as filepath to an input file.")
        if data:
            self.data=data
        elif data_path:
            from optimization.interface.optimizer import instance as instance
            self.data=instance.from_csv(data_path)
        else:
            raise AttributeError("Please provide input data either as input dictionary for pyomo or as filepath to an input file.")
       
        if not config:   
            from optimization.settings.config import config
            self.config = config
        else:
            self.config=config
        
        self.interface=OperationalOptimizationInterface(self.model,self.data,config=self.config)

    def create_model(self,output_dir,abstract_model=True):
        self.pyomo_model, self.input_dictionary=self.interface.create_pyomo_model(output_dir=output_dir, abstract=abstract_model)

#model_recreator=ModelRecreation(model_path=r"C:\Users\niels\Documents\LTT\automog\automog\data\Goderbauer2016\optimization\models\transfered_model-2021_04_07_09-51-18.pkl",data_path=r"C:\Users\niels\Documents\LTT\automog\automog\data\Goderbauer2016\optimization\instances\8760.CSV")
#model_recreator.create_model(abstract_model=True)
#concrete_model=model_recreator.pyo_model
