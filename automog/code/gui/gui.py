""" This script defines the ModEst GUI and all its Toplevel Window.
    The main method at the end is the entrance point for starting the GUI.
"""
### in-built- and site-packages
# GUI packages 
import ctypes
import tkinter as tk
from tkinter.filedialog import askdirectory, asksaveasfilename, askopenfilename
from tkinter.messagebox import askokcancel, askyesno, askyesnocancel, showinfo, showerror, showwarning
from tkinter.simpledialog import askinteger

# pandas packages: DataFrames, Tables
import pandas as pd
from pandastable import Table, TableModel, IndexHeader
import pandastable.config as pt_config
import pandastable.util as pt_util

# packages for visualization
import matplotlib
import matplotlib.pyplot as plt
from PIL import ImageTk, Image
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import mpl_toolkits.mplot3d.art3d as art3d
from matplotlib.patches import Circle

#matplotlib.use("TkAgg")

# other packages
import os
import sys
import logging
import copy
from datetime import datetime
import dill

# pyomo for solver availability
import pyomo.environ as pe

### own packages
# GUI packages
from gui.toolbox import Mainframe, Toplevel
from gui.helpers import get_rwth_colors
# modeling packages
from modeling import Clusterer
from modeling import AutoMoG, MOG_Component
from modeling import clean_operating_points, outlier_detection, reduce_operating_points
from modeling.Hinging_Hyperplane import hinging_hyperplane as hh
from modeling.FlowChart import FlowChart
# optimization packages
from optimization.interface import optimizer, model, instance, results
from optimization.engine import OperationalOptimizationInterface
from ModelRecreation import ModelRecreation
# initialize application
appid = 'LTT.AutoMoG' # arbitrary string
ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(appid)

# initialize logging
logging.getLogger().handlers.clear()
logging.shutdown()
logging.basicConfig(level=logging.INFO, format= '%(message)s')

class ModEst_GUI(Mainframe):
    """
    Graphical User Interface for the ModEst Project.
    """ 
    
    def __init__(self, parent: tk.Tk):
        """
        Instantiate an object of ModEst_GUI.        

        """
        # call parent constructor
        super().__init__(parent)
        
        # =====================================================================
        ### setup Window
        # title
        self.parent.title('Data-Driven Model Generation and Optimization of Multi-Energy Systems')
        # icon
        self.icon = os.path.dirname(os.path.abspath(__file__)) + '/utils/LTT.ico'
        self.parent.iconbitmap(self.icon)
        
        # =====================================================================
        ### setup Menu 
        menus = {'File': {'Read Operating Points from xlsx': self.read_working_directory,
                          'Read Setting File': lambda: [self.init_automog(),
                                                        self.startPlots(),
                                                        self.showTables()],
                          'Save Plots': self.save_plots, 
                          'Save Operating Points to xlsx': self.save_operating_points
                          },
                 'Select': {'Components': self.select_components,
                            'Sections':   lambda: [self.select_sections(),
                                                   self.startPlots(),
                                                   self.showTables()]                                             
                            },
                 'Model': {'Export to pkl': self.export_model,
                           'Export to xlsx': self.export_model_to_xlsx,
                           'Import': lambda: [self.import_model(),
                                              self.tab_parent.select(self.tabs['Model Generation']['tab'])] 
                           }
                 }
        
        self.setup_menu(menus)
        
        # =====================================================================
        ### setup Tabs
       
        # height (rows) and width (columns) fraction of frames, important: only integer values
        tabs = {'Model Generation':         {'rows':    [14,7,8],
                                             'columns': [2,3,5,5]},
                'Operational Optimization': {'rows':    [4,4,1,6,2],
                                             'columns': [8,8,20,15]}
                                  }
        
        self.setup_tabs(tabs)
        
        # =====================================================================
        ### setup Frames 
        frames = {'Model Generation': 
                                      {'Plot': {'row': 0, 'column': 0, 'columnspan': 3, 'rowspan': 1, 'relief': 'raised'},
                                       'SW':   {'row': 2, 'column': 0, 'columnspan': 2, 'rowspan': 1, 'relief': 'raised'},
                                       'SE':   {'row': 2, 'column': 2, 'columnspan': 1, 'rowspan': 1, 'relief': 'raised'},
                                       'NW':   {'row': 1, 'column': 0, 'columnspan': 2, 'rowspan': 1, 'relief': 'raised'},
                                       'NE':   {'row': 1, 'column': 2, 'columnspan': 1, 'rowspan': 1, 'relief': 'flat'},
                                       'E':    {'row': 0, 'column': 3, 'columnspan': 1, 'rowspan': 3, 'relief': 'flat'}
                                       },
                  'Operational Optimization':
                                      {'Aa1':  {'row': 0, 'column': 0, 'columnspan': 1, 'rowspan': 1, 'relief': 'flat'},
                                       'Ab1':  {'row': 0, 'column': 1, 'columnspan': 1, 'rowspan': 1, 'relief': 'flat'},
                                       'Aa2':  {'row': 1, 'column': 0,  'columnspan': 1, 'rowspan': 2, 'relief': 'flat'},
                                       'Ab2':  {'row': 1, 'column': 1,  'columnspan': 1, 'rowspan': 2, 'relief': 'flat'},
                                       'A3':   {'row': 3, 'column': 0,  'columnspan': 2, 'rowspan': 2, 'relief': 'raised'},
                                       'BC1':  {'row': 0, 'column': 2,  'columnspan': 2, 'rowspan': 2, 'relief': 'raised'},
                                       'BC2':  {'row': 2, 'column': 2,  'columnspan': 2, 'rowspan': 1, 'relief': 'flat'},
                                       'B3':   {'row': 3, 'column': 2,  'columnspan': 1, 'rowspan': 1, 'relief': 'raised'},
                                       'B4':   {'row': 4, 'column': 2,  'columnspan': 1, 'rowspan': 1, 'relief': 'flat'},
                                       'C3':   {'row': 3, 'column': 3,  'columnspan': 1, 'rowspan': 2, 'relief': 'flat'}
                                       }
                      }
            
        # height (rows) and width (columns) fraction of frames, important: only integer values
        sizes = {'Operational Optimization': 
                    {'B4': {'rows':    [5,5],
                            'columns': [4,4,5,1,8,1]}
                     }
                }
            
        self.setup_frames(frames, tab_definition=True, set_size = sizes)
        
        # =====================================================================
        ### read working directory
        self.read_working_directory()
        
        # =====================================================================
        ### set optimizer
        self.optimizer = optimizer()
        
        # =====================================================================
        ### setup Widgets 
        widgets = {}
        
            ### labels 
        widgets['labels'] = {
                  'Operating States Determination':
                      {'position': (self.frames['Model Generation']['NW'],0,0,5,'center'),
                       'font': self.utilities['standard_font'] +' 18 bold'},
                  'Manual Model Generation':
                      {'position': (self.frames['Model Generation']['SW'],0,0,2,'center'),
                       'font': self.utilities['standard_font'] +' 18 bold'},
                  'Automated Model Generation':
                      {'position': (self.frames['Model Generation']['SE'],0,0,3,'center'),
                       'font': self.utilities['standard_font'] +' 18 bold'},
                  'Model':
                      {'position': (self.frames['Operational Optimization']['Aa1'],0,0,3,'center'),
                       'font': self.utilities['standard_font'] +' 18 bold'},
                  'Model Information': 
                      {'position': (self.frames['Operational Optimization']['Aa1'],2,0,2,'w'),
                       'font': self.utilities['standard_font'] + self.utilities['standard_font_size'],
                       'text': 'Components : -\nSections: -\nProducts: -\nStorages: -',
                       'background': 'gray94'},
                  'Instance':
                      {'position': (self.frames['Operational Optimization']['Ab1'],0,0,2,'center'),
                       'font': self.utilities['standard_font'] +' 18 bold'},
                  'Instance Information': 
                      {'position': (self.frames['Operational Optimization']['Ab1'],2,0,2,'w'),
                       'font': self.utilities['standard_font'] + self.utilities['standard_font_size'],
                       'text': 'Timesteps: -\nSources: -\nSinks: -\nDemands: -\nExogenous Parameters: -',
                       'background': 'gray94'},
                  'Optimization Setup':
                      {'position': (self.frames['Operational Optimization']['Aa2'],0,0,3,'center'),
                       'font': self.utilities['standard_font'] +' 18 bold'},
                  'Optimization Results':
                      {'position': (self.frames['Operational Optimization']['Ab2'],0,0,3,'center'),
                       'font': self.utilities['standard_font'] +' 18 bold'}
                      }
    
            ### list for option menus
        self.lists = {'table_names': 
                          {'Model Generation':
                               ['Operating Points',
                               'Linear Sections',
                               'OBJECTIVE FUNCTION - parameters',
                               'WEIGHTED ERROR - parameters',
                               'time',
                               'delta target',
                               'target'],
                           'Operational Optimization':
                               ['Product Balance',
                                'Operational Costs']},
                      'linearization_methods': 
                          ['PYTHON-PWLF', 
                           'REBENNACK/KRASKO',
                           'KONG/MARAVELIAS'],
                      'clustering_methods':
                          ['Gaussian Mixture',
                           'Regression Clustering',
                           'KMeans Clustering',
                           'Agglomerative Clustering'],
                      'automog_mode' : 
                          ['WEIGHTED ERROR',
                           'OBJECTIVE FUNCTION'
                           ],
                      'information_criteria' : 
                          ['BIC', 'AIC', 'AIC_C'],
                      'solver':
                          ['GUROBI', 'CPLEX', 'GLPK']
                      }        
        
        # check solver availability
        for solver in self.lists['solver']:
            try:
                pe.SolverFactory(solver.lower()).available()
            except:
                self.lists['solver'].remove(solver)
                      
            ### define variables
            ### string variables for option menus
        widgets['optionMenus'] = {
                      # TAB: Model Generation 
                      'clustering': # select clustering method
                          {'init': self.lists['clustering_methods'][0], 'list': self.lists['clustering_methods'], 
                           'command': None,
                           'position': (self.frames['Model Generation']['NW'],2,2,'ew'),#
                           'label': ('Clustering Method',1,2,'ew')},
                      'component_Model Generation': # select component to be plotted
                          {'init': MOG_Component.names[0], 'list': MOG_Component.names, 
                           'command': self.updateWidgets,
                           'position': (self.frames['Model Generation']['NE'],1,0,'ew'),
                           'label': ('Plot Component',0,0,'ew')},
                      'operating_state': # select operating state to be clustered 
                          {'init': list(self.operating_points[MOG_Component.names[0]].keys())[0] , 'list': list(self.operating_points[MOG_Component.names[0]].keys()), 
                           'command': None,
                           'position': (self.frames['Model Generation']['NW'],2,0,'ew'),
                           'columnspan': 2,
                           'label': ('Operating State',1,0,'ew')},
                      'table_Model Generation': # select table to be shown
                          {'init': self.lists['table_names']['Model Generation'][0], 
                           'list': self.lists['table_names']['Model Generation'], 
                           'command': self.showTables,
                           'position': (self.frames['Model Generation']['NE'],1,1,'ew'),
                           'label': ('Select Table',0,1,'ew')},
                      'linearization manual': # select pwlf package for manual model generation
                          {'init': self.lists['linearization_methods'][0], 'list': self.lists['linearization_methods'], 
                           'command': None,
                           'position': (self.frames['Model Generation']['SW'],2,0,'ew'),
                           'label': ('PWLF Package',1,0,'ew')},
                      'automog mode': # select mode for automated model generation
                          {'init': self.lists['automog_mode'][0], 'list': self.lists['automog_mode'], 
                           'command': self.enable_disable_widgets,
                           'position': (self.frames['Model Generation']['SE'],2,0,'ew'),
                           'label': ('Mode',1,0,'ew')},
                      'solver_modeling':
                          {'init': self.lists['solver'][0], 'list': self.lists['solver'], 
                           'command': self.synch_solver,
                           'position': (self.frames['Model Generation']['SE'],4,0,'ew'),
                           'label': ('Solver',3,0,'ew'),
                           'label_bg': 'gray94'},
                      'information criterion': # select information criterion to be used during automated model generation
                          {'init': self.lists['information_criteria'][0], 'list': self.lists['information_criteria'], 
                           'command': None,
                           'position': (self.frames['Model Generation']['SE'],2,1,'ew'),
                           'label': ('Information criterion',1,1,'ew')},
                      'linearization auto': # select pwlf package for automated model generation
                          {'init': self.lists['linearization_methods'][0], 'list': self.lists['linearization_methods'], 
                           'command': None,
                           'position': (self.frames['Model Generation']['SE'],2,2,'ew'),
                           'label': ('PWLF Package',1,2,'ew')},
                    
                      # TAB: Operational Optimization 
                      'solver':
                          {'init': self.lists['solver'][0], 'list': self.lists['solver'], 
                           'command': self.synch_solver_modeling,
                           'position': (self.frames['Operational Optimization']['Aa2'],1,1,'ew'),
                           'label': ('Solver',1,0,'ew'),
                           'label_bg': 'gray94'},
                      'component_Operational Optimization':
                          {'init': '', 'list': [''], 
                           'command': self.startPlots,
                           'position': (self.frames['Operational Optimization']['B4'],1,1,'ew'),
                           'label': ('Plot Component',0,1,'ew')},
                      'product':
                          {'init': '', 'list': [''], 
                           'command': self.startPlots,
                           'position': (self.frames['Operational Optimization']['B4'],1,0,'ew'),
                           'label': ('Plot Product',0,0,'ew')},
                      'table_Operational Optimization':
                           {'init': self.lists['table_names']['Operational Optimization'][0], 
                           'list': self.lists['table_names']['Operational Optimization'], 
                           'command': self.showTables,
                           'position': (self.frames['Operational Optimization']['B4'],1,2,'ew'),
                           'label': ('Select Table',0,2,'ew')}
                      }
        
            ### define checkbuttons 
        widgets['checkButtons'] = {
            # TAB: Model Generation 
            'set manually': # set Number of sections (all) manually for manual model generation
                       {'init': 0,
                        'command': lambda: [self.enable_disable_widgets(), 
                                            self.startPlots()],
                        'position': (self.frames['Model Generation']['SW'],3,1,'ew')},
            'full reset':
                       {'init': 0,
                        'command': None,
                        'position': (self.frames['Model Generation']['NW'],3,1,'ew')},
                 
            # TAB: Operational Optimization 
            'cumulated':
                        {'init': 0,
                         'command': lambda: [self.showTables(),
                                             self.startPlots()],
                         'position': (self.frames['Operational Optimization']['BC2'],0,0,'ew')},
            'demand':
                        {'init': 0,
                         'command': lambda: [self.showTables(),
                                             self.startPlots()],
                         'position': (self.frames['Operational Optimization']['BC2'],0,1,'ew')},
            'compensation':
                        {'init': 0,
                         'command': lambda: [self.showTables(),
                                             self.startPlots()],
                         'position': (self.frames['Operational Optimization']['BC2'],0,2,'ew')},
            'price':
                        {'init': 0,
                         'command': lambda: [self.showTables(),
                                             self.startPlots()],
                         'position': (self.frames['Operational Optimization']['BC2'],0,3,'ew')},
                    
                
                  }

            ### define buttons
        widgets['buttons'] = {
                   # TAB: Model Generation
                   'Reset Clustering': # reset clustering
                       {'label': 'reset',
                        'command' : self.reset_clustering,
                        'position': (self.frames['Model Generation']['NW'],3,0,1,1,'ew')},
                   'Start Clustering': # start clustering
                       {'command' : self.start_clustering,
                        'position': (self.frames['Model Generation']['NW'],3,2,1,3,'ew')},
                       
                   'Start Linearization': # start manual model generation
                       {'command' : lambda: [self.manual_linearization(),
                                             self.showTables()],
                        'position': (self.frames['Model Generation']['SW'],5,0,1,2,'ew')},
                       
                   'reset': # reset automated model generation
                       {'command' : lambda: [self.mogger.reset(),
                                             self.updateWidgets()],
                        'position': (self.frames['Model Generation']['SE'],6,0,1,1,'ew')},
                   'next iteration': # proceed next iteration of automated model generation
                       {'command' : lambda: [self.mogger.update(icrit=self.stringVars['information criterion'].get(),
                                                                lin_method=self.stringVars['linearization auto'].get(),
                                                                mode=self.stringVars['automog mode'].get(),
                                                                acc=self.get_entry_content('Allowed relative error / [%]', target_type=float)), 
                                             self.show_iteration_frame(),
                                             self.mogger.next_iter(),
                                             self.iteration_frame.parent.destroy(),
                                             self.updateWidgets()],
                        'position': (self.frames['Model Generation']['SE'],6,1,1,1,'ew')}, 
                   'converge': # converge automated model generation
                       {'command' : lambda: [self.mogger.update(icrit=self.stringVars['information criterion'].get(),
                                                                lin_method=self.stringVars['linearization auto'].get(),
                                                                mode=self.stringVars['automog mode'].get(),
                                                                acc=self.get_entry_content('Allowed relative error / [%]', target_type=float)),
                                             self.mogger.reset(),
                                             self.show_iteration_frame(),
                                             self.mogger.converge(),
                                             self.iteration_frame.parent.destroy(),
                                             self.updateWidgets()],
                        'position': (self.frames['Model Generation']['SE'],6,2,1,1,'ew')},
                       
                    'Data Reduction ': # starts data reduction
                     {'command': self.reduce_data,
                      'position':(self.frames['Model Generation']['NE'],2,0,1,2,'ew'),
                      'font size': 15},
                     
                    u'Transfer to optimization \u27a2': # transfer model to operational optimization mode
                       {'command': self.transfer_to_optimization,
                        'position':(self.frames['Model Generation']['NE'],3,0,1,2,'ew'),
                        'font size': 15},
                       
                      
                       
                    # TAB: Operational Optimization 
                    'Load model':
                        {'command': self.load_model,
                         'position': (self.frames['Operational Optimization']['Aa1'],1,0,1,1,'ew')},
                    'Settings_model':
                        {'label': 'Settings',  
                         'command': self.set_ModelSettings,
                         'position': (self.frames['Operational Optimization']['Aa1'],1,1,1,1,'ew')},
                    'Flow_chart':
                        {'label': 'Flow Chart',  
                         'command': self.show_FlowChart,
                         'position': (self.frames['Operational Optimization']['Aa1'],1,2,1,1,'ew')},
                    'Load instance':
                        {'command': self.load_instance,
                         'position': (self.frames['Operational Optimization']['Ab1'],1,0,1,1,'ew')},
                    'Settings_instance':
                        {'label': 'Settings',  
                         'command': self.set_InstanceSettings,
                         'position': (self.frames['Operational Optimization']['Ab1'],1,1,1,1,'ew')},
                     'Settings_solver':
                      {'label': 'Settings', 
                       'command': self.set_SolverSettings,
                       'position': (self.frames['Operational Optimization']['Aa2'],1,2,1,1,'ew')},
                    'Start Optimization':
                        {'command': self.start_optimization,
                         'position': (self.frames['Operational Optimization']['Aa2'],2,0,1,3,'ew')},
# =============================================================================
#                     'Export Pyomo Model':
#                         {'command': self.export_PyomoModel,
#                          'position': (self.frames['Operational Optimization']['Aa2'],3,0,1,3,'ew')},
# =============================================================================
                    u'\u276e': 
                      {'command': lambda: [self.updateTimeStep(-1)],
                       'position': (self.frames['Operational Optimization']['B4'],1,3,1,1,'ew')},
                    u'\u276f': 
                      {'command': lambda: [self.updateTimeStep(+1)],
                       'position': (self.frames['Operational Optimization']['B4'],1,5,1,1,'ew')},
                    'Load Results':
                        {'command': self.load_results,
                         'position': (self.frames['Operational Optimization']['Ab2'],5,0,1,3,'ew')}
                   }
            
            ### define scales 
        widgets['scales'] = {
                  # TAB: Model Generation 
                  'Number of clusters': 
                      {'init': 1, 
                       'ticks': (1,5,1,2),
                       'command': None,
                       'position': (self.frames['Model Generation']['NW'],2,3,'ew'),
                       'label': (1,3,1,'center','ew')},
                  'Number of sections (all)': 
                      {'init': 1, 
                       'ticks': (1,20,1,5),
                       'command': self.startPlots,
                       'position': (self.frames['Model Generation']['SW'],2,1,'ew'),
                       'label': (1,1,1,'center','ew')},
                
                  # TAB: Operational Optimization
                  'Time Step': 
                      {'init': 0, 
                       'ticks': (0,0,1,1),
                       'command': lambda x=None: [self.showTables(),
                                                  self.startPlots()],
                       'position': (self.frames['Operational Optimization']['B4'],1,4,'new'),
                       'label': (0,3,3,'center','ew')}
                 }
             
            ### define entries
        widgets['entries'] = {
                   # TAB: Model Generation
                   'Allowed relative error / [%]': 
                        {'init': 0.5, 
                         'command': None,
                         'position': (self.frames['Model Generation']['SE'],4,1,'ew'),
                         'label': (3,1,1,'w','ew'),
                         'label_bg': 'gray94'},
                    'Maximum Runtime':
                        {'init': 600, 
                         'command': None,
                         'position': (self.frames['Model Generation']['NW'],2,4,'ew'),
                         'label': (1,4,1,'w','ew')},
                    'Allowed input space reduction / [%]':
                        {'init': 0,
                        'command': None,
                        'position': (self.frames['Model Generation']['SW'], 4, 1, 'ew'),
                        'label': (4, 0, 1, 'w', 'ew'),
                        'label_bg': 'gray94'},
                        }
        
            ### define scrolled text 
        widgets['scrolled_texts'] = {
                        'OPT_LOG':
                             {'height': 28,
                              'width': 83,
                              'position': (self.frames['Operational Optimization']['A3'],0,0,'we')}
                         }
            ### create widgets
        for w in widgets:
            # the corresponding functions are defined in gui.toolbox
            getattr(self, 'create_{}'.format(w))(widgets[w])
            
        # =====================================================================
        ### adapt appearance if not all modes for automated model generation are available     
        for mode in self.mogger.mode_availability:
            if self.mogger.mode_availability[mode] == 0:
                for table_name in self.lists['table_names']['Model Generation']:
                    if mode in table_name:
                        self.optionMenus['table_Model Generation']['menu'].entryconfigure(table_name, state = "disabled")   
        inserted = 0
        for mode in self.lists['automog_mode']:
            if self.mogger.mode_availability[mode] == 0:
                self.optionMenus['automog mode']['menu'].entryconfigure(mode, state = "disabled")
            else:
                if not inserted:
                    self.stringVars['automog mode'].set(mode)
                    inserted = 1
        
        self.enable_disable_widgets()
        
        # =====================================================================
        ### layout fixes
        # Vertical spacing
        self.frames['Model Generation']['SE'].grid_rowconfigure(5, minsize=10)
        self.frames['Model Generation']['SW'].grid_rowconfigure(4, minsize=37)
        
        # expand gui widgets over whole frame for specified frames -> looks nicer
        expand = {'Model Generation': ['Plot','NW','NE','SW','SE'],
                  'Operational Optimization': ['A3','BC2']}
        for tab in expand:
            for f in expand[tab]:
                frame = self.frames[tab][f]
                for col in range(frame.grid_size()[0]):
                    frame.grid_columnconfigure(index = col, weight = 1)
                    #frame.grid_rowconfigure(index = 1, weight = 1)
                    
        # =====================================================================
        ### GUI is completely built
        self.utilities['GUI_built'] = True
        self.startPlots()
        self.showTables()
        self.Clusterer=Clusterer()
        
        
    def read_working_directory(self):
        """ Select a working directory and read its data. """
        # =====================================================================
        ## start read mode
        self.utilities['read_mode'] = 1
        
        # =====================================================================
        # get last working directory  
        if not 'lwd.txt' in os.listdir(os.path.dirname(__file__) + '/utils'):
            initialdir = 'data'
        else:
            with open(os.path.dirname(__file__)+'/utils/lwd.txt','r') as f:
                initialdir = f.read()
        
        # =====================================================================
        # dialog: select working directory 
        working_file= askopenfilename(parent = self.parent,
                                      title='Select Operating Points',
                                      initialdir = initialdir,
                                      initialfile = 'operating_points.xlsx',
                                      filetypes = [('xlsx-files (*.xlsx)','*.xlsx')],
                                      defaultextension='.xlsx')
        
        # =====================================================================
        ## if working directory selected
        if working_file!='':
            cwd = os.path.dirname(working_file)
            
            # overwrite last working directory
            with open(os.path.dirname(__file__) + '/utils/lwd.txt','w') as f:
                f.write(cwd)
                
            # set working, output directory and working file
            self.working_directory = cwd
            self.output_dir = self.working_directory + '/output'
            if not os.path.exists(self.output_dir):
                os.makedirs(self.output_dir)
            self.working_file=working_file
            
            # read and preprocess operating points
            operating_points = {}
            operating_points_all = pd.ExcelFile(self.working_file)
            for comp in operating_points_all.sheet_names:
                temp_single_state = {0: pd.read_excel(operating_points_all, comp)}
                comp_frame=pd.read_excel(operating_points_all, comp)
                if 'Operating State' in comp_frame.columns:
                    operating_points[comp]={}
                    read_states=comp_frame.loc[:,'Operating State'].unique()
                    for state in read_states:
                        operating_points[comp][state]=comp_frame[(comp_frame.loc[:,'Operating State'] == state).to_frame().any(axis=1)].drop('Operating State',
                                                                                                                                             axis=1)
                else:
                    operating_points[comp]=temp_single_state
            
            self.operating_points = self.preprocess_data(operating_points)

            # initialize automog functionalities
            self.init_automog()
            
            # read storage file if available
            self.read_storages()
            
            # update GUI appearence if it has already been completely built (in case a new working directory should be read)
            if self.utilities['GUI_built']:
                self.updateWidgets()
        
        # if no working directory selected
        else: 
            # if GUI has not been completely built
            if not self.utilities['GUI_built']:
                self.parent.destroy()
                sys.exit()
        # end read mode
        self.utilities['read_mode'] = 0
    
    def read_storages(self):
        storage_file = self.working_directory + '/storages.xlsx'
        if os.path.isfile(storage_file):
            storages = pd.read_excel(storage_file, index_col = 'storage')
            self.storages=storages.transpose().to_dict()
        else:
            self.storages={}
            
    def read_supplementary_input(self):
        # read component parameters for optimization if available
        params_file = self.working_directory + '/component_parameters.xlsx'
        
        if os.path.isfile(params_file):
            params = pd.ExcelFile(params_file)
            comp_params = {}
            for comp in params.sheet_names:
                comp_params[comp] = pd.read_excel(params, comp, skiprows=0, header=1,
                                                  converters={'r_length':eval,
                                                              'rr':eval})
            # write attributes to self.mogger.components
            for subcomp in self.mogger.components:
                comp = subcomp.split('_')[0]
                
                if not comp in comp_params:
                    logging.warning(f'No supplementary input available for component {comp}.' + \
                                    ' Please check completeness of the component_parameters.xlsx file.')
                else:
                    # overwrite opt_params
                    self.mogger.components[subcomp].opt_params = {} 
                    for attr in comp_params[comp].columns:
                        value = eval(str(comp_params[comp][attr][0]))
                        self.mogger.components[subcomp].opt_params[attr] = value
    
    def preprocess_data(self, operating_points):
        """
        Remove 0-rows, checks if given operating points need to be furhter
        preprocessed and initiates preprocessing.  

        Parameters
        ----------
        operating_points : dict
            Operating points to check.
            {component: {operating_state: pd.DataFrame}}

        Returns
        -------
        operating_points : dict
            Preprocessed operating points. 
            {component: {operating_state: pd.DataFrame}}

        """
        
        for comp in operating_points:
            # remove all-zero rows
            operating_points[comp][0] = operating_points[comp][0].loc[(operating_points[comp][0] != 0).all(axis=1), :]
            # if not all columns of dtype float64 preprocessing will be executed
            if (operating_points[comp][0].dtypes != 'float64').any():
                operating_points[comp][0] = clean_operating_points(operating_points[comp][0]) 
        
        #define and initialize variables for reset
        global StepBack_operating_points
        StepBack_operating_points=list()
        global old_states
        old_states=list()
        global initial_points
        initial_points=copy.deepcopy(operating_points)
        return operating_points
        
    def reduce_data(self):
        maximum = 0
        
        # if more than 100 data points available optional preprocessing can be executed if desired
        for comp in self.operating_points:
            for state in self.operating_points[comp]:
                number_of_points = len(self.operating_points[comp][state])
                if number_of_points > maximum:
                    maximum = number_of_points
                 
        if askyesno('Data reductiom',
                    f'The \'operating_points.xlsx\' file contains up to {maximum} operating points. ' +\
                    'Data reduction improves computability. ' +\
                    'Do you want to reduce your input data?'):
                
            n_clusters = askinteger('Number of operating points.',
                                    'Please select the desired number of operating points.',
                                    initialvalue=maximum)
            
            # reduce operating_points
            for comp in self.operating_points:
                for state in self.operating_points[comp]:
                    self.operating_points[comp][state] = reduce_operating_points(self.operating_points[comp][state], 
                                                                                 n_clusters = n_clusters)
        
        self.mogger = AutoMoG(self, 
                              operating_points=self.operating_points,
                              wdir=self.working_directory)
        
        self.updateWidgets()

    def read_selected_components(self, selected_components: dict):
        """
        Filter data by selected components. Reinitialize self.mogger. 

        Parameters
        ----------
        selected_components : dict
            Dictionary with component names as key and boolean as value. 
            Value is 1 if component is selected and 0 otherwise. 

        Returns
        -------
        None.

        """

        operating_points_filtered = copy.deepcopy(self.operating_points)
        
        component_names_filtered = copy.deepcopy(MOG_Component.__names__)
        
        for comp_name, comp_select in selected_components.items():
            MOG_Component.__all__[comp_name].select = comp_select
            if not comp_select:
                del operating_points_filtered[comp_name]
                component_names_filtered.remove(comp_name)

        self.mogger = AutoMoG(self, 
                              operating_points = operating_points_filtered,
                              wdir=self.working_directory)

        self.updateOptionMenu('component_Model Generation', component_names_filtered, 
                              component_names_filtered[0], self.updateWidgets)
        
        self.startPlots()
        self.showTables()
        
    def init_automog(self):
        if hasattr(self, 'settings'):
            delattr(self, 'settings')
            
        settings_dir = self.working_directory + '/settings'
        
        # if it is a new working directory --> read_mode
        if self.utilities['read_mode'] == 1:
            
            # get all setting-files in directory
            if os.path.isdir(settings_dir):
                settings = os.listdir(settings_dir)
            else:
                settings = []
            
            # if at least one setting file exists
            if len(settings) > 0:
                # determine setting_file
                # only one possible setting in directory
                if len(settings) == 1:
                    setting_file = settings_dir + '/' + settings[0]
                elif 'settings.xlsx' in settings:
                    setting_file = settings_dir + '/settings.xlsx'
                # multiple possible settings in directory
                elif len(settings) > 1:
                    showinfo('Manual setting selection necessary.', 'More than one .xlsx-file specifying the model settings have been found.'\
                             + ' Additionally, no default setting-file (\'settings.xlsx\') was found. Please select the desired setting-file manually.')
                    
                    setting_file = askopenfilename(parent = self.parent,
                                          title = 'Select settings',
                                          initialdir = settings_dir,
                                          initialfile = 'settings.xlsx',
                                          filetypes = [('Excel-files (*.xlsx)','*.xlsx')],
                                          defaultextension='.xlsx')
                # read setting_file
                self.settings = pd.read_excel(setting_file, index_col = 'component')
            
            # no setting file in directory: self.settings will not be defined
            else:
                pass
            
        # same working directory
        else:
            if not os.path.isdir(settings_dir):
                showwarning('No setting-file available for current working directory.')
            else:
                setting_file = askopenfilename(parent = self.parent,
                                               title = 'Select settings',
                                               initialdir = self.working_directory + '/settings',
                                               initialfile = 'settings.xlsx',
                                               filetypes = [('Excel-files (*.xlsx)','*.xlsx')],
                                               defaultextension='.xlsx')
                if setting_file != '':
                    self.settings = pd.read_excel(setting_file, index_col = 'component')
                else:
                    return
        
        # with components create model generator
        self.mogger = AutoMoG(self, 
                              operating_points=self.operating_points,
                              wdir=self.working_directory)
    
    def init_optimizer(self):
        self.optimizer.model    = model(self.mogger.components, self.mogger.curves, self.mogger.sections, 
                                        storages=self.storages,
                                        name = os.path.basename(self.working_directory)) 
        
        self.optimizer.instance = instance(self.mogger.params_opt)
        
        self.updateWidgets()
                
    def enable_disable_widgets(self, *kwargs):
        """ Update GUI appearence depending on selection of 'set manually'. """
        if self.intVars['set manually'].get() == 1:
            self.scales['Number of sections (all)'].configure(state = 'normal', fg = 'black')
        else:
            self.scales['Number of sections (all)'].configure(state = 'disable', fg = 'gray62')
    
    def enable_disable_op_opt(self, *kwargs):
        for frame in self.frames['Operational Optimization']:
            for child in self.frames['Operational Optimization'][frame].winfo_children():
                try:
                    if str(child.cget('state')) == 'disable':
                        child.configure(state='normal')
                    elif str(child.cget('state')) == 'normal':
                        child.configure(state='disable')
                except:
                    continue
                
    def updateWidgets(self,*args):
        # create new Option Menu: component_Model Generation
        if MOG_Component.names != self.optionMenus['component_Model Generation'].items:
            current = self.stringVars['component_Model Generation'].get()
            if current in MOG_Component.names:
                init = current
            else:
                init = MOG_Component.names[0]
                
            self.updateOptionMenu('component_Model Generation', MOG_Component.names, init, self.updateWidgets)
    
        # check mode availabilities
        current_mode   = self.stringVars['automog mode'].get()
        current_table  = self.stringVars['table_Model Generation'].get()
        available_mode = None
        
        # check all modes
        for mode in self.lists['automog_mode']:
            if self.mogger.mode_availability[mode] == 0:
                # disable mode selection
                self.optionMenus['automog mode']['menu'].entryconfigure(mode, state = "disabled")
                
                # disable corresponding tables
                for table_name in self.lists['table_names']['Model Generation']:
                    if mode in table_name:
                        self.optionMenus['table_Model Generation']['menu'].entryconfigure(table_name, state = "disabled")
                
            else:
                # enable mode selection
                self.optionMenus['automog mode']['menu'].entryconfigure(mode, state = "active")
                
                # set first available
                if not available_mode:
                    available_mode = mode
                    
                # enable corresponding tables
                for table_name in self.lists['table_names']['Model Generation']:
                    if mode in table_name:
                        self.optionMenus['table_Model Generation']['menu'].entryconfigure(table_name, state = "active")
        
        # update mode if necessary
        if not self.mogger.mode_availability[current_mode]:
            self.stringVars['automog mode'].set(available_mode)
            
        # update shown table if necessary
        if self.optionMenus['table_Model Generation']['menu'].entrycget(current_table, 'state') == 'disabled':
            self.stringVars['table_Model Generation'].set(self.lists['table_names']['Model Generation'][0])
        
        #update operating states 
        component=self.stringVars['component_Model Generation'].get()
        current_states=list(self.operating_points[component].keys())
        self.updateOptionMenu('operating_state', current_states, 0, None)
        
        # insert results if available
        if hasattr(self.optimizer, 'results'):
            from_,to,resolution,tickinterval = (min(self.optimizer.results.balance_frames),
                                                max(self.optimizer.results.balance_frames),
                                                1,
                                                len(self.optimizer.results.balance_frames)//5)
            self.scales['Time Step'].configure(from_=from_, to=to, resolution=resolution, tickinterval=tickinterval)
            self.scales['Time Step'].set(0)
                
        # insert model if available
        if hasattr(self.optimizer, 'model'):
            self.updateOptionMenu('component_Operational Optimization', 
                                  sorted(self.optimizer.model.components)+sorted(self.optimizer.model.storages),
                                  sorted(self.optimizer.model.components)[0], 
                                  self.startPlots)
        
        # update model and instance information labels
        # TODO: do this with help of the textvariable option of labels
        labels = {}
        if hasattr(self.optimizer, 'model'):
            labels['Model Information']=  {
                'position': (self.frames['Operational Optimization']['Aa1'],2,0,1,'w'),
                'font': self.utilities['standard_font'] + self.utilities['standard_font_size'],
                'text': 'Components : {}\nSections: {}\nProducts: {}\nStorages: {}'.format(self.optimizer.model.ncomponents,
                                                                                           self.optimizer.model.nsections,
                                                                                           self.optimizer.model.nproducts,
                                                                                           self.optimizer.model.nstorages),
                'background': 'gray94'}
            
        if hasattr(self.optimizer, '_instance'):
            labels['Instance Information'] =  {
                'position': (self.frames['Operational Optimization']['Ab1'],2,0,1,'w'),
                'font': self.utilities['standard_font'] + self.utilities['standard_font_size'],
                'text': 'Timesteps: {}\nSources: {}\nSinks: {}\nDemands: {}\nExogenous Parameters: {}'.format(
                    self.optimizer.instance.ntimesteps,
                    self.optimizer.instance.nsources,
                    self.optimizer.instance.nsinks,
                    self.optimizer.instance.ndemands,
                    self.optimizer.instance.nexparams),
                'background': 'gray94'}
            
            self.updateOptionMenu('product', list(self.optimizer.model.products.keys()),
                                  list(self.optimizer.model.products.keys())[0], self.startPlots)
        
        self.create_labels(labels)

        self.startPlots()
        self.showTables()
        
    def updateTables(self):
        """ Update all table frames. """
        if self.mogger.iter == -1:
            nrows = 1
        else:
            nrows = self.mogger.iter 
        
        self.table_frames = {}
        
        # concatenate all operating states in one frame
        operating_points = pd.concat(self.mogger.operating_points[self.stringVars['component_Model Generation'].get()])
        
        # flatten sections dictionary 
        sections = pd.DataFrame.from_dict(self.mogger.sections).fillna(0).astype(int)
        sections.index.name = 'state'
        
        # optimization parameters
        params_opt = self.mogger.params_opt
        
        # weighting factors
        weighting_factors = pd.DataFrame(self.mogger.weighting_factors,index=['weighting_factors']).reindex(sorted(self.mogger.weighting_factors.keys()),  
                                                                                                            axis=1)
        
        # model generation tables
        sol_time, delta_target, target = self.mogger.store['solution_time'].head(nrows),\
                                         self.mogger.store['delta_target'].head(nrows),\
                                         self.mogger.store['target'].head(nrows)

        self.table_frames['Model Generation'] = [operating_points, sections, 
                                                 params_opt, weighting_factors,
                                                 sol_time, delta_target, target]
        
        if hasattr(self.optimizer, 'results'):
            self.table_frames['Operational Optimization'] = [self.optimizer.results.balance_frames[self.scales['Time Step'].get()],
                                                             self.optimizer.results.cost_frames[self.scales['Time Step'].get()]]
        else:
            self.table_frames['Operational Optimization'] = [pd.DataFrame(),
                                                             pd.DataFrame()]
                         
        self.table_map = {tab: dict(zip(self.lists['table_names'][tab], self.table_frames[tab])) 
                          for tab in self.table_frames}
    
    def showTables(self,*args):
        # update table frames
        self.updateTables()
        
        for tab in self.table_map:
            if not isinstance(self.table_map[tab][self.stringVars['table_{}'.format(tab)].get()], pd.DataFrame):
                logging.info('For selected table is no data available.')
            
        # if table not yet created
        if not hasattr(self,'tables'): 
            # create table
            self.tables = {'Model Generation': 
                               Table(self.frames['Model Generation']['E'], 
                                     dataframe=self.table_map['Model Generation'][self.stringVars['table_Model Generation'].get()]),
                           'Operational Optimization':
                               Table(self.frames['Operational Optimization']['C3'], 
                                     dataframe=self.table_map['Operational Optimization'][self.stringVars['table_Operational Optimization'].get()])
                           }
                
            for tab in self.tables.values():
                tab.grid()
                
                # table configuration
                pt_config.apply_options({'fontsize':  7,
                                         'cellwidth':60,
                                         },
                                        tab)
                if 'standard_font' in self.utilities: # if standard_font manually set
                    if self.utilities['standard_font'] in pt_util.getFonts(): # if possible to set standard_font in table
                        pt_config.apply_options({'font': self.utilities['standard_font']},tab)       
                    else:
                        logging.info('Unable to set Font in Table. Used default Font instead.')
                tab.show()
                tab.showIndex()
            
        # update table 
        else:
            for tab_name, tab in self.tables.items():
                tab.updateModel(TableModel(copy.deepcopy(self.table_map[tab_name][self.stringVars['table_{}'.format(tab_name)].get()])))
                tab.rowindexheader = IndexHeader(tab.parentframe, 
                                                 tab)
                tab.rowindexheader.grid(row=0,column=0,rowspan=1,sticky='news')
                #self.adjustManualColumnWidths()
        for tab in self.tables.values():
            tab.redraw()
    
    def startPlots(self,*args):
        """ start setup or plot of figure"""
        # if figure already created, change plot; otherwise first create figure 
        if not hasattr(self,'fig'):
            self.setupFigures()
        self.plotFigures()
        
    def setupFigures(self):
        """ set up figure in north frame"""
        
        # setup figure 
        matplotlib.style.use('fast')
        self.utilities['dpi']=75

        self.fig = {'Model Generation - Components': 
                        {'tab': 'Model Generation',
                         'frame': 'Plot'},
                    'Operational Optimization - Components':
                        {'tab': 'Operational Optimization',
                         'frame': 'B3'},
                    'Operational Optimization - Results':
                        {'tab': 'Operational Optimization',
                         'frame': 'BC1'}
                    }
        
        # create figs
        for fig in self.fig:
            tab   = self.fig[fig]['tab']
            frame = self.fig[fig]['frame']
            self.fig[fig]['fig'] = Figure(figsize = (self.calculateFrameGeometry(tab, frame)['width']/self.utilities['dpi'],
                                                     self.calculateFrameGeometry(tab, frame)['height']/self.utilities['dpi']))
        
        if 'standard_font' in self.utilities: # if standard_font manually set
            try: # if possible to set standard_font in matplotlib
                plt.rcParams.update({'font.family': self.utilities['standard_font']})
            except:
                logging.info('Unable to set Font in Plot. Used default Font instead.')
        
        self.plot_canvas = {}
        for fig in self.fig:
            frame = self.frames[self.fig[fig]['tab']][self.fig[fig]['frame']]
            self.plot_canvas[fig] = FigureCanvasTkAgg(self.fig[fig]['fig'], 
                                                      master = frame)
            self.plot_canvas[fig].get_tk_widget().pack(expand=1)
        
    def plotFigures(self):
        """ plot component """
        self.axes = {} 
        for fig_name in self.fig:
            # get and clear figure
            fig = self.fig[fig_name]['fig']
            fig.clf()
            
            # component plot
            if fig_name.split('- ')[1] == 'Components':
                
                # get component to plot
                comp_to_plot = self.stringVars['component_{}'.format(fig_name.split(' -')[0])].get()

                # operational optimization
                if fig_name == 'Operational Optimization - Components':
                    # create axes object
                    # self.axes[fig_name] = fig.add_axes([0.15, 0.15, 0.65, 0.75])
                        
                    if hasattr(self.optimizer, 'model'):
                        production_unit = comp_to_plot in self.optimizer.model.components
                        storage_unit    = comp_to_plot in self.optimizer.model.storages
                        
                        if production_unit:
                            any_state = list(self.operating_points[comp_to_plot].keys())[0]
                            if len(self.operating_points[comp_to_plot][any_state].columns) == 2:
                                self.axes[fig_name] = fig.add_axes([0.15, 0.15, 0.65, 0.75])
                            else:
                                self.axes[fig_name] = fig.add_axes([0.15, 0.15, 0.65, 0.75], projection='3d')

                            
                            # get component object
                            comp = self.optimizer.model.components[comp_to_plot]
                            
                            # get current operating point if available
                            if hasattr(self.optimizer, 'results'):
                                parent_comp = comp_to_plot.split('_')[0]
                                T = self.scales['Time Step'].get()
                                if len(comp.inputs) == 1:
                                    y_param  = comp.inputs[0]
                                    x_param = comp.outputs[0]
                                    point_frame = self.optimizer.results.balance_frames[T]
                                    x = point_frame.loc[parent_comp, x_param]
                                    y = -1*point_frame.loc[parent_comp, y_param]
                                    point = (x,y)
                                else:
                                    x_param = comp.outputs[0]
                                    z_param = comp.inputs[0] 
                                    y_param = comp.inputs[1]
                                    data_in  = self.optimizer.results.all_['INPUT_SUB_MULTIVARIATE_LIN_EL_AUX'].groupby(level=[0,1]).get_group((parent_comp,comp.name)).groupby(level=[4,5]).sum()
                                    data_out = self.optimizer.results.all_['OUTPUT_SUB_MULTIVARIATE_TOTAL'].groupby(level=[0,1]).get_group((parent_comp,comp.name)).groupby(level=[3]).sum() 
                                    x=data_in[0,T] 
                                    z=data_out[T]
                                    y=data_in[1,T]
                                    point = (x,y,z)
                            else:
                                point = None
                            
                            # plot component
                            self.plotComponent(fig_name, self.optimizer.model, comp_to_plot, plot_point = point, plot_ops = False)
                        
                        elif storage_unit:
                            self.axes[fig_name] = fig.add_axes([0.15, 0.15, 0.65, 0.75], projection='3d')
                            self.axes[fig_name].view_init(elev=10, azim=-45)
                            self.plotStorage(fig_name, comp_to_plot)
                                  
                # model generation
                else:
                    any_state = list(self.operating_points[comp_to_plot].keys())[0]
                    if len(self.operating_points[comp_to_plot][any_state].columns) == 2:
                        self.axes[fig_name] = fig.add_axes([0.15, 0.15, 0.65, 0.75])
                    else:
                        self.axes[fig_name] = fig.add_axes([0.15, 0.15, 0.65, 0.75], projection='3d')

                    self.plotComponent(fig_name, MOG_Component, comp_to_plot)
            
            # product balance plot
            else:
                self.axes[fig_name] = fig.add_axes([0.15, 0.15, 0.65, 0.75])
                    
                if hasattr(self.optimizer, 'instance'):
                    # get product to plot
                    product_to_plot = self.stringVars['product'].get()
                    self.plotProduct(fig_name, product_to_plot)
        
    def plotComponent(self, fig_name: str, src: dict,
                      comp_to_plot: str, plot_point: tuple = None, plot_ops: bool = True):
        
        comp = src.components[comp_to_plot]
        comp_states = list(comp.operating_points.keys())
        comp_data = {state: comp.operating_points[state] 
                     for state in comp_states}

        if fig_name == 'Model Generation - Components':
            if self.intVars['set manually'].get() == 1:
                sect = {state: self.scales['Number of sections (all)'].get()
                        for state in comp_states}
            else:
                sect = {state: self.mogger.sections[comp_to_plot][state]
                        for state in comp_states}
                            
            curve_data = self.mogger.curves   
        else:
            curve_data = src.curves
            sect = src.sections[comp_to_plot]
        
        # color idx
        color = 0
        
        # save axis limits for multistate plots of multivariate components
        limits = {'x': (None,None), 
                  'y': (None,None), 
                  'z': (None,None)}
     
        # plot all operating states of component
        for state in comp_states:
            # if data already fitted
            if sect[state] in curve_data[comp_to_plot][state]:
                mode = 'fitted'
                
                # plot 2D
                if len(comp.operating_points[state].columns) == 2:
                    # extract fitted model for plot
                    curve_comp = curve_data[comp_to_plot][state][sect[state]]
                    breakpoints = curve_comp['v']
                    x = list(breakpoints.keys())
                    y = list(breakpoints.values())
                
                    # plot data if desired
                    if plot_ops: 
                        self.axes[fig_name].plot(comp_data[state][comp.outputs[0]],
                                                 comp_data[state][comp.inputs[0]],
                                                 'o',
                                                 color = self.utilities['colormap'][color],
                                                 alpha = 0.5, label='state'+str(state))
                        self.axes[fig_name].legend(bbox_to_anchor=(1.05, 1), loc='upper left')
                    
                    # plot fit
                    self.axes[fig_name].plot(x, y, '-',
                                             color = self.utilities['colormap'][6])

                # plot 3D
                else:
                    curve_comp = copy.deepcopy(curve_data[comp_to_plot][state][sect[state]])
                    x_plot = comp.operating_points[state][comp.outputs[0]].values.reshape(-1, 1)
                    y_plot = comp.operating_points[state][comp.inputs[0]].values.reshape(-1, 1)
                    
                    for i in range(1, len(comp.inputs)):
                        x_plot = np.column_stack((x_plot, 
                                                  comp.operating_points[state][comp.inputs[i]].values.reshape(-1, 1)))
                        
                    limits = hh.plot_model(self.axes[fig_name], 
                                           curve_comp['model'], 
                                           len(comp.operating_points[state].columns)-1, 
                                           x_plot, y_plot, self.utilities['colormap'][color], 
                                           'state'+str(state), plot_ops,
                                           limits=limits)
                    
                    if fig_name == 'Model Generation - Components':
                        self.axes[fig_name].legend(bbox_to_anchor=(1.05, 1), loc='upper left')
            # if data not fitted yet
            else:
                mode = 'unfitted'
                
                # plot 2D
                if len(comp.inputs) == 1:
                    # plot all operating states in different colors
                    x = comp_data[state][comp.outputs[0]]
                    y = comp_data[state][comp.inputs[0]]
                    self.axes[fig_name].scatter(x.values,y.values, 
                                                color = self.utilities['colormap'][color],
                                                alpha = 0.5, label='state'+str(state)) 
                    self.axes[fig_name].legend(bbox_to_anchor=(1.05, 1), loc='upper left')
                    
                    
                # plot 3D
                else:
                    x = comp_data[state][comp.outputs[0]]
                    z = comp_data[state][comp.inputs[0]]  # dependent variable in the first column of the inputs
                    y = comp_data[state][comp.inputs[1]]  # TODO: decision which dim to plot
                    self.axes[fig_name].scatter(x.values, y.values, z.values,
                                                color=self.utilities['colormap'][color],
                                                alpha=0.5, label='state'+str(state))
                    self.axes[fig_name].legend(bbox_to_anchor=(1.05, 1), loc='upper left')
                    
            # plot one specific point
            if plot_point != None and len(comp.inputs) == 1:
                        x,y = plot_point
                        self.axes[fig_name].plot([x],[y],'x', 
                                                 color = self.utilities['colormap'][1],
                                                 alpha = 1)
                        
            elif plot_point != None and len(comp.inputs) > 1:
                        x,y,z = plot_point
                        self.axes[fig_name].plot([x],[y],[z],'x',
                                                 color = self.utilities['colormap'][1],
                                                 alpha = 1)
                        
            
            color = (color + 3) % len(self.utilities['colormap'])        
            
        # title     
        title_string = comp_to_plot
        self.title_string = r'Component: '
        
        # handle subscript in title
        if len(title_string.split('_')) == 2:
            parts = title_string.split('_')
            self.title_string += r'$%s_{%s}$' % (parts[0], parts[1])
        elif len(title_string.split('_')) == 1:
            self.title_string += r'$%s$' % (title_string)
        else:
            self.title_string += title_string 
            
        if fig_name != 'Operational Optimization - Components':
            fontsize={'title': 18, 'label': 15}
        else:
            fontsize={'title': 12, 'label': 10}
            
        self.axes[fig_name].set_title(self.title_string, fontweight="bold", fontsize = fontsize['title'])
                
        # adjust axes
        
        # 2D
        if len(comp.inputs) == 1:
            try:
                units = {'in':  comp.units['inputs'][comp.inputs[0]],
                         'out': comp.units['outputs'][comp.outputs[0]]}
            except:
                units = {'in':  '[-]',
                         'out': '[-]'}

            self.axes[fig_name].set_xlabel(comp.outputs[0] + ' / ' + units['out'], 
                                           fontweight="bold", fontsize = fontsize['label'])
            self.axes[fig_name].set_ylabel(comp.inputs[0] + ' / ' + units['in'],
                                           fontweight="bold", fontsize = fontsize['label'])
            self.axes[fig_name].set_xlim(left=0)
            self.axes[fig_name].set_ylim(bottom=0)

            # annotations
            if mode == 'fitted':
                props = dict(boxstyle='round', facecolor='#e8f1fa', alpha=1)
                xmin, xmax = self.axes[fig_name].get_xlim()
                ymin, ymax = self.axes[fig_name].get_ylim()
                x_pos = xmin + 0.05 * (xmax - xmin)
                y_pos = ymin + 0.93 * (ymax - ymin)
                self.axes[fig_name].text(x_pos, y_pos,
                                         "Nominal Data \n-------------------------- \nOutput: {} kW \nInput: {} kW \nEfficiency: {} %".format(
                                             round(curve_comp['o_n'], 0),
                                             round(curve_comp['o_n'] /
                                                   curve_comp['e_n'], 0),
                                             round(curve_comp['e_n'] * 100, 2)),
                                         bbox=props, ha='left', va='top', fontsize=9, fontstyle='italic')

        # 3D
        else:
            units = {'in_0':  comp.units['inputs'][comp.inputs[0]],
                     'in_1': comp.units['inputs'][comp.inputs[1]],
                     'out': comp.units['outputs'][comp.outputs[0]]}

            self.axes[fig_name].set_xlabel(comp.outputs[0] + ' / ' + units['out'], fontweight="bold", fontsize=fontsize['label'])
            self.axes[fig_name].set_ylabel(comp.inputs[1] + ' / ' + units['in_1'], fontweight="bold", fontsize=fontsize['label'])
            self.axes[fig_name].set_zlabel(comp.inputs[0] + ' / ' + units['in_0'], fontweight="bold", fontsize=fontsize['label'])

        self.axes[fig_name].grid(True, which='major',  linestyle="dashed", alpha=0.3)
        self.axes[fig_name].grid(True, which='minor',  linestyle="dashed", alpha=0.3)

        # draw
        self.plot_canvas[fig_name].draw()
    
    def plotStorage(self, fig_name: str, storage: str):
        max_filllevel = self.optimizer.model.storages[storage]['max_vol']
        min_filllevel = self.optimizer.model.storages[storage]['min_vol']
        storage_product = self.optimizer.model.storages[storage]['storage_product']
        
        if hasattr(self.optimizer, 'results'):
            t = self.scales['Time Step'].get()
            next_ = {v:k 
                     for k,v in self.optimizer.results.all_['previous_timestep'].items()}[t]
            
            filllevel_0 = self.optimizer.results.all_['FILLLEVEL'][storage,t]
            filllevel_1 = self.optimizer.results.all_['FILLLEVEL'][storage,next_]
            
        else:
            filllevel_0 = min_filllevel
            filllevel_1 = max_filllevel
            
        loading   = (filllevel_1 > filllevel_0)
        unloading = (filllevel_1 < filllevel_0)
        
        def data_for_cylinder_along_z(center_x,center_y,radius,height_z_lower,height_z_upper):
            z = np.linspace(height_z_lower, height_z_upper, 50)
            theta = np.linspace(0, 2*np.pi, 50)
            theta_grid, z_grid=np.meshgrid(theta, z)
            x_grid = radius*np.cos(theta_grid) + center_x
            y_grid = radius*np.sin(theta_grid) + center_y
            return x_grid,y_grid,z_grid
        
        Xc,Yc,Z_full  = data_for_cylinder_along_z(0,0,max_filllevel/4,0,max_filllevel)
        _,_,Z0 = data_for_cylinder_along_z(0,0,max_filllevel/4,0,filllevel_0)
        _,_,Z1 = data_for_cylinder_along_z(0,0,max_filllevel/4,filllevel_0,filllevel_1)
        
        if loading:
            c,alpha = 'green', 1 
        elif unloading:
            c,alpha = 'red',1
        else:
            c,alpha = 'white',0
        
        self.axes[fig_name].plot_surface(Xc, Yc, Z_full, alpha=0.2, 
                                         color='white', zorder=1)
        self.axes[fig_name].plot_surface(Xc, Yc, Z0, alpha=0.7, 
                                         color='blue', zorder=2)
        self.axes[fig_name].plot_surface(Xc, Yc, Z1, alpha=alpha, 
                                         color=c, zorder=3)
        
        
        # ceiling = Circle((0,0), max_filllevel/4, color=c)
        # self.axes[fig_name].add_patch(ceiling)
        # art3d.pathpatch_2d_to_3d(ceiling, z=Z1, zdir="z")
        
        self.axes[fig_name].set_xticks([])
        self.axes[fig_name].set_yticks([])
        self.axes[fig_name].set_zlabel('Filllevel / [kWh]', fontweight="bold", fontsize=10)
        self.axes[fig_name].set_title(f'Storage: {storage} ({storage_product})', fontweight="bold", fontsize=12)
        
        # draw
        self.plot_canvas[fig_name].draw()
        
    
    def plotProduct(self, fig_name: str, product_to_plot: str):
        # create second y-axis with same x-axis
        self.axes[fig_name + '_2'] = self.axes[fig_name].twinx()

        # if optimization results are available
        if hasattr(self.optimizer, 'results'):
            lines_to_plot = self.optimizer.results.lines[product_to_plot]

            if not 'component' in lines_to_plot:
                # check if storage exsits for product
                unloading = lines_to_plot['UNLOADING']
                loading   = lines_to_plot['LOADING']
                
                if len(unloading.index) > 0:
                    unloading = unloading.unstack().rename(index={idx: idx + ' unloading' 
                                                                  for idx in lines_to_plot['LOADING'].unstack().index})
                    loading   = loading.unstack().rename(index={idx: idx + ' loading' 
                                                                for idx in lines_to_plot['LOADING'].unstack().index})
                    positive = [unloading]
                    negative = [-loading]
                else:
                    positive = []
                    negative = []
                
                # concatenate results
                positive.extend([lines_to_plot['OUTPUT'].droplevel(1).unstack(),
                                 lines_to_plot['USED_SOURCE'].droplevel(0).to_frame(name='USED_SOURCE').transpose(),
                                 lines_to_plot['NON_SERVED_DEMAND'].droplevel(0).to_frame(name='NON_SERVED_DEMAND').transpose()])
                                                        
                negative.extend([-lines_to_plot['INPUT'].droplevel(1).unstack(),
                                 -lines_to_plot['USED_SINK'].droplevel(0).to_frame(name='USED_SINK').transpose()])
                
                lines_to_plot['component'] = pd.concat([*positive, *negative], 
                                                        axis=0).fillna(0)
                    
                # change small values to zero
                lines_to_plot['component'][abs(lines_to_plot['component']) < 1e-3] = 0
                
                # filter rows with all zero values
                lines_to_plot['component'] = lines_to_plot['component'].loc[(lines_to_plot['component'] != 0).any(axis=1),:].transpose()
                
            if not lines_to_plot['component'].empty:
                self.axes[fig_name].clear()
                if self.intVars['cumulated'].get():
                    frame_plot = lines_to_plot['component'].cumsum()
                else:
                    frame_plot = lines_to_plot['component']
                frame_plot.plot.area(alpha=0.4, ax=self.axes[fig_name], 
                                     color=get_rwth_colors()[-frame_plot.shape[1]:],
                                     zorder=1)
    
            # for all lines to plot
            lines_iter = list(lines_to_plot.keys())
            for idx, line in enumerate(lines_iter):
                # demand, price, compensation
                if line in ['demand', 'price', 'compensation']:
                    # if checkbox marked
                    if self.intVars[line].get():
                        # determine axis and line color
                        if line == 'demand':
                            ax = fig_name
                            c = 'k'
                        elif line in ['price', 'compensation']:
                            ax = fig_name + '_2'
                            c = get_rwth_colors()[idx]
                        
                        # TODO: dont use try-except here
                        # first plot
                        try:
                            # if non-zero values exist
                            if any(x != 0 for x in lines_to_plot[line].values):
                                
                                # drop non-numeric index levels
                                todrop=[]
                                for idx_level in range(lines_to_plot[line].index.nlevels):
                                    if lines_to_plot[line].index.get_level_values(idx_level).dtype != 'int64':
                                        todrop.append(idx_level)
                                lines_to_plot[line] = lines_to_plot[line].droplevel(todrop)
                                
                                if self.intVars['cumulated'].get() and line == 'demand':
                                    frame_plot = lines_to_plot[line].cumsum()
                                else:
                                    frame_plot = lines_to_plot[line]
                                # line plot
                                frame_plot.plot(ax=self.axes[ax], label=line, alpha=1, c=c)
                                
                        # later plots
                        except: 
                            if self.intVars['cumulated'].get() and line == 'demand':
                                    frame_plot = lines_to_plot[line].cumsum()
                            else:
                                frame_plot = lines_to_plot[line]
                            # line plot
                            frame_plot.plot(ax=self.axes[ax], label=line, alpha=1, c=c)
                        
                        
            # for both y-axis
            for axes in [fig_name, fig_name + '_2']:
                
                # set x-lim
                self.axes[axes].set_xlim(left=0, right=self.optimizer.instance.ntimesteps-1)
                
                # if no lines plotted in axis, remove ticks
                if len(self.axes[axes].get_lines()) == 0:
                    self.axes[axes].get_yaxis().set_ticks([])
                
                # if lines plotted in axis
                else:
                    # adjust limits
                    if self.axes[axes].get_ylim()[0] > 0:
                        self.axes[axes].set_ylim(bottom=0)
                    
                    # adjust labels
                    # TODO: generic unit handling for optimization results
                    if axes == fig_name:
                        unit = '[kW]'
                    else:
                         unit = '[€]'
                    
                    self.axes[axes].set_ylabel(unit,  fontweight="bold", fontsize = 15)
                    
                    # plot vertical line for current timestep
                    if axes==fig_name:
                        x = 2*[self.scales['Time Step'].get()]
                        y = list(self.axes[axes].get_ylim())
                        self.axes[axes].plot(x,y, c = 'k', alpha=1,
                                             linestyle='dotted', linewidth=1)
                        self.axes[axes].set_ylim(tuple(y))
                        
                        self.axes[fig_name].legend(bbox_to_anchor=(1.05, 1), loc='upper left')
                    
                    elif axes==fig_name + '_2':
                        # legend
                        lines_1, labels_1 = self.axes[fig_name].get_legend_handles_labels()
                        lines_2, labels_2 = self.axes[fig_name + '_2'].get_legend_handles_labels()
                        self.axes[fig_name].legend(lines_1+lines_2, labels_1+labels_2, 
                                                   bbox_to_anchor=(1.1, 1), loc='upper left')
            
            # plot title
            title_string = 'Product: ' +  product_to_plot
            self.axes[fig_name].set_title(title_string, fontweight="bold", fontsize = 18)
            
            # xlabel 
            self.axes[fig_name].set_xlabel('time / [h]', fontweight="bold", fontsize = 15)
            
            # plot grid
            self.axes[fig_name].grid(True, which='major',  linestyle="dashed", alpha=0.3, axis='both')
        
        # if no optimization results available
        else:
            # TODO: create some plots already if only instance is available
            pass
        
        # draw plot
        self.plot_canvas[fig_name].draw()
    
    def build_operating_points_column_names(self, operating_points: dict):
        """ put unit and (in)/(out) identifier back to column name of operating_points 
            Needed before: exports, reinitialization of AutoMoG object. """
        for comp in operating_points:
            for state in operating_points[comp]:
                rename = {}
                for col in operating_points[comp][state].columns:
                    if col in self.mogger.components[comp].inputs:
                        if col in self.mogger.components[comp].params:
                            rename[col] = col + ' (param) ' + self.mogger.components[comp].units['params'][col]
                        else:
                            rename[col] = col + ' (in) ' + self.mogger.components[comp].units['inputs'][col]
                    elif col in self.mogger.components[comp].units['outputs']:
                        rename[col] = col + ' (out) ' + self.mogger.components[comp].units['outputs'][col]
            
                operating_points[comp][state].rename(rename, axis=1, inplace=True)
        return operating_points
    
    def save_operating_points(self):
        """saves operating points into new .xlsx file with thrid column containing the operating states"""
        save_dir = self.working_directory 
        save_path = asksaveasfilename(parent = self.parent,
                                      title = 'Save Operating Points',
                                      initialdir = save_dir,
                                      initialfile = 'operating_points.xlsx',
                                      filetypes = [('xlsx-files (*.xlsx)','*.xlsx')],
                                      defaultextension='.xlsx')
            
        if save_path != '':
            with pd.ExcelWriter(save_path) as writer:
                operating_points_export = self.build_operating_points_column_names(self.operating_points)
                for comp in operating_points_export.keys():
                    for state in operating_points_export[comp].keys():
                        operating_points_export[comp][state]['Operating State']=state
                    excel_frame = pd.concat(operating_points_export[comp])
                    excel_frame.to_excel(writer, sheet_name=comp, index=False)
            showinfo('Operating Points saved.', 'Operating Points have been saved to {}.'.format(save_dir))
    
    def save_plots(self):
        """saves plots to .pdf and .svg file"""
        save_dir = {'..': self.output_dir + '/plots'}
        file_formats = ['.pdf', '.svg']
        
        for f in file_formats:
            save_dir[f] = save_dir['..'] + '/' +  f.split('.')[1]
            if not os.path.exists(save_dir[f]):
                os.makedirs(save_dir[f])
            
        for comp_name in MOG_Component.components.keys():
            self.fig['Model Generation - Components']['fig'].clf()
            self.axes['Model Generation - Components'] = self.fig['Model Generation - Components']['fig'].add_axes([0.1,0.1,0.8,0.8])
            self.plotComponent('Model Generation - Components', MOG_Component, comp_name)
            for f in file_formats:
                self.fig['Model Generation - Components']['fig'].savefig(save_dir[f] + '/{}{}'.format(comp_name,f))
        
        self.plotFigures()
        
        showinfo('Plots saved.', 'Plots have been saved to {}.'.format(save_dir['..']))
           
    def export_model(self, save_path: str = ''):
        dialog = (save_path == '')
        
        try:
            m = model(self.mogger.components, self.mogger.curves, self.mogger.sections, storages = self.storages, 
                      name = os.path.basename(self.working_directory))
        except KeyError:
            showerror('Model couldn\'t be saved.', 'For the given number of linear sections no model has been created yet.'\
                        +' Create a model first.')
            return
        
        if dialog:
            save_dir = self.working_directory + '/optimization/models'
            if not os.path.exists(save_dir):
                os.makedirs(save_dir)
                
            save_path = asksaveasfilename(parent = self.parent,
                                          title = 'Export model to pkl',
                                          initialdir = save_dir,
                                          initialfile = 'energy_system_model.pkl',
                                          filetypes = [('pickle files (*.pkl)','*.pkl')],
                                          defaultextension='.pkl')
        
        if save_path != '':
            m.to_file(save_path)
            if dialog:
                showinfo('Model saved.', 'Model has been saved to {}.'.format(save_path))
        else:
            showwarning('Saving interrupted.', 'Saving process has been manually interrupted.')

    def export_model_to_xlsx(self, save_path: str = ''):
        dialog = (save_path == '')

        try:
            self.mogger.curves['legend'] = {'legend': {'o_n': 'nomial output', 'e_n': 'nominal efficiency',
                         'v': 'vertices (breakpoints of piecewise linear model)', 'ic': 'value of used information criterion', 'model_error': 'sum of squared residuals (python-pwlf) or sum of distances (REBENNACK/KRASKO, KONG/MARAVELIAS)', 'mse':'mean squared error', 'sol_time':'used time to fit the model'}}
            m = pd.DataFrame(data=self.mogger.curves)
            m = (m.T)
        except KeyError:
            showerror('Model couldn\'t be saved.',
                      'For the given number of linear sections no model has been created yet.' \
                      + ' Create a model first.')
            return

        if dialog:
            save_dir = self.working_directory + '/optimization/models'
            if not os.path.exists(save_dir):
                os.makedirs(save_dir)

            save_path = asksaveasfilename(parent=self.parent,
                                          title='Export model to xlsx',
                                          initialdir=save_dir,
                                          initialfile='energy_system_model.xlsx',
                                          filetypes=[('xlsx-files (*.xlsx)','*.xlsx')],
                                          defaultextension='.xlsx')

        if save_path != '':
            m.to_excel(save_path)
            if dialog:
                showinfo('Model saved.', 'Model has been saved to {}.'.format(save_path))
        else:
            showwarning('Saving interrupted.', 'Saving process has been manually interrupted.')

    def import_model(self, to_opt: bool = False):
        import_dir = self.working_directory + '/optimization/models'
            
        import_path = askopenfilename(parent = self.parent,
                                      title = 'Import model',
                                      initialdir = import_dir,
                                      filetypes = [('pickle files (*.pkl)','*.pkl')],
                                      defaultextension='.pkl')
        
        if import_path != '':
            imported_model = model.from_file(import_path)

            if to_opt:
                self.optimizer.model = imported_model
                
                
            for attr in ['components','curves','sections']:
                setattr(self.mogger, attr, getattr(imported_model, attr))     
            self.updateWidgets()
            showinfo('Model imported.', 'Model has been successfully imported.')
    
    def load_results(self):
        import_dir = self.working_directory + '/optimization/results'
        if not os.path.isdir(import_dir):
            os.makedirs(import_dir)
            
        import_path = askopenfilename(parent = self.parent,
                                      title = 'Import results',
                                      initialdir = import_dir,
                                      filetypes = [('pickle files (*.pkl)','*.pkl')],
                                      defaultextension='.pkl')
        
        if import_path != '':
            res,mod,inst = results.from_file(import_path)
            
            self.optimizer.model = mod
            self.optimizer.instance = inst
            self.optimizer.results = res
            
            log_path = os.path.dirname(import_path) + '/solver_output.log'
            
            self.read_log(log_path)
            
            self.updateWidgets()
            showinfo('Results imported.', 'Results have been successfully imported.')        
    
    def read_log(self, path:str):
        with open (path, "r") as f:
                log=f.read()

        self.scrolled_text['OPT_LOG'].delete(1.0,'end')
        self.scrolled_text['OPT_LOG'].insert('end', log)
    
    def load_model(self):
        self.optimizer.reset()
        self.import_model(to_opt=True)
    
    def load_instance(self):
        self.optimizer.reset()
        if hasattr(self.optimizer, 'model'):
            import_dir = self.working_directory + '/optimization/instances'
            if not os.path.isdir(import_dir):
                os.makedirs(import_dir)
                
            import_path = askopenfilename(parent = self.parent,
                                          title = 'Import instance',
                                          initialdir = import_dir,
                                          filetypes = [('Excel-files (*.xlsx)','*.xlsx'),
                                                       ('CSV-files (*.csv)','*.csv')],
                                          defaultextension='.xlsx')
            
            if import_path != '':
                if import_path.endswith('.csv'):
                    imported_instance = instance.from_csv(import_path)
                elif import_path.endswith('.xlsx'):
                    imported_instance = instance.from_excel(import_path)
                    
                self.optimizer.instance = imported_instance
                self.updateWidgets()
                showinfo('Instance imported.', 'Instance has been successfully imported.')
        else:
            showerror('Instance import failed', 'Load a model first.')
            
    def transfer_to_optimization(self):
        # TODO: error handling
        # reset optimizer
        self.optimizer.reset()
        # read supplementary input if available
        self.read_supplementary_input()
                
        # TODO: save instance to file in optimization_input as well
        export = self.working_directory + '/optimization/models'
        if not os.path.isdir(export):
            os.makedirs(export)
        export += '/transfered_model-{}.pkl'.format(datetime.now().strftime("%Y_%m_%d_%H-%M-%S"))
        self.export_model(export) 
        self.scrolled_text['OPT_LOG'].delete(1.0,'end')
        self.tab_parent.select(self.tabs['Operational Optimization']['tab'])
        self.init_optimizer()
        
    def start_optimization(self):
        if not hasattr(self.optimizer, 'model'): 
            showerror('Start optimization failed','Load a model first.')
        elif not hasattr(self.optimizer, 'instance'): 
            showerror('Start optimization failed','Load an instance first.')
        else:  
            #sys.stdout = self.text_handler['OPT_LOG']
            self.optimizer.solver = self.stringVars['solver'].get().lower()
            self.optimizer.optimize(save=True, output_dir = self.working_directory + '/optimization/results')
            
            self.read_log(self.optimizer.output_dir + '/solver_output.log')
            #sys.stdout = sys.__stdout__
            self.updateWidgets()
        
            
    def start_clustering(self):
        component = self.stringVars['component_Model Generation'].get()
        state     = int(self.stringVars['operating_state'].get())
        method    = self.stringVars['clustering'].get()
        number_of_clusters = int(self.scales['Number of clusters'].get())
        max_runtime=int(self.entries['Maximum Runtime'].get())
        
        operating_points = self.build_operating_points_column_names(self.mogger.operating_points)
        StepBack=copy.deepcopy(operating_points)
        StepBack_operating_points.append(StepBack)
        old_states.append(list(StepBack[component].keys()))
        Clusterer=self.Clusterer
        self.operating_points = Clusterer.CombiClustering(operating_points, component, method, number_of_clusters,
                                                state,max_runtime, i=10, patience=10)
        
        self.mogger = AutoMoG(self, 
                              operating_points=self.operating_points,
                              wdir=self.working_directory)
        
        self.updateWidgets()

    
    def reset_clustering(self):   #TODO: Clean Up Method, only updateWidgets should do
        component = self.stringVars['component_Model Generation'].get()
        initial_states=list(initial_points[component].keys())
        if StepBack_operating_points:
            if self.intVars['full reset'].get() == 0:
                self.operating_points=copy.deepcopy(StepBack_operating_points[-1])
                del StepBack_operating_points[-1]

                self.mogger = AutoMoG(self, 
                                  operating_points=self.operating_points,
                                  wdir=self.working_directory)
                
                self.updateOptionMenu('operating_state', old_states[-1], self.stringVars['operating_state'].get(), None)
                del old_states[-1]
            
            elif self.intVars['full reset'].get() == 1:
                self.operating_points=copy.deepcopy(initial_points)
                self.mogger = AutoMoG(self, 
                                  operating_points=self.operating_points,
                                  wdir=self.working_directory)  
                self.updateOptionMenu('operating_state', initial_states, self.stringVars['operating_state'].get(), None)
                
            self.updateWidgets()

    def updateTimeStep(self, delta: int):
        try:
            current = self.scales['Time Step'].get()
            self.scales['Time Step'].set(current+delta)
        except:
            pass
        
    def synch_solver(self, *args):
        self.stringVars['solver'].set(self.stringVars['solver_modeling'].get())
    def synch_solver_modeling(self, *args):
        self.stringVars['solver_modeling'].set(self.stringVars['solver'].get())
    
    def manual_linearization(self):
        self.mogger.hull_acc = 0.01 * self.get_entry_content('Allowed input space reduction / [%]', target_type=float)
        # if set manually, choose number of sections according to GUI selection
        if self.intVars['set manually'].get()==1:
            for comp in self.mogger.sections:
                for state in self.mogger.sections[comp]:
                    self.mogger.sections[comp][state] = self.scales['Number of sections (all)'].get()
        
        # check for data which will be overwritten
        overwrite=[]
        overwrite_str='\n'
        for comp in self.mogger.components:
            for state in self.mogger.components[comp].operating_points:
                if self.mogger.sections[comp][state] in self.mogger.curves[comp][state]:
                    overwrite.append(comp + '_' + str(state))
                    overwrite_str+='\n{}'.format(comp + '_' + str(state))
        
        if len(overwrite)>0:
            write_permit = askokcancel('Overwrite',
                                       'For the following components linearized models are already available with the given Number of sections (all):'+
                                        overwrite_str +
                                       '\n Do you want to overwrite them?')
        else:
            write_permit = True
        
        # start linearization if permitted and replot
        if write_permit:
            self.mogger.reset(current_section=True)
            self.mogger.update(icrit=self.stringVars['information criterion'].get(),
                               lin_method=self.stringVars['linearization manual'].get(),
                               mode=self.stringVars['automog mode'].get())
            self.mogger.linearizer.linearize_all()
            self.startPlots()       
        
    def select_components(self):
        self.newWindow = tk.Toplevel(self.parent)
        ComponentSelecter(self.newWindow, self)
        
    def select_sections(self):
        self.newWindow = tk.Toplevel(self.parent)
        SectionSelecter(self.newWindow, self)
        
    def show_iteration_frame(self):
        self.newWindow = tk.Toplevel(self.parent)
        self.iteration_frame = Iteration_Frame(self.newWindow, self)
        
    def set_SolverSettings(self):
        self.newWindow = tk.Toplevel(self.parent)
        SolverSettings(self.newWindow, self)
        
    def set_ModelSettings(self):
        if hasattr(self.optimizer, '_model'):
            self.newWindow = tk.Toplevel(self.parent)
            ModelSettings(self.newWindow, self)
        else:
            showerror('No model available.', 'Cannot show model settings. You need to load a model first.')
    
    def show_FlowChart(self):
        
        if hasattr(self.optimizer, 'model') and hasattr(self.optimizer, 'instance'):
            
            save_dir = self.working_directory 
            
            self.newWindow = tk.Toplevel(self.parent)
            
            toplevel = Toplevel(self.newWindow, self)
            
            flow_chart_creator=FlowChart(self.optimizer.model,self.working_directory)
            flow_chart_creator.create_FlowChart(include_legend=False)
            chart_img = ImageTk.PhotoImage(Image.open(self.working_directory+"/FlowChart.png"))
            
            toplevel.parent.title('Flow Chart')
            
            panel=tk.Label(toplevel.parent,image=chart_img)
            panel.pack(side = "bottom", fill = "both", expand = "yes")
            toplevel.parent.mainloop()
            os.remove(self.working_directory+"/FlowChart.png")
            os.remove(self.working_directory+"/FlowChart")
        elif hasattr(self.optimizer, 'instance'):
            showerror('No model available.', 'Cannot show flow chart. You need to load a model first.')
        elif hasattr(self.optimizer, 'model'):
            showerror('No instance available.','Cannot show flow chart. You need to load an instance first.')
        else:
            showerror('Neither a model nor an instance available.','You need to load a model and an instance first in order to show the flow chart.')
    def set_InstanceSettings(self):
        if hasattr(self.optimizer, '_instance'):
            self.newWindow = tk.Toplevel(self.parent)
            InstanceSettings(self.newWindow, self)
        else:
            showerror('No instance available.', 'Cannot show instance settings. You need to load an instance first.')
   
# =============================================================================
#     def export_PyomoModel(self):
#                 
#         if hasattr(self.optimizer, 'model') and hasattr(self.optimizer, 'instance'):
# 
#             output_dir = self.working_directory + '/Output_pyomo_model/' + self.optimizer.model.name
#             right_length_dir=output_dir + '/longer'
#             if not os.path.isdir(output_dir):
#                 os.makedirs(output_dir)
#             
#             model_recreator=ModelRecreation(self.optimizer.model,self.optimizer.instance,config=self.optimizer.config)
#             model_recreator.create_model(right_length_dir, abstract_model=True)
#             pyomo_model=model_recreator.pyomo_model
#             input_dict=model_recreator.input_dictionary
# 
#             with open(output_dir+'/model.pkl', 'wb') as f:
#                 dill.dump(pyomo_model, f)
#             with open(output_dir+'/input_dictionary.pkl', 'wb') as f:
#                 dill.dump(input_dict, f)
# 
#         elif hasattr(self.optimizer, 'instance'):
#             showerror('No model available.', 'Cannot export Pyomo Model. You need to load a model first.')
#         elif hasattr(self.optimizer, 'model'):
#             showerror('No instance available.','Cannot export Pyomo Model. You need to load an instance first.')
#         else:
#             showerror('Neither a model nor an instance available.','You need to load a model and an instance first in order to export the Pyomo Model.')
# =============================================================================
        
class ComponentSelecter(Toplevel):
    #TODO: use GUI Toolbox functions
    def __init__(self, parent, mainframe):
        super().__init__(parent, mainframe)
        
        self.parent.title('Select Components')
        self.setup_frames({'N': {'row': 0, 'column': 0, 'columnspan': 1, 'rowspan': 1, 'relief': 'flat'},
                           'S': {'row': 1, 'column': 0, 'columnspan': 1, 'rowspan': 1, 'relief': 'flat'}})
    
        self.outputs = {}
        for comp_name in MOG_Component.__all__:
            comp = MOG_Component.__all__[comp_name]
            if comp.outputs[0] not in self.outputs:
                self.outputs[comp.outputs[0]] = [comp.name]
            else:
                self.outputs[comp.outputs[0]].append(comp.name)
        self.outputs = {k.split(' ')[0]: v for k,v in self.outputs.items()}
        
        
        for col in range(len(self.outputs)):
            self.frames['N'].grid_columnconfigure(col, weight=1, uniform="x")
            
        col_type_matcher = dict(zip(self.outputs, 
                                    list(range(len(self.outputs)))))
                                                       
        row_counter = dict(zip(self.outputs.keys(), 
                               len(self.outputs)*[1]))  
                                            
        for k, output in enumerate(self.outputs.keys()):
            tk.Label(self.frames['N'], 
                     text = output,bg = 'grey', 
                     font=self.utilities['standard_font'] + 
                     self.utilities['standard_font_size'] + ' italic').grid(row = 0,column=k,sticky='ew')        
        self.checkbuttons = {}
        self.checkbuttons_var = {}
       
        for comp in MOG_Component.__names__:
            if 'CHP' in comp and '_el' in comp:
                continue
            else:
                output = MOG_Component.__all__[comp].outputs[0].split(' ')[0]
                self.checkbuttons_var[comp] = tk.IntVar()
                self.checkbuttons_var[comp].set(MOG_Component.__all__[comp].select)
                self.checkbuttons[comp] = tk.Checkbutton(self.frames['N'], text = comp,
                                                         variable = self.checkbuttons_var[comp])
                self.checkbuttons[comp].grid(row = row_counter[output],
                                             column= col_type_matcher[output], 
                                             sticky='ew')
                row_counter[output]+=1
              
        self.button_select = tk.Button(self.frames['S'],text = 'Select',
                                       font=self.utilities['standard_font'] + self.utilities['standard_font_size'] + ' bold',
                                       command = self.select)
        self.button_select.pack()
        
    def select(self):
        selected_components = dict()
        for comp in MOG_Component.__names__:
            if 'CHP' in comp and '_el' in comp:
                continue
            else:
                selected_components[comp] = self.checkbuttons_var[comp].get()
                
        for comp in MOG_Component.__names__:
            if 'CHP' in comp and '_el' in comp:
                selected_components[comp] = self.checkbuttons_var[comp.replace('_el','')].get()
                
        self.mainframe.read_selected_components(selected_components)
        self.parent.destroy()
   
class SectionSelecter(Toplevel):
    def __init__(self, parent, mainframe):
        super().__init__(parent, mainframe)
        
        self.parent.title('Select Sections')
        self.setup_frames({'N': {'row': 0, 'column': 0, 'columnspan': 1, 'rowspan': 1, 'relief': 'flat'},
                           'S': {'row': 1, 'column': 0, 'columnspan': 1, 'rowspan': 1, 'relief': 'flat'}})
        
        widgets = {}
        # define scales 
        widgets['scales'] = {}
        MAX_COLUMN = 5
        
        counter = 0
        for comp_name in MOG_Component.components:
            comp= MOG_Component.components[comp_name]
            
            for state in comp.operating_points:
                if hasattr(comp, 'sections'):
                    init = self.mainframe.mogger.sections[comp_name][state]
                else:
                    init = 1
                # define scales
                widgets['scales'][comp.name+'_' + str(state)] = {'init': (init,'normal','black'), 
                                                                 'ticks': (1,10,1,2),
                                                                 'command': None,
                                                                 'position': (self.frames['N'],2*(counter//MAX_COLUMN)+1,
                                                                              counter % MAX_COLUMN,'ew'),
                                                                 'label': (2*(counter//MAX_COLUMN),
                                                                           counter % MAX_COLUMN,1,'center','ew')}
                counter += 1
            
        # define buttons
        widgets['buttons'] = {'Accept Selection': # overwrite self.mainframe.mogger.sections
                              {'command' : self.accept_selection,
                               'position': (self.frames['S'],0,0,1,1,'ew')},
                              'Export Setting File': # exports a setting file
                              {'command' : self.export_settings,
                               'position': (self.frames['S'],0,1,1,1,'ew')}
                              }
        
        # create widgets
        for w in widgets:
            getattr(self, 'create_{}'.format(w))(widgets[w])
            
    def export_settings(self):
        comps = sorted(MOG_Component.components)
        if hasattr(self.mainframe, 'settings'):
            settings = self.mainframe.settings
        else:
            columns = ['sections']
            if hasattr(MOG_Component.components[comps[0]], 'select'):
                columns.append('select')
                
            settings = pd.DataFrame(index = comps, 
                                    columns = columns)
            settings.index.name = 'component'
    
        for comp in comps:
            sections = []
            for sc in self.scales:
                if comp == '_'.join(sc.split('_')[:-1]):
                    sections.append(self.scales[sc].get())
            # select minimum of sections in all operating states for export
            sect = min(sections)
            settings.loc[comp, 'sections'] = sect 
            if 'select' in settings.columns:
                settings.loc[comp, 'select'] = 1
            
        save_dir = self.mainframe.working_directory + '/settings'
        if not os.path.isdir(save_dir):
            os.makedirs(save_dir)
            
        save_path = asksaveasfilename(parent = self.parent,
                                      title = 'Export setting file',
                                      initialdir = save_dir,
                                      initialfile = 'settings.xlsx',
                                      filetypes = [('Excel-files (*.xlsx)','*.xlsx')],
                                      defaultextension='.xlsx')
            
        if save_path != '':
            settings.to_excel(save_path)
            showinfo('Settings saved.', 'Setting file has been saved to {}.'.format(save_path))
            
    def accept_selection(self):
        accept = askyesnocancel('Accept Selection',
                                'If you continue, so far generated curve data will not be exportable anymore. Do you want to continue without exporting?')
        if accept != None:
            if not accept:
                self.mainframe.export_curves()
            
            for sc in self.scales:
                comp  = '_'.join(sc.split('_')[:-1])
                state = int(sc.split('_')[-1])
                self.mainframe.mogger.sections[comp][state] = self.scales[sc].get() 
            self.parent.destroy()
    
class Iteration_Frame(Toplevel):
    def __init__(self, parent, mainframe):
        super().__init__(parent, mainframe)
        self.setup_frames({
            'N_title':   {'row': 0, 'column': 0, 'columnspan': 1, 'rowspan': 1, 'relief': 'flat'},
            'N':         {'row': 1, 'column': 0, 'columnspan': 1, 'rowspan': 1, 'relief': 'flat'},
            'S_title':   {'row': 2, 'column': 0, 'columnspan': 1, 'rowspan': 1, 'relief': 'flat'},
            'S':         {'row': 3, 'column': 0, 'columnspan': 1, 'rowspan': 1, 'relief': 'flat'},
            'console':   {'row': 4, 'column': 0, 'columnspan': 1, 'rowspan': 1, 'relief': 'flat'}
            })
        
        self.set_geometry()
        self.setup()
        self.utilities['rows']       = [1,2,1,5,7] # height fraction of frames, important: only integer values
        self.utilities['columns']    = [1] # width fraction of frames, important: only integer values
        self.set_frame_sizes()
        
    def setup(self):
        self.setup_title()
        
        self.tables = {}
        self.table_north = Table(self.frames['N'])
        self.table_south = Table(self.frames['S'])
        
        self.color_cells = {'sections': [],
                            'delta':    []
                            }
        
        self.update(first=1)
        self.fill_tables()
        
        for t in self.table_match:
            # table configuration
            pt_config.apply_options({'fontsize':8,
                                     'cellwidth':50
                                     },
                                    t)
            if 'standard_font' in self.utilities: # if standard_font manually set
                if self.utilities['standard_font'] in pt_util.getFonts(): # if possible to set standard_font in table
                    pt_config.apply_options({'font': self.utilities['standard_font']},t)       
                else:
                    logging.info('Unable to set Font in Table. Used default Font instead.')
            t.show()
            t.showIndex()
            tk.Label(self.table_match[t]['frame'],
                     text = self.table_match[t]['title'],
                     font=self.utilities['standard_font'] +' 18 bold').grid()
            t.grid(sticky='nsew')
        
        self.txt = tk.scrolledtext.ScrolledText(self.frames['console'], height=20)
        self.txt.grid(sticky='nsew')
        
        self.parent.update()
        
    def setup_title(self):
        self.parent.title('Proceed AutoMoG iteration... (iteration: {}, components finished: {}/{})'.format(self.mainframe.mogger.iter+1,
                                                                                                            sum(list(self.mainframe.mogger.component_finished.values())),
                                                                                                            len(self.mainframe.mogger.components)))
    def update(self, first=0, max_delta_component = None, iter_counter = None):
        # if max_delta_component != None:
        #     self.color_cells['sections'] = [max_delta_component]
        #     if iter_counter != None:
        #         self.color_cells['delta'].append((iter_counter, max_delta_component))
        self.setup_title() 
        # update section table
        sections = pd.DataFrame.from_dict(self.mainframe.mogger.sections).fillna(0).astype(int)
        sections.index.name     = 'state'
        self.tables['sections'] = sections
        # update delta table        
        self.tables['delta'] = self.mainframe.mogger.store['delta_target'].head(self.mainframe.mogger.iter+1)
        
        self.table_match = {self.table_north: {'table': self.tables['sections'],
                                               'title': 'NUMBER OF SECTIONS', 
                                               'frame': self.frames['N_title']},
                            self.table_south: {'table': self.tables['delta'], 
                                               'title': 'DELTA {}'.format(self.mainframe.mogger.options['mode']), 
                                               'frame': self.frames['S_title']}
                            }
        
        if not first:
            self.fill_tables()
            self.parent.update()
    
    def fill_tables(self):
        for t in self.table_match:
            t.updateModel(TableModel(copy.deepcopy(self.table_match[t]['table'])))
            # if len(self.color_cells['sections']) >0:
            #     for t in self.table_match:
            #         t.resetColors()
            #         t.columncolors[self.color_cells['sections'][0]] = '#00549f'
            t.rowindexheader = IndexHeader(t.parentframe, 
                                           t)
            t.rowindexheader.grid(row=0,column=0,rowspan=1,sticky='news')
            t.redraw()
    
    def update_txt(self, text):
        self.txt.insert('end', text)
        self.txt.insert('end', "\n")
        self.txt.yview(tk.END)
        self.parent.update()

class SolverSettings(Toplevel):
    def __init__(self, parent, mainframe):
        super().__init__(parent, mainframe)
        self.parent.title('Solver settings')
        self.setup_frames({'N': {'row': 0, 'column': 0, 'columnspan': 1, 'rowspan': 1, 'relief': 'flat'},
                           'S': {'row': 1, 'column': 0, 'columnspan': 1, 'rowspan': 1, 'relief': 'flat'}})
        
        self.solver = self.mainframe.stringVars['solver'].get()
        self.settings = self.mainframe.optimizer.solver_options[self.solver.lower()]
        
        widgets = {}
        widgets['buttons'] = {'Accept & Close': # overwrite self.mainframe.mogger.sections
                              {'command' : self.accept,
                               'position': (self.frames['S'],0,0,1,1,'ew')},
                              'Set as default': # exports a setting file
                              {'command' : self.export,
                               'position': (self.frames['S'],0,1,1,1,'ew')},
                              }
        
        widgets['entries'] = {}
        for idx,setting in enumerate(self.settings):
            widgets['entries'][setting] = {'init': self.settings[setting], 
                                           'position': (self.frames['N'],idx,1,'ew'),
                                           'label': (idx,0,1,'w','ew'),
                                           'label_bg': 'gray94'}
            
        for ent in widgets['entries']:
            row = widgets['entries'][ent]['position'][1]
            widgets['buttons']['rem_' + ent] = {'command' : lambda btn = 'rem_' + ent: self.remove_option(btn),
                                                'position': (self.frames['N'],row,2,1,1,'ew'),
                                                'label': u'\u2212'} 
        max_row = len(self.settings)
        widgets['buttons']['add'] = {'command' : self.add_option,
                                     'position': (self.frames['N'],max_row,2,1,1,'ew'),
                                     'label': u'\u002b'}  
            
       # create widgets
        for w in widgets:
            getattr(self, 'create_{}'.format(w))(widgets[w])
            
        self.custom_counter=0
    
    def add_option(self):
        row = self.buttons['add'].grid_info()['row']
        self.buttons['add'].grid_forget()
        self.buttons['add'].grid(row = row+1,column = 2, rowspan = 1, columnspan = 1, sticky='ew')
        
        self.create_entries({'name_custom_{}'.format(self.custom_counter): 
                                 {'init': '', 
                                  'position': (self.frames['N'],row,0,'ew')},
                            'custom_{}'.format(self.custom_counter):
                                {'init': '', 
                                  'position': (self.frames['N'],row,1,'ew')}
                                }
                             )

        self.create_buttons({'rem_' + 'custom_{}'.format(self.custom_counter):
                                 {'command' : lambda btn = 'rem_' + 'custom_{}'.format(self.custom_counter): self.remove_option(btn),
                                  'position': (self.frames['N'],row,2,1,1,'ew'),
                                  'label': u'\u2212'}})
        self.custom_counter+=1
        
    def remove_option(self, btn):
        ent = btn.split('rem_')[1]
        row_remove = self.buttons[btn].grid_info()['row']
        for widget in [self.buttons[btn], self.entries[ent], ]:
            widget.grid_forget()
        del self.buttons[btn]
        del self.entries[ent]
        if 'custom' in ent:
            self.entries['name_' + ent].grid_forget()
            del self.entries['name_' + ent]
        else:
            self.entries_labels[ent].grid_forget()
            del self.entries_labels[ent]
        
        for b in self.buttons:
            if b=='add' or 'rem_' in b:
                row = self.buttons[b].grid_info()['row']
                if row > row_remove:
                    self.buttons[b].grid_forget()
                    self.buttons[b].grid(row = row-1,column = 2, rowspan = 1, columnspan = 1, sticky='ew')
                    if 'rem_' in b:
                        e=b.split('rem_')[1]
                        self.entries[e].grid_forget()
                        self.entries[e].grid(row=row-1, column=1, sticky='ew')
                        if 'custom' in e:
                            self.entries['name_' + e].grid_forget()
                            self.entries['name_' + e].grid(row =row-1,column=0,columnspan=1,sticky='ew')
                        else:
                            self.entries_labels[e].grid_forget()
                            self.entries_labels[e].grid(row =row-1,column=0,columnspan=1,sticky='ew')
                        
    def collect(self):
        export = {}
        for ent in self.entries:
            if not 'custom' in ent:
                export[ent]=self.entries[ent].get()
            elif not 'name' in ent:
                export[self.entries['name_' + ent].get()] = self.entries[ent].get()
                
        return export
    
    def accept(self):
        self.mainframe.optimizer.solver_options[self.solver.lower()] = self.collect()
        self.parent.destroy()
    
    def export(self):
        export = self.collect()
        import settings.solver_settings as s
        settings = s.settings
        settings[self.solver.lower()] = export
        file = s.__file__ 
        with open(file, 'w') as f:
            f.write('settings = ' + repr(settings))
            
class ModelSettings(Toplevel):
    def __init__(self, parent, mainframe):
        super().__init__(parent, mainframe)
        self.parent.title('Model settings')
        self.setup_frames({'N': {'row': 0, 'column': 0, 'columnspan': 1, 'rowspan': 1, 'relief': 'flat'},
                           'S': {'row': 1, 'column': 0, 'columnspan': 1, 'rowspan': 1, 'relief': 'flat'}})
        
        self.config = mainframe.optimizer.config
        
        match_attrs = {'ramping':     ['rr'],
                       'startup':     ['sc'],
                       'updowntimes': ['mut','mdt','sc'],
                       'revision':    ['r_length','nr_length']}
        
        widgets = {}
        widgets['buttons'] = {'Accept & Close': # overwrite self.mainframe.mogger.sections
                              {'command' : self.accept,
                               'position': (self.frames['S'],0,0,1,1,'ew')},
                               'Set as default': # exports a setting file
                               {'command' : self.export,
                                'position': (self.frames['S'],0,1,1,1,'ew')}
                              }
        
        widgets['checkButtons'] = {}
    
        for idx,c in enumerate(self.config):
            disable = 0
            for attr in match_attrs[c]:
                for comp in mainframe.mogger.components:
                    if not attr in mainframe.mogger.components[comp].opt_params:
                        disable=1
                        break
                if disable:
                    break
            
            if disable:
                init = (0,'disable','gray62')
            else:
                init = self.config[c]
                    
                
            widgets['checkButtons'][c.upper()] = {'init': init, 
                                                  'position': (self.frames['N'],idx,1,'w')}
            
       # create widgets
        for w in widgets:
            getattr(self, 'create_{}'.format(w))(widgets[w])
                        
    def collect(self):
        export = {}
        for cat in self.intVars:
            export[cat.lower()]=self.intVars[cat].get()
        return export
    
    def accept(self):
        config = self.collect()
        self.mainframe.optimizer.config = config
        self.parent.destroy()
    
    def export(self):
        export = self.collect()
        import optimization.settings.config as c
        config = export
        file = c.__file__ 
        with open(file, 'w') as f:
            f.write('config = ' + repr(config))
            
class InstanceSettings(Toplevel):
    def __init__(self, parent, mainframe):
        super().__init__(parent, mainframe)
        self.parent.title('Instance settings')
        self.setup_frames({'N': {'row': 0, 'column': 0, 'columnspan': 1, 'rowspan': 1, 'relief': 'flat'},
                           'S': {'row': 1, 'column': 0, 'columnspan': 1, 'rowspan': 1, 'relief': 'flat'}})
    
        widgets = {}
        widgets['buttons'] = {'Accept': # overwrite self.mainframe.mogger.sections
                              {'command' : self.accept,
                               'position': (self.frames['S'],0,0,1,1,'ew')},
                               'Accept & Export': # exports an instance file as well 
                               {'command' : lambda: self.accept(export=True),
                                'position': (self.frames['S'],0,1,1,1,'ew')}
                              }
        
        widgets['labels'] = { 
            'Scaling': 
                {'position': (self.frames['N'],0,0,5,'center'),
                 'font': self.utilities['standard_font'] +' 18 bold'},
            'Storage Coupling':
                      {'position': (self.frames['N'],4,0,5,'center'),
                       'font': self.utilities['standard_font'] +' 18 bold'}
            }
        
        widgets['checkButtons'] = {'downscale': {'init': 1,
                                                 'command': lambda: self.enable_disable_widgets(pressed='downscale'),
                                                 'position': (self.frames['N'],1,0,'w')},
                                   'upscale':   {'init': 0, 
                                                 'command': lambda: self.enable_disable_widgets(pressed='upscale'),
                                                 'position': (self.frames['N'],2,0,'w')}
                                   }
        
        if hasattr(mainframe.optimizer.instance, 'initial_params'):
            init = len(mainframe.optimizer.instance.initial_params.index)
        else:
            init = mainframe.optimizer.instance.ntimesteps
        
        widgets['entries'] = {
                   'from': 
                        {'init': 0, 
                         'command': None,
                         'position': (self.frames['N'],1,2,'ew'),
                         'label': (1,1,1,'w','ew'),
                         'label_bg': 'gray94'},
                    'to': 
                        {'init': init-1, 
                         'command': None,
                         'position': (self.frames['N'],1,4,'ew'),
                         'label': (1,3,1,'w','ew'),
                         'label_bg': 'gray94'},
                    'scaling':
                        {'init': 1, 
                         'command': None,
                         'position': (self.frames['N'],2,1,'ew'),
                         'columnspan': 4},
                    'Coupling cycles':
                        {'init': 1, 
                         'command': None,
                         'position': (self.frames['N'],5,1,'ew'),
                         'columnspan': 4,
                         'label': (5,0,1,'w','ew'),
                         'label_bg': 'gray94'}
                        }
            
       # create widgets
        for w in widgets:
            getattr(self, 'create_{}'.format(w))(widgets[w])
            
        self.enable_disable_widgets()
    
    def enable_disable_widgets(self, pressed: str = None, *kwargs):
        if pressed == 'downscale':
            self.intVars['upscale'].set(0)
        elif pressed == 'upscale':
            self.intVars['downscale'].set(0)
            
        if self.intVars['downscale'].get():
            self.entries['from'].configure(state = 'normal', fg = 'black')
            self.entries['to'].configure(state = 'normal', fg = 'black')
        else:
            self.entries['from'].configure(state = 'disable', fg = 'gray62')
            self.entries['to'].configure(state = 'disable', fg = 'gray62')
            
        if self.intVars['upscale'].get():
            self.entries['scaling'].configure(state = 'normal', fg = 'black')
        else:
            self.entries['scaling'].configure(state = 'disable', fg = 'gray62')
            
    def accept(self, export=False):
        if self.intVars['downscale'].get():
            if hasattr(self.mainframe.optimizer.instance, 'initial_params'):
                length = len(self.mainframe.optimizer.instance.initial_params.index)
            else:
                length = self.mainframe.optimizer.instance.ntimesteps
            
            try:
                from_ = self.get_entry_content('from',target_type=int)
                to = self.get_entry_content('to',target_type=int)
                assert from_ <= to,    '\'from\' can not be greater than \'to\'.'
                assert from_ >= 0,     '\'from\' must be greater than 0.'
                assert to <= length-1, f'\'to\' must be smaller than length of original timeseries ({length}).'
            except TypeError as t:
                return
            except  AssertionError as a:
                showerror('Invalid input', *a.args)
                return
                
            self.mainframe.optimizer.instance.downscale(from_ = from_, to = to)
            
        elif self.intVars['upscale'].get():
            try:
                scale_factor = self.get_entry_content('scaling', float)
                assert scale_factor >= 1, 'Can only scale up with factors greater than 1.'
            except TypeError as t:
                return
            except AssertionError as a:
                showerror('Invalid input', *a.args)
                return
            
            self.mainframe.optimizer.instance.upscale(scale_factor = scale_factor)
        
        try:
            cycles = self.get_entry_content('Coupling cycles',target_type=int)
            assert cycles > 0, '\'Coupling cycles\' must be greater than 0.'
            assert cycles <= self.mainframe.optimizer.instance.ntimesteps,\
                f'\'Coupling cycles\' must be smaller than length of timeseries ({self.mainframe.optimizer.instance.ntimesteps}).'
        except TypeError as t:
            return
        except AssertionError as a:
            showerror('Invalid input', *a.args)
            return
            
        self.mainframe.optimizer.instance.cycles = cycles
   
        # export 
        if export and (self.intVars['downscale'].get() or self.intVars['upscale'].get()):
            self.export()
            
        self.parent.destroy()
        self.mainframe.updateWidgets()
        
    def export(self):
        export_dir = self.mainframe.working_directory + '/optimization/instances'
        if not os.path.isdir(export_dir):
            os.makedirs(export_dir)
            
        if self.intVars['upscale'].get():
            initialfile = f'{self.mainframe.optimizer.instance.ntimesteps}.xlsx'
        elif self.intVars['downscale'].get():
            initialfile = '{}_{}_{}.xlsx'.format(self.mainframe.optimizer.instance.ntimesteps, 
                                                 self.get_entry_content('from',target_type=int), 
                                                 self.get_entry_content('to',target_type=int))
            
        export_path = asksaveasfilename(parent = self.parent,
                                        title = 'Export instance',
                                        initialdir = export_dir,
                                        initialfile = initialfile,
                                        filetypes = [('Excel-files (*.xlsx)','*.xlsx')],
                                        defaultextension='.xlsx')
            
        if export_path != '':
            self.mainframe.optimizer.instance.to_excel(export_path)
            showinfo('Instance exported.', f'Instance has been successfully exported to {export_path}.')


def main():
    root = tk.Tk()
    ModEst_GUI(root)
    root.mainloop()
    
if __name__ == "__main__":
    main()
