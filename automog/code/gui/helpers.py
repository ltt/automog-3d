import numpy as np
import logging
import pandas as pd
    
def get_rwth_colors():
    """ return RWTH colors as array """
    rwth_colors = [
        (0,104,180),
        (0,97,101),
        (0,152,161),
        (87,171,39),
        (189,205,0),
        (246,168,0),
        (204,7,30),
        (161,16,53),
        (97,33,88),
        (122,111,172),
    ]
    rwth_colors = np.divide(rwth_colors,256) # percentage of 256 spectrum
    return rwth_colors

def log_heading(message: str):
    """
    Takes a message and creates a nicely formatted heading log message.
    """
    message = "=   {0}   =".format(message)
    length  = len(message)
    logging.info("\n\n\n" + "=" * length + "\n" + message + "\n" + "=" * length + "\n")