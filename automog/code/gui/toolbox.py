### in-built- and site-packages
# GUI packages 
import tkinter as tk
from tkinter import font, ttk, _setit
from tkinter.messagebox import showerror
# other packages
import logging

### own packages 
from gui.helpers import get_rwth_colors

class GUI(tk.Frame):
    """
    Abstract GUI class.
    """

    def __init__(self, parent: tk.Tk):
        """
        Instantiate a GUI object.        

        Parameters
        ----------
        parent : tk.Tk
            DESCRIPTION.

        """
        
        # =====================================================================
        ## setup Window
        
        # general setup
        self.parent = parent
        #self.parent.resizable(False,False)
        
        # setup utilities
        self.utilities = {}
        
        if 'Arial' in font.families():
            self.utilities['standard_font'] = 'Arial'
            self.utilities['standard_font_size'] = ' 9'
            self.parent.option_add('*Font', self.utilities['standard_font'] + ' 9')
        
        # set borderwidth between frames
        self.utilities['borderwidth'] = 10
        
        # =====================================================================
        ## setup Tabs
        self.tab_parent = ttk.Notebook(self.parent)
        

    def setup_menu(self, menus: dict):
        self.menu = {'..': tk.Menu(self.parent)}
        
        def menu_add_cascade(parent, cascade, content):
            """ adds a new cascade to the parent element. """
            self.menu[cascade]    = tk.Menu(parent, tearoff = 0)
            for element in content:
                if type(content[element]) == dict:
                    menu_add_cascade(self.menu[cascade], element, content[element])
                else:
                    self.menu[cascade].add_command(label = element, 
                                                   command = content[element])
            parent.add_cascade(label = cascade, menu = self.menu[cascade]) 
        
        for cascade in menus:
            menu_add_cascade(self.menu['..'], cascade, menus[cascade])
        self.parent.config(menu = self.menu['..'])
        
    def setup_tabs(self, tabs: dict):
        if not hasattr(self, 'tabs'):
                self.tabs = {}
                
        for tab in tabs:
            self.tabs[tab] = tabs[tab]
            self.tabs[tab]['tab'] = ttk.Frame(self.tab_parent)
             
            for orientation in ['row', 'column']:
                self.tabs[tab][orientation + 'ints']    = sum(self.tabs[tab][orientation + 's'])

            # set sizes of Frames
            for [idxrow, row] in enumerate(self.tabs[tab]['rows']):
                self.tabs[tab]['tab'].grid_rowconfigure(idxrow, weight=row, uniform="x")
            for [idxcolumn, column] in enumerate(self.tabs[tab]['columns']):
                self.tabs[tab]['tab'].grid_columnconfigure(idxcolumn, weight=column, uniform="x")
            
            self.tabs[tab]['tab'].grid_propagate(False)   
            self.tab_parent.add(self.tabs[tab]['tab'], text = tab)
        
        self.tab_parent.pack(expand = 1, fill = 'both')
        
    def setup_frames(self, frames: dict, tab_definition: bool = False, set_size: dict = None):
        if not hasattr(self, 'frames'):
                self.frames = {}
        
        if tab_definition:
            for tab in frames: 
                self.frames[tab] = {}
                
                for frame in frames[tab]:
                    options = frames[tab][frame]
                    self.frames[tab][frame] = tk.Frame(self.tabs[tab]['tab'], 
                                                       bd = self.utilities['borderwidth'],
                                                       relief = options['relief'])
                    self.frames[tab][frame].grid(row=options['row'],column=options['column'],
                                                 rowspan=options['rowspan'],columnspan=options['columnspan'],
                                                 sticky='nsew')
        else:
            for frame in frames:
                options = frames[frame]
                self.frames[frame] = tk.Frame(self.parent, 
                                              bd = self.utilities['borderwidth'],
                                              relief = options['relief'])
                self.frames[frame].grid(row=options['row'],column=options['column'],
                                        rowspan=options['rowspan'],columnspan=options['columnspan'],
                                        sticky='nsew')
                
        if tab_definition and set_size != None:
            for tab in set_size:
                for frame in set_size[tab]:
                    # set sizes of Frames
                    for [idxrow, row] in enumerate(set_size[tab][frame]['rows']):
                        self.frames[tab][frame].grid_rowconfigure(idxrow, weight=row, uniform="x")
                    for [idxcolumn, column] in enumerate(set_size[tab][frame]['columns']):
                        self.frames[tab][frame].grid_columnconfigure(idxcolumn, weight=column, uniform="x")
            self.frames[tab][frame].grid_propagate(False)
            
                
    def create_buttons(self, buttons:dict):
        for button in buttons:
            name = button 
            options = buttons[button]
            if not hasattr(self, 'buttons'):
                self.buttons = {}
            frame,row,column,rowspan,columnspan,sticky=options['position']
            if 'font size' in options:
                font_size = ' ' + str(options['font size'])
            else: 
                font_size = self.utilities['standard_font_size']
                
            if 'label' in options:
                label = options['label']
            else: 
                label = name
                
            self.buttons[name] = tk.Button(frame,
                                           text = label,
                                           font=self.utilities['standard_font'] + font_size + ' bold',
                                           command = options['command'])
            self.buttons[name].grid(row = row,column = column, rowspan = rowspan, columnspan = columnspan, sticky='ew')
            
        
    def create_checkButtons(self, intvars: dict):
        for ivar in intvars:    
            name = ivar
            options = intvars[ivar]
            
            if not 'command' in options:
                options['command'] = None
            
            if not hasattr(self, 'checkButtons'):
                self.checkButtons = {}
            if not hasattr(self, 'intVars'):
                self.intVars = {}
            
            if type(options['init']) == tuple:
                init,state,fg = options['init']
                configure=True
            else:
                init = options['init']
                configure=False
            
            self.intVars[name] = tk.IntVar()
            self.intVars[name].set(init)
            
            frame,row,column,sticky=options['position']
            self.checkButtons[name] = tk.Checkbutton(frame, 
                                                     text = name,\
                                                     variable = self.intVars[name], 
                                                     command= options['command'])
            if configure:
                self.checkButtons[name].configure(state = state, fg = fg)
            self.checkButtons[name].grid(row=row,column=column,sticky=sticky) 
    
    def create_entries(self, entries: dict):
        for ent in entries:
            name = ent
            options = entries[ent]
            if not hasattr(self, 'entries'):
                self.entries = {}
                self.entries_labels = {}
            init = options['init']
            frame,row,column,sticky = options['position']
            
            if 'columnspan' in options:
                columnspan = options['columnspan']
            else:
                columnspan = 1
            
            if 'label' in options:
                label_row,label_column,label_columnspan,label_anchor,label_sticky = options['label']
                if 'label_bg' in options:
                    bg = options['label_bg']
                else:
                    bg = 'grey'
                self.entries_labels[name] = tk.Label(frame,
                                                     text = name,bg = bg, anchor = label_anchor, 
                                                     font=self.utilities['standard_font'] +\
                                                         self.utilities['standard_font_size'] +\
                                                             ' italic')
                self.entries_labels[name].grid(row = label_row,column=label_column,columnspan=label_columnspan,sticky=label_sticky)
            self.entries[name] = tk.Entry(frame, highlightcolor='yellow')
            self.entries[name].insert(0,init)
            self.entries[name].grid(row=row, column=column, sticky=sticky,
                                    columnspan=columnspan)
    
    def create_labels(self, labels: dict):
        for lb in labels:
            name = lb
            options = labels[lb]
            if not hasattr(self, 'labels'):
                self.labels = {}
                
            frame,row,column,columnspan,anchor=options['position']
            if 'background' in options:
                bg = options['background']
            else:
                bg = 'grey'
            
            if 'text' in options:
                text = options['text']
            else:
                text = name
            
            self.labels[name] = tk.Label(frame,
                                       text = text, bg = bg,
                                       font= options['font'],
                                       anchor= anchor,
                                       justify ='left')
            self.labels[name].grid(row = row,column=column,columnspan=columnspan,
                                   sticky='ew')
    
    def create_optionMenus(self, stringvars: dict):
        for svar in stringvars:
            name = svar
            options = stringvars[name]
            if not hasattr(self, 'optionMenus'):
                self.optionMenus = {}
            if not hasattr(self, 'stringVars'):
                self.stringVars = {}
                   
            self.stringVars[name]= tk.StringVar()
            self.stringVars[name].set(options['init'])
            
            if 'label_bg' in options:
                    bg = options['label_bg']
            else:
                bg = 'grey'
                
            if 'columnspan' in options:
                columnspan = options['columnspan']
            else:
                columnspan = 1
            
            frame,row,column,sticky=options['position']
            self.optionMenus[name] = tk.OptionMenu(frame,
                                                   self.stringVars[name],
                                                   *options['list'],
                                                   command=options['command'])
            self.optionMenus[name].grid(row=row,column=column,columnspan=columnspan, sticky=sticky)
            label,label_row,label_column,label_sticky=options['label']
            tk.Label(frame,text = label,bg = bg, 
                     font=self.utilities['standard_font'] + self.utilities['standard_font_size'] + ' italic').grid(row = label_row,
                                                                                                                   column=label_column,
                                                                                                                   columnspan=columnspan,
                                                                                                                   sticky=label_sticky)
            self.optionMenus[name].items = options['list']                                                                                                   
                                                                                                                   
    def create_scales(self, scales: dict):
        for sc in scales:
            name = sc
            options = scales[sc]
            if not hasattr(self, 'scales'):
                self.scales = {}
            frame,row,column,sticky = options['position']
            label_row,label_column,label_columnspan,label_anchor,label_sticky = options['label']
            from_,to,resolution,tickinterval = options['ticks']
            if 'label_bg' in options:
                bg = options['label_bg']
            else:
                bg = 'grey'
            tk.Label(frame,
                     text = name,bg = bg, anchor = label_anchor, 
                     font=self.utilities['standard_font'] +\
                          self.utilities['standard_font_size'] +\
                          ' italic').grid(row = label_row,column=label_column,columnspan=label_columnspan,sticky=label_sticky)
            self.scales[name] = tk.Scale(frame, from_= from_, to= to, highlightthickness = 0,\
                                            tickinterval= tickinterval, 
                                            resolution = resolution, 
                                            orient=tk.HORIZONTAL,
                                            command = options['command'])
            try:
                init, state, fg = options['init']
                self.scales[sc].configure(state = state, fg = fg)
            except:
                init = options['init']
                
            self.scales[name].set(init)
            self.scales[name].grid(row=row,column=column, 
                                   columnspan=1,
                                   sticky=sticky)
        
    def create_scrolled_texts(self, scrolled_text: dict):
        for st in scrolled_text:
            name = st
            options = scrolled_text[st]
            if not hasattr(self, 'scrolled_text'):
                self.scrolled_text = {}
                self.text_handler = {}
            frame,row,col,sticky=options['position']
            
            kwargs = {}
            
            for attr in ['height','width']:
                if attr in options:
                    kwargs[attr] = options[attr]
                else:
                    kwargs[attr] = None
                    
            self.scrolled_text[name] = tk.scrolledtext.ScrolledText(frame, font='Consolas 8',
                                                                    **kwargs)
            self.scrolled_text[name].grid(row=row,column=col,sticky=sticky)
            self.text_handler[name] = TextHandler(self.parent, self.scrolled_text[name])

    def updateOptionMenu(self, name: str, items: list, init: str, command):
        menu = self.optionMenus[name]["menu"]
        menu.delete(0, "end")
        for string in items:
            menu.add_command(label=string, 
                             command=_setit(self.stringVars[name], string, command))
        
        self.stringVars[name].set(init)
        self.optionMenus[name].items = items
        
    def get_entry_content(self, entry, target_type = float):
        str_in = self.entries[entry].get()
        error_msg = 'Input of \'{}\' needs to be convertible to type \'{}\'.'.format(entry, target_type) 
        
        try: 
            out = target_type(str_in)
        except:
            try:
                if target_type == float:
                    out = target_type(str_in.replace(',','.'))
                else: 
                    raise TypeError(error_msg) 
            except:
                showerror('Wrong input type', error_msg)
                raise TypeError(error_msg)
        return out
    
    def calculateFrameGeometry(self, tab, frame):
        """ Calculate width and height of frame in specified tab of GUI """
        geometry = {'width': 0,'height': 0}
    
        grid_info = self.frames[tab][frame].grid_info()
        idxrow = list(range(grid_info['row'],
                            grid_info['row']+grid_info['rowspan']))
        idxcolumn = list(range(grid_info['column'],
                               grid_info['column']+grid_info['columnspan']))
        
        for col in idxcolumn:
            geometry['width']  += int(self.utilities['window_width']*\
                                      self.tabs[tab]['columns'][col]/\
                                      self.tabs[tab]['columnints'])
        for row in idxrow:
            geometry['height'] += int(self.utilities['window_height']*\
                                      self.tabs[tab]['rows'][row]/\
                                      self.tabs[tab]['rowints'])
        return geometry
    
class Mainframe(GUI):
    def __init__(self, parent: tk.Tk, frac_height: float = 0.8, frac_width: float = 0.9):
        """
        Instantiate a Mainframe object.        

        Parameters
        ----------
        parent : tk.Tk
            DESCRIPTION.
        frac_height : float, optional
            fraction height: window/screen. The default is 0.8.
        frac_width : float, optional
            fraction width: window/screen. The default is 0.8.

        Returns
        -------
        None.

        """
        # call parent-class constructor
        super().__init__(parent)
        
        # add utilities
        
        # boolean if GUI has been completely built
        self.utilities['GUI_built'] = False
        
        # get width and height of screen
        self.utilities['screen_height'] = parent.winfo_screenheight()
        self.utilities['screen_width']  = parent.winfo_screenwidth()
        
        # define fraction of used screen
        self.utilities['fraction_height'] = frac_height
        self.utilities['fraction_width']  = frac_width
        
        # set width and height of window
        self.utilities['window_height'] = int(self.utilities['fraction_height']*self.utilities['screen_height'])
        self.utilities['window_width']  = int(self.utilities['fraction_width']*self.utilities['screen_width'])
        
        # set colormap
        self.utilities['colormap'] = get_rwth_colors()

        # set geometry
        self.parent.geometry('{}x{}+{}+{}'.format(int(self.utilities['window_width']),
                                                  int(self.utilities['window_height']),
                                                  int((1-self.utilities['fraction_width'])/ 
                                                       2*self.utilities['screen_width']),
                                                  int((1-self.utilities['fraction_height'])/ 
                                                       2*self.utilities['screen_height'])
                                                  )
                             )

class Toplevel(GUI):
    def __init__(self, parent, mainframe):
        # call parent-class constructor
        super().__init__(parent)

        self.parent.grab_set()
        self.mainframe = mainframe
        
        if hasattr(self.mainframe, 'icon'):
            self.parent.iconbitmap(self.mainframe.icon)
            
    def set_geometry(self, width_share: float=0.5, height_share: float=1):
        self.parent.geometry('{}x{}+{}+{}'.format(int(width_share*self.mainframe.utilities['window_width']),
                                                  int(height_share*self.mainframe.utilities['window_height']),
                                                  int((1-self.mainframe.utilities['fraction_width'])/ 
                                                       2*self.mainframe.utilities['screen_width']),
                                                  int((1-self.mainframe.utilities['fraction_height'])/ 
                                                       2*self.mainframe.utilities['screen_height'])
                                                  )
                             )
    def set_frame_sizes(self):
        self.utilities['rowints']    = sum(self.utilities['rows'])
        self.utilities['columnints'] = sum(self.utilities['columns']) 
        
        # set sizes of Frames
        for [idxrow, row] in enumerate(self.utilities['rows']):
            self.parent.grid_rowconfigure(idxrow, weight=row, uniform="x")
        for [idxcolumn, column] in enumerate(self.utilities['columns']):
            self.parent.grid_columnconfigure(idxcolumn, weight=column, uniform="x")
        self.parent.grid_propagate(False)   
    
class TextHandler(logging.Handler):
    """This class allows you to log to a Tkinter Text or ScrolledText widget"""

    def __init__(self, parent, text):
        # run the regular Handler __init__
        logging.Handler.__init__(self)
        # Store a reference to the Text it will log to
        self.text = text
        self.parent = parent

    def emit(self, record):
        msg = self.format(record)
        self.text.configure(state='normal')
        self.text.insert(tk.END, msg + '\n')
        self.text.configure(state='disabled')
        # Autoscroll to the bottom
        self.text.yview(tk.END)
        self.parent.update()
        
class TextWriter(object):
    """This class allows you to log to a Tkinter Text or ScrolledText widget"""

    def __init__(self, parent, text):
        # Store a reference to the Text it will log to
        self.text = text
        self.parent = parent

    def write(self, record):
        msg= record
        self.text.configure(state='normal')
        self.text.insert(tk.END, msg + '\n')
        self.text.configure(state='disabled')
        # Autoscroll to the bottom
        self.text.yview(tk.END)
        self.parent.update()
    def flush(self):
        pass
        