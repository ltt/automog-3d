"""
This script containts the class linearizer which performs all model fitting 
tasks during a manual and automated model generation process.
"""

### in-built and site-packages
import numpy as np
from sklearn.linear_model import LinearRegression as LR
from sklearn.metrics import mean_squared_error as mse
import pwlf
import time

### own packages
from modeling.pwlf_miqp import MIQP_model
from modeling.Hinging_Hyperplane import hinging_hyperplane as hh

class linearizer(object):
    """
    This class containts all necessary functions for model fitting.
    """
    def __init__(self, mogger):
        """
        Class constructor.

        Args:
            mogger (AutoMoG): Model Generator. 
                For more information see automog.py

        """
        self.mogger = mogger
        
    def linearize_all(self, relative:bool = False):
        """
        Linearize all components contained in the components attribute 
        of the model generator.

        Args:
            relative (bool, optional): If true, relative values are saved as 
                regression results, otherwise absolute values.
                Defaults to False.
        """
        for component in self.mogger.components.values():
            self.linearize(component, relative=relative)
            
    def linearize(self, comp, relative:bool = False):
        """
        Linearize a specified component contained in the components attribute 
        of the model generator.

        Args:
            component (MOG_Component): Energy system component.
                For more information see automog.py
            relative (bool, optional): If true, relative values are saved as 
                regression results, otherwise absolute values.
                Defaults to False.
        """

        for state in comp.operating_points:
            ### PERFORM REGRESSION 
            # if regression with the specified number of sections has not already been done
            if not self.mogger.sections[comp.name][state] in self.mogger.curves[comp.name][state]:
                ### CREATE RESULTS STORAGE
                # o_n: nominal output, 
                # e_n: nominal efficieny, 
                # v: vertices, 
                # ic: information criterion value
                self.mogger.curves[comp.name][state][self.mogger.sections[comp.name][state]] = {'o_n': None, 
                                                                                                'e_n': None, 
                                                                                                'v': None, 
                                                                                                'ic': None}
                # get non-zero values of operating points
                comp_data = comp.operating_points[state]
                 
                ### FITS WITH ONE SECTION
                if self.mogger.sections[comp.name][state] == 1:
                    # get data for model
                    x = comp_data[comp.outputs[0]].values.reshape(-1, 1)
                    y = comp_data[comp.inputs[0]].values.reshape(-1, 1)
                    X = comp_data[comp.outputs[0]]
                    Y = comp_data[comp.inputs[0]]
            
                    # stack all input columns except first one with output column
                    for i in range(1, len(comp.inputs)):
                        x = np.column_stack((x, comp_data[comp.inputs[i]].values.reshape(-1, 1)))

                    ### UNIVARIATE COMPONENTS
                    # --> sklearn
                    if len(comp.inputs) == 1:
                        # create and fit model
                        model = LR() 
                        start = time.time()
                        model.fit(x, y) 
                        sol_time = time.time()-start
                        # mean squared error
                        ypred = model.predict(x)
                        me = mse(y, ypred)
                        # get model parameters: intercept and slope
                        intercept = model.intercept_[0]
                        slope = model.coef_[0][0]

                        # get nominal values: output, input
                        o_n = max(X)
                        i_n = o_n * slope + intercept

                        # get vertices (lower bounds of sections):
                        # relative to nominal values
                        if relative:
                            v = {(min(X) / o_n): (min(X) * slope + intercept) / i_n,
                                 1.0: (o_n * slope + intercept) / i_n
                                 }
                        # absolute values
                        else:
                            v = {min(X): min(X) * slope + intercept,
                                 o_n: o_n * slope + intercept
                                 }

                    ### MULTIVARIATE COMPONENTS
                    # --> hinging hyperplance algorithm
                    else:
                        # create and fit model
                        start = time.time()
                        model = hh.piecewiseLinearModel_bt({'X': x, 'Y': y[:, 0]},
                                                            self.mogger.sections[comp.name][state] - 1, 1, self.mogger.hull_acc)
                        sol_time = time.time() - start

                        # mean squared error
                        me = model.mse

                        # get model parameters: intercept and slope
                        intercept = model.theta[0]
                        slope = model.theta[1:]

                        # get nominal values: output, input
                        o_n = np.max(x, axis=0)
                        i_n = np.dot(o_n, slope) + intercept

                        v = None
                
                    ### GET DATA FOR APPLICATION OF INFORMATION CRITERION
                    # number of data points
                    d = len(y)
                    
                    # model error (sum of squared residuals)
                    model_error = me * d
                    
                    # number of regression parameters used to describe the model
                    # freedom degrees: slope, intercept, assumption of normal distribution for each section
                    k = 3*self.mogger.sections[comp.name][state]
                       
                ### FITS WITH MULTIPLE SECTIONS
                else:
                    ### UNIVARIATE COMPONENTS
                    # --> python-pwlf or miqp-models (Rebennack/Krasko, Kong/Maravelias)
                    if len(comp.inputs) == 1:
                        ### usage of python-pwlf
                        if self.mogger.options['lin_method'] == 'PYTHON-PWLF':
                            # get data for model  
                            x = comp_data[comp.outputs[0]]
                            y = comp_data[comp.inputs[0]]
                            # create and fit model
                            start = time.time()
                            model = pwlf.PiecewiseLinFit(x, y)
                            sol_time = time.time()-start
                            model.fit(self.mogger.sections[comp.name][state])
                            
                            # get model parameters: intercepts and slopes
                            intercepts = model.intercepts
                            slopes = model.slopes
                            
                        ### usage of miqp-models (Rebennack/Krasko, Kong/Maravelias)
                        elif self.mogger.options['lin_method'] in ['REBENNACK/KRASKO','KONG/MARAVELIAS']:
                            assert self.mogger.options['lin_method'] in ['REBENNACK/KRASKO','KONG/MARAVELIAS']
                            formulation = self.mogger.options['lin_method'].replace('/','_').lower() 
                            solver = self.mogger.mainframe.stringVars['solver'].get().lower()
                            # get data for model
                            x = comp_data[comp.outputs[0]]
                            y = comp_data[comp.inputs[0]]
                            # create and fit model                    
                            model = MIQP_model(x, y, formulation=formulation,
                                               solver = solver)
                            model.fit(self.mogger.sections[comp.name][state])
                            sol_time = model.sol_time
                            
                            # get model parameters: intercepts and slopes
                            intercepts = model.intercepts 
                            slopes = model.slopes 
                            
                    ### MULTIVARIATE COMPONENTS
                    # --> hinging hyperplance algorithm 
                    else:
                        # get data for model
                        x = comp_data[comp.outputs[0]].values.reshape(-1, 1)
                        y = comp_data[comp.inputs[0]].values.reshape(-1, 1)
                        for i in range(1, len(comp.inputs)):
                            x = np.column_stack((x, comp_data[comp.inputs[i]].values.reshape(-1, 1)))

                        # check for linearizations that already exists
                        sections = list(self.mogger.curves[comp.name][state].keys())
                        sections.remove(self.mogger.sections[comp.name][state])

                        # use hinging hyperplane algorithm to create and fit model
                        loops = 5  # number of models to be created (the beset is chosen)
                        if not sections:
                            start = time.time()
                            model = hh.piecewiseLinearModel_bt({'X': x, 'Y': y[:, 0]},
                                                               self.mogger.sections[comp.name][state] - 1,
                                                               loops, self.mogger.hull_acc)
                            sol_time = time.time() - start

                        else:
                            max_section = max(sections)
                            if 1 < max_section < self.mogger.sections[comp.name][state]:
                                # add additional section to existing model if at least one hinge existed
                                existing_model = self.mogger.curves[comp.name][state][max_section]['model']
                                start = time.time()
                                model = hh.piecewiseLinearModel_bt({'X': x, 'Y': y[:, 0]},
                                                                   self.mogger.sections[comp.name][state] - 1,
                                                                   loops, self.mogger.hull_acc, existing_model)
                                sol_time = time.time() - start
                            else:
                                start = time.time()
                                model = hh.piecewiseLinearModel_bt({'X': x, 'Y': y[:, 0]},
                                                                   self.mogger.sections[comp.name][state] - 1,
                                                                   loops, self.mogger.hull_acc)
                                sol_time = time.time() - start

                        # mean squared error
                        me = model.mse

                        # get model parameters: intercept and slope
                        intercepts = []
                        slopes = []
                        for i in range(len(model.theta)):
                            intercepts.append(model.theta[i][0])
                            slopes.append(model.theta[i][1:])
                    
                    ### GET DATA FOR APPLICATION OF INFORMATION CRITERION
                    ### PROCESS OUTPUT: UNIVARIATE COMPONENTS
                    if len(comp.inputs) == 1:
                        # get nominal values
                        # output
                        o_n = max(x)
                        # input
                        i_n = o_n * slopes[-1] + intercepts[-1]           
                        
                        # get vertices (lower bounds of sections): relative to nominal values
                        v = {}
                        
                        # relative to nominal values
                        if relative:
                            v[min(x) / o_n] = (min(x) * slopes[0] + intercepts[0]) / i_n
                        # absolute values
                        else:
                            v[min(x)] = min(x) * slopes[0] + intercepts[0]
    
                        for i in range(1, len(intercepts)):
                            # get abscissa of intersection of linear section i and i-1
                            # TODO: what if slopes are equal??
                            xh = (intercepts[i] - intercepts[i - 1]) / (slopes[i - 1] - slopes[i])
    
                            # relative to nominal values
                            if relative:
                                v[xh / o_n] = (xh * slopes[i] + intercepts[i]) / i_n
                            # absolute values
                            else:
                                v[xh] = xh * slopes[i] + intercepts[i]
    
                        # relative to nominal values
                        if relative:
                            v[1.0] = 1.0
                        # absolute values
                        else:
                            v[o_n] = i_n
    
                        # apply information criterion
                        # number of regression parameters used to describe the model
                        # freedom degrees: slope, intercept, assumption of normal distribution for each section
                        k = 3 * self.mogger.sections[comp.name][state]
                        # number of data points
                        d = len(x)
                        
                        # get model error
                        if self.mogger.options['lin_method'] == 'PYTHON-PWLF':
                            model_error = model.ssr
                        elif self.mogger.options['lin_method'] in ['REBENNACK/KRASKO','KONG/MARAVELIAS']:
                            model_error = model.model_error
                    
                        # mean error
                        me = model_error / d
                        
                    ### PROCESS OUTPUT: MULTIVARIATE COMPONENTS
                    else:
                        # get nominal values
                        # output
                        o_n = np.max(x, axis=0)
                        # input
                        i_n = np.dot(o_n, slopes[-1]) + intercepts[-1]
    
                        v = None
    
                        # apply information criterion
                        # number of regression parameters used to describe the model
                        # freedom degrees: slope, intercept, assumption of normal distribution for each section
                        k = 3*self.mogger.sections[comp.name][state]
                        # number of data points
                        d = len(x)
                        # model error
                        model_error = me * d
                
                ### CALCULATE INFORMATION CRITERION
                # base term
                ic = d * np.log(me)
                # supplementary terms
                # akaike information criterion
                if self.mogger.options['icrit'] == "AIC":
                    ic += 2*k
                # corrected akaike information criterion
                elif self.mogger.options['icrit'] == "AIC_C": 
                    ic += 2 * k + (2 * (k ** 2) + 2 * k) / (d - k - 1)
                # bayesian information criterion
                elif self.mogger.options['icrit'] == "BIC":
                    ic += np.log(d)*k

                ### WRITE RESULTS
                attrs =  ['o_n', 'e_n', 'v', 'ic', 'model_error', 'mse', 'sol_time']
                vals  =  [o_n, o_n/i_n, v, ic, model_error, me, sol_time]
                
                if len(comp.inputs) > 1:
                    attrs += ['model']
                    vals  += [model]
                    
                for attr,val in zip(attrs,vals):
                    self.mogger.curves[comp.name][state][self.mogger.sections[comp.name][state]][attr] = val
