""" This script contains all data processing functions for the pwlf-miqp model
"""

def generate_input_dictionary(x, y, z,
                              M_a        =  1e6, 
                              M          =  1e6):
    ### sets
    input_dictionary = {}
    input_dictionary["B_set"]   = {None: list(range(1, z + 2))}
    input_dictionary["B_1_set"] = {None: list(range(1, z + 1))}
    input_dictionary["B_2_set"] = {None: list(range(1, z))}
    input_dictionary["I_set"]   = {None: list(range(1, len(x)+1))}
    input_dictionary["I_1_set"] = {None: list(range(1, len(x)))}
    
    #### parameters
    # sort data in ascending order: important for miqp-pwlf models
    x,y = zip(*sorted(zip(x,y)))
    input_dictionary["X"]          = {i+1: x[i] 
                                      for i in range(len(x))}
    input_dictionary["Y"]          = {i+1: y[i] 
                                      for i in range(len(y))}
    
    M_a = abs(y[0]-y[-1])
    input_dictionary["M_a"]        = {i+1: M_a 
                                      for i in range(len(x))}
    
    max_delta_y = max(b-a for (a,b) in zip(y[:-1], y[1:]))
    min_delta_x = min(b-a for (a,b) in zip(x[:-1], x[1:]))
    C_upper = max_delta_y/min_delta_x
    C_lower = 0
    input_dictionary["C_upper"]    = {b+1: C_upper 
                                      for b in range(z)}
    input_dictionary["C_lower"]    = {b+1: C_lower 
                                      for b in range(z)}
    
    max_delta_x = max(b-a for (a,b) in zip(x[:-1], x[1:]))
    M = max_delta_x*C_upper
    input_dictionary["M"]        = {i+1: M 
                                    for i in range(len(x))}
    
    D_upper = max(y)
    D_lower = y[-1]  - C_upper*x[-1]
    input_dictionary["D_upper"]    = {b+1: D_upper 
                                      for b in range(z)}
    input_dictionary["D_lower"]    = {b+1: D_lower 
                                      for b in range(z)}
    
    # input_dictionary["M_a"]        = {i+1: max([abs(y[i] - C_upper * x[i] - D_upper),
    #                                             abs(y[i] - C_upper * x[i] - D_lower),
    #                                             abs(y[i] - C_lower * x[i] - D_upper),
    #                                             abs(y[i] - C_lower * x[i] - D_lower)]) 
    #                                   for i in range(len(x))}
    
    # input_dictionary["M"]        = {i+1: D_upper - D_lower - x[i]*(C_lower-C_upper) 
    #                                 for i in range(len(x))}
    
    return {None: input_dictionary}