import pyomo.environ as pe
from pyomo.opt import TerminationCondition
import pandas as pd
import sys

# TODO: adapt constraint rules commments to rebennack2020_rebennack_vs_kong.pdf
class kong_maravelias_model:

    def __init__(self, quadratic=True):
        """This method initializes the model as an abstract pyomo model."""
        self.model = pe.AbstractModel()
        self.quadratic=quadratic
        self.setupSets()
        self.setupParameters()
        self.setupVariables()
        self.setupObjective()
        self.setupConstraints()

    def setupSets(self):
        """This method sets up all Sets required by all optimization models."""
        # TODO: think about adapting sets dynamically
        # set of breakpoints
        self.model.B_set = pe.Set(
            within=pe.NonNegativeIntegers)
        # set of affine segments
        self.model.B_1_set = pe.Set(
            within=pe.NonNegativeIntegers)
        # set of left segments
        self.model.B_2_set = pe.Set(
            within=pe.NonNegativeIntegers)
        # set of datapoints
        self.model.I_set = pe.Set(
            within=pe.NonNegativeIntegers)
        # set of left datapoints
        self.model.I_1_set = pe.Set(
            within=pe.NonNegativeIntegers)

    def setupParameters(self):
        """This method sets up all Parameters required by all optimization models."""
        ### data
        # input data for input side
        self.model.X = pe.Param(
            self.model.I_set,
            within=pe.Reals)
        # input data for output side
        self.model.Y = pe.Param(
            self.model.I_set,
            within=pe.Reals)
        
        ### BigMs
        # upper bound for xi
        self.model.M_a = pe.Param(
            self.model.I_set,
            within=pe.Reals)
        # 
        self.model.M = pe.Param(
            self.model.I_set,
            within=pe.Reals)
        
        ### bounds
        # upper bound for d
        self.model.D_upper = pe.Param(
            self.model.B_1_set,
            within=pe.Reals)
        # lower bound for d
        self.model.D_lower = pe.Param(
            self.model.B_1_set,
            within=pe.Reals)
        # upper bound for c
        self.model.C_upper = pe.Param(
            self.model.B_1_set,
            within=pe.Reals)
        # lower bound for c
        self.model.C_lower = pe.Param(
            self.model.B_1_set,
            within=pe.Reals)

    def setupVariables(self):
        """This method sets up all Variables required by all optimization models."""
        ### continous variables
        # objective value
        self.model.z = pe.Var(
            within=pe.Reals)
        # slope of affine segment b
        self.model.c = pe.Var(
            self.model.B_1_set,
            within=pe.Reals,
            bounds=self.bounds_c_rule)
        # intercept of affine segment b
        self.model.d = pe.Var(
            self.model.B_1_set,
            within=pe.Reals,
            bounds=self.bounds_d_rule)
        # residual for data point i
        self.model.xi = pe.Var(
            self.model.I_set,
            within=pe.Reals,
            bounds=self.bounds_xi_rule)
        # non-negative slack variables
        self.model.p_up = pe.Var(
            self.model.I_set,
            self.model.B_1_set,
            within=pe.Reals,
            bounds=self.bounds_pq_rule)
        # non-negative slack variables
        self.model.p_down = pe.Var(
            self.model.I_set,
            self.model.B_1_set,
            within=pe.Reals,
            bounds=self.bounds_pq_rule)
        # non-negative slack variables
        self.model.q_up = pe.Var(
            self.model.I_set,
            self.model.B_1_set,
            within=pe.Reals,
            bounds=self.bounds_pq_rule)
        # non-negative slack variables
        self.model.q_down = pe.Var(
            self.model.I_set,
            self.model.B_1_set,
            within=pe.Reals,
            bounds=self.bounds_pq_rule)

        ### binary variables
        # 1: data point i is associated with affine segment b, 0: else
        self.model.delta = pe.Var(
            self.model.I_set,
            self.model.B_1_set,
            within=pe.Binary)
        # needed for Kong Maravelias formulation
        self.model.u = pe.Var(
            self.model.I_set,
            self.model.B_1_set,
            within=pe.Binary)
        self.model.v = pe.Var(
            self.model.I_set,
            self.model.B_1_set,
            within=pe.Binary)
        self.model.delta_F = pe.Var(
            self.model.I_set,
            self.model.B_1_set,
            within=pe.Binary)
        self.model.delta_L = pe.Var(
            self.model.I_set,
            self.model.B_1_set,
            within=pe.Binary)


    def setupObjective(self):
        """This method declares the pe.Objective required by all optimization models."""
        self.model.Objective = pe.Objective(rule=self.objective_rule, sense=pe.minimize)

    def setupConstraints(self):
        """This method sets up all Constraints required by all optimization models."""
        # define objective rule as constraint in order to obtain a linear objective; optional use absolute error or squared error
        if self.quadratic:
            self.model.constraint_1a = pe.Constraint(
                rule=self.constraint_1a_rule)
        else:
            self.model.constraint_1a_ = pe.Constraint(
                rule=self.constraint_1a_linear_rule)
        # calculate xi[i] as abolute of the residual
        self.model.constraint_1b = pe.Constraint(
            self.model.I_set,
            self.model.B_1_set,
            rule=self.constraint_1b_rule)
        self.model.constraint_1c = pe.Constraint(
            self.model.I_set,
            self.model.B_1_set,
            rule=self.constraint_1c_rule)
        # each data point is associated with exactly one affine segment
        self.model.constraint_1d = pe.Constraint(
            self.model.I_set,
            rule=self.constraint_1d_rule)

        self.model.constraint_2c = pe.Constraint(
            self.model.I_set,
            self.model.B_1_set,
            rule=self.constraint_2c_rule)
        self.model.constraint_2d = pe.Constraint(
            self.model.B_1_set,
            rule=self.constraint_2d_rule)
        self.model.constraint_2e = pe.Constraint(
            self.model.B_1_set,
            rule=self.constraint_2e_rule)
        self.model.constraint_2f = pe.Constraint(
            self.model.I_set,
            self.model.B_2_set,
            rule=self.constraint_2f_rule)
        self.model.constraint_2g = pe.Constraint(
            self.model.I_set,
            self.model.B_2_set,
            rule=self.constraint_2g_rule)
        self.model.constraint_2h = pe.Constraint(
            self.model.I_set,
            self.model.B_1_set,
            rule=self.constraint_2h_rule)
        self.model.constraint_2i = pe.Constraint(
            self.model.I_set,
            self.model.B_1_set,
            rule=self.constraint_2i_rule)
        self.model.constraint_2j = pe.Constraint(
            self.model.I_set,
            self.model.B_2_set,
            rule=self.constraint_2j_rule)
        self.model.constraint_2k = pe.Constraint(
            self.model.I_set,
            self.model.B_2_set,
            rule=self.constraint_2k_rule)
        self.model.constraint_2l = pe.Constraint(
            self.model.I_set,
            self.model.B_2_set,
            rule=self.constraint_2l_rule)
        self.model.constraint_2m = pe.Constraint(
            self.model.I_set,
            self.model.B_2_set,
            rule=self.constraint_2m_rule)
        self.model.constraint_2n = pe.Constraint(
            self.model.I_set,
            self.model.B_2_set,
            rule=self.constraint_2n_rule)
        self.model.constraint_2o = pe.Constraint(
            self.model.I_set,
            self.model.B_2_set,
            rule=self.constraint_2o_rule)
        self.model.constraint_2p = pe.Constraint(
            self.model.I_set,
            self.model.B_2_set,
            rule=self.constraint_2p_rule)

    def run(self, input_dict: dict = None, filepath: str = None, solver: str = 'gurobi', solver_options: dict = None,
            debug: bool = False, skip_instantiation: bool = False):
        """This method is used to start an optimization.

        To start an optimization it is necessary to provide a file with the necessary input data.
        This file includes all information required to instantiate the optimization problem.

        Args:
            input_dict (dict): Input dictionary that contains all the necessary data for the optimization
            filename (str): Filename of the previously generated input file
            solver (str): Name of the solver to be used. Standard is 'gurobi'
        """
        #debug=True
        self.mysolver = pe.SolverFactory(solver, options=solver_options)

        if not skip_instantiation:
            if input_dict:
                self.model_instance = self.model.create_instance(data=input_dict)
            elif filepath:
                self.model_instance = self.model.create_instance(filename=filepath)
            else:
                raise AttributeError(
                    "Please provide input data either as input dictionary for pyomo or as filepath to an input file.")

        if (solver == "gurobi_persistent"):
            self.mysolver.set_instance(self.model_instance, symbolic_solver_labels=True)
            self.results = self.mysolver.solve(tee=debug)
        else:
            self.results = self.mysolver.solve(self.model_instance, tee=debug)

        if self.results.solver.termination_condition == TerminationCondition.optimal:
            self.model_instance.solutions.load_from(self.results)
        else:
            self.write_model_to_file('model.txt')
        
    def write_model_to_file(self, filename):
        with open(filename, 'w') as output:
            sys.stdout = output
            self.model_instance.pprint()
            sys.stdout = sys.__stdout__
    # Evaluation methods
    @staticmethod
    def get_dataframe_from_result(model_instance_variable, unstack: list = None):
        """Returns a dataframe with the results from the optimization model."""

        # Create empty dictionary
        variable = {}
        # for all indices in the model_instance_variable put its key-value pair in the dictionary
        for index in model_instance_variable:
            variable[index] = model_instance_variable[index].value

        # Create pandas Series from
        variable = pd.Series(variable)
        if unstack:
            variable = variable.unstack(unstack)

        return variable

    @staticmethod
    def get_dataframe_from_parameter(model_instance_variable, unstack: list = None):
        """Returns a dataframe with the results from the optimization model."""

        # Create empty dictionary
        variable = {}
        # for all indices in the model_instance_variable put its key-value pair in the dictionary
        for index in model_instance_variable:
            variable[index] = model_instance_variable[index]

        # Create pandas Series from
        variable = pd.Series(variable)
        if unstack:
            variable = variable.unstack(unstack)

        return variable

    def get_objective(self):
        return self.model_instance.Objective()
    
    def get_solution_time(self):
        try: 
            return self.results.Solver._list[0]['Time']
        except: #solver == 'gurobi_persistent':
            return self.mysolver.get_model_attr('Runtime')
        
    @staticmethod
    def bounds_c_rule(model, b: int) -> "pyomo rule":
        """
        Bounds for variable c.
        """
        return (model.C_lower[b], model.C_upper[b])

    @staticmethod
    def bounds_d_rule(model, b: int) -> "pyomo rule":
        """
        Bounds for variable d.
        """
        return (model.D_lower[b], model.D_upper[b])

    @staticmethod
    def bounds_xi_rule(model, i: int) -> "pyomo rule":
        """
        Bounds for variable xi.
        """
        return (0, model.M_a[i])

    @staticmethod
    def bounds_pq_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Bounds for variables p and q.
        """
        return (0, model.M_a[i])

    @staticmethod
    def objective_rule(model) -> "pyomo rule":
        """This method is used as an pe.Objective for the optimization, using weight factors for all impact categories.

        Args:
            model: The equivalent of "self" in pyomo optimization models
        """
        return model.z

    @staticmethod
    def constraint_1a_rule(model) -> "pyomo rule":
        """
        Provides the rule for the objective in order to achieve linearity in the objective.
        """
        return (
                model.z ==
                sum(model.xi[i]
                    for i in model.I_set)
        )
    
    @staticmethod
    def constraint_1a_linear_rule(model) -> "pyomo rule":
        """
        Provides the rule for the objective in order to achieve linearity in the objective.
        """
        return (
            model.z ==
            sum(model.xi[i]
                for i in model.I_set)
                ) 
    
    @staticmethod
    def constraint_1b_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Together with constraint 1c: define the value of xi[i] as the
        absolute difference of Y[i] and its associated segment's b function value
        p[X[i]] = c[b]*X[i]+d[b]
        """
        return (
                model.Y[i] - (model.c[b] * model.X[i] + model.d[b]) <=
                model.xi[i] + model.M_a[i] * (1 - model.delta[i, b])
        )
    
    @staticmethod
    def constraint_1c_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Together with constraint 1b: defines the value of xi[i] as the
        absolute difference of Y[i] and its associated segment's b function value
        p[X[i]] = c[b]*X[i]+d[b]
        """
        return (
                (model.c[b] * model.X[i] + model.d[b]) - model.Y[i] <=
                model.xi[i] + model.M_a[i] * (1 - model.delta[i, b])
        )
    
    @staticmethod
    def constraint_1d_rule(model, i: int) -> "pyomo rule":
        """Enforces that each data point is associated with exactly one affine segment."""
        return (
                sum(model.delta[i, b]
                    for b in model.B_1_set) == 1
        )

    @staticmethod
    def constraint_2c_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Mind the index error for delta_L in rebennack2020_rebennack_vs_kong.pdf. 
        Correct formulation in Kong2020.pdf
        
        """
        if i-1 > 0:
            return (
                    model.delta[i, b] ==
                    model.delta[i-1, b] + model.delta_F[i, b] - model.delta_L[i-1, b]
                    )
        else:
            return pe.Constraint.Skip

    @staticmethod
    def constraint_2d_rule(model, b: int) -> "pyomo rule":
        """
        Only one first point per section b. 
        """
        return (
                sum(model.delta_F[i, b]
                    for i in model.I_set) == 1
        )

    @staticmethod
    def constraint_2e_rule(model, b: int) -> "pyomo rule":
        """
        Only one last point per section b. 
        """
        return (
                sum(model.delta_L[i, b]
                    for i in model.I_set) == 1
        )
    
    @staticmethod
    def constraint_2f_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Kong and Maravelias
        """
        return (
                sum(model.delta_F[i_prime, b]
                    for i_prime in range(1,i+1)) >=
                sum(model.delta_F[i_prime, b+1]
                    for i_prime in range(1,i+1))        
        )

    @staticmethod
    def constraint_2g_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Kong and Maravelias
        """
        return (
                sum(model.delta_L[i_prime, b]
                    for i_prime in range(1,i+1)) >=
                sum(model.delta_L[i_prime, b+1]
                    for i_prime in range(1,i+1))        
        )
    
    @staticmethod
    def constraint_2h_rule(model, i: int, b: int) -> "pyomo rule":
        """
        If i is first point of b, point i is part of section b
        delta_F[i, b] => delta[i, b] 
        """
        return (
                model.delta_F[i, b] <=
                model.delta[i, b]
        )

    @staticmethod
    def constraint_2i_rule(model, i: int, b: int) -> "pyomo rule":
        """
        If i is last point of b, point i is part of section b
        delta_L[i, b] => delta[i, b]
        """
        return (
                model.delta_L[i, b] <=
                model.delta[i, b]
        )
    
    @staticmethod
    def constraint_2j_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Kong and Maravelias
        """
        return (
                model.X[i] * model.c[b+1] + model.d[b+1] - (model.X[i] * model.c[b] + model.d[b]) ==
                model.p_up[i, b] - model.p_down[i,b]
        )
    
    @staticmethod
    def constraint_2k_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Kong and Maravelias
        """
        if i+1 <= max(model.I_set):
            return (
                    model.X[i+1] * model.c[b] + model.d[b] - (model.X[i+1] * model.c[b+1] + model.d[b+1]) ==
                    model.q_up[i+1, b+1] - model.q_down[i+1,b+1]
                    )
        else:
            return pe.Constraint.Skip
    
    @staticmethod
    def constraint_2l_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Kong and Maravelias
        """
        return (
                model.p_up[i,b] <= model.M_a[i] * (1 - model.u[i,b])
        )
    
    @staticmethod
    def constraint_2m_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Kong and Maravelias
        """
        if i+1 <= max(model.I_set):
            return (
                    model.q_up[i+1,b+1] <= model.M_a[i] * (1 - model.u[i,b])
                    )
        else:
            return pe.Constraint.Skip
    
    @staticmethod
    def constraint_2n_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Kong and Maravelias
        """
        return (
                model.p_down[i,b] <= model.M_a[i] * (1 - model.v[i,b])
        )
    
    @staticmethod
    def constraint_2o_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Kong and Maravelias
        """
        if i+1 <= max(model.I_set):
            return (
                    model.q_down[i+1,b+1] <= model.M_a[i] * (1 - model.v[i,b])
                    )
        else:
            return pe.Constraint.Skip
    
    @staticmethod
    def constraint_2p_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Kong and Maravelias
        """
        return (
                model.u[i,b] + model.v[i,b] == model.delta_L[i,b]
        )
    