import pyomo.environ as pe
from pyomo.opt import TerminationCondition
import logging
import pandas as pd

# TODO: adapt constraint rules commments to rebennack2020_rebennack_vs_kong.pdf
class rebennack_krasko_model:

    def __init__(self, quadratic=True):
        """This method initializes the model as an abstract pyomo model."""
        self.model = pe.AbstractModel()
        self.quadratic=quadratic
        self.setupSets()
        self.setupParameters()
        self.setupVariables()
        self.setupObjective()
        self.setupConstraints()


    def setupSets(self):
        """This method sets up all Sets required by all optimization models."""
        # TODO: think about adapting sets dynamically
        # set of breakpoints
        self.model.B_set = pe.Set(
            within=pe.NonNegativeIntegers)
        # set of affine segments
        self.model.B_1_set = pe.Set(
            within=pe.NonNegativeIntegers)
        # set of left segments
        self.model.B_2_set = pe.Set(
            within=pe.NonNegativeIntegers)
        # set of datapoints
        self.model.I_set = pe.Set(
            within=pe.NonNegativeIntegers)
        # set of left datapoints
        self.model.I_1_set = pe.Set(
            within=pe.NonNegativeIntegers)

    def setupParameters(self):
        """This method sets up all Parameters required by all optimization models."""
        # input data for input side
        self.model.X = pe.Param(
            self.model.I_set,
            within=pe.Reals)
        # input data for output side
        self.model.Y = pe.Param(
            self.model.I_set,
            within=pe.Reals)
        # upper bound for xi
        self.model.M_a = pe.Param(
            self.model.I_set,
            within=pe.Reals)
        #
        self.model.M = pe.Param(
            self.model.I_set,
            within=pe.Reals)
        # upper bound for d
        self.model.D_upper = pe.Param(
            self.model.B_1_set,
            within=pe.Reals)
        # lower bound for d
        self.model.D_lower = pe.Param(
            self.model.B_1_set,
            within=pe.Reals)
        # upper bound for c
        self.model.C_upper = pe.Param(
            self.model.B_1_set,
            within=pe.Reals)
        # lower bound for c
        self.model.C_lower = pe.Param(
            self.model.B_1_set,
            within=pe.Reals)

    def setupVariables(self):
        """This method sets up all Variables required by all optimization models."""
        ### continous variables
        # objective value
        self.model.z = pe.Var(
            within=pe.Reals)
        # slope of affine segment b
        self.model.c = pe.Var(
            self.model.B_1_set,
            within=pe.Reals,
            bounds=self.bounds_c_rule)
        # intercept of affine segment b
        self.model.d = pe.Var(
            self.model.B_1_set,
            within=pe.Reals,
            bounds=self.bounds_d_rule)
        # residual for data point i
        self.model.xi = pe.Var(
            self.model.I_set,
            within=pe.Reals,
            bounds=self.bounds_xi_rule)

        # decreasing slopes between data points i and i+1
        self.model.delta_up = pe.Var(
            self.model.I_1_set,
            self.model.B_2_set,
            within=pe.Reals,
            bounds=(0, 1))
        # increasing slopes between data points i and i+1
        self.model.delta_down = pe.Var(
            self.model.I_1_set,
            self.model.B_2_set,
            within=pe.Reals,
            bounds=(0, 1))

        ### binary variables
        # 1: data point i is associated with affine segment b, 0: else
        self.model.delta = pe.Var(
            self.model.I_set,
            self.model.B_1_set,
            within=pe.Binary)
        # 1: c[b] - c[b-1] >= 0 (decreasing slopes), 0: c[b] - c[b-1] <= 0 (increasing slopes)
        self.model.gamma = pe.Var(
            self.model.B_2_set,
            within=pe.Binary)

    def setupObjective(self):
        """This method declares the pe.Objective required by all optimization models."""
        self.model.Objective = pe.Objective(rule=self.objective_rule, sense=pe.minimize)

    def setupConstraints(self):
        """This method sets up all Constraints required by all optimization models."""
        # define objective rule as constraint in order to obtain a linear objective; optional use absolute error or squared error
        if self.quadratic:
            self.model.constraint_1a = pe.Constraint(
                rule=self.constraint_1a_rule)
        else:
            self.model.constraint_1a_ = pe.Constraint(
                rule=self.constraint_1a_linear_rule)
        # calculate xi[i] as abolute of the residual
        self.model.constraint_1b = pe.Constraint(
            self.model.I_set,
            self.model.B_1_set,
            rule=self.constraint_1b_rule)
        self.model.constraint_1c = pe.Constraint(
            self.model.I_set,
            self.model.B_1_set,
            rule=self.constraint_1c_rule)
        # each data point is associated with exactly one affine segment
        self.model.constraint_1d = pe.Constraint(
            self.model.I_set,
            rule=self.constraint_1d_rule)
        # enforce the ordering of the breakpoints
        self.model.constraint_1e = pe.Constraint(
            self.model.I_1_set,
            self.model.B_2_set,
            rule=self.constraint_1e_rule)
        self.model.constraint_1f = pe.Constraint(
            self.model.I_1_set,
            rule=self.constraint_1f_rule)
        self.model.constraint_1g = pe.Constraint(
            self.model.I_1_set,
            rule=self.constraint_1g_rule)
        # enforce continuity of affine segments
        self.model.constraint_1h = pe.Constraint(
            self.model.I_1_set,
            self.model.B_2_set,
            rule=self.constraint_1h_rule)
        self.model.constraint_1i = pe.Constraint(
            self.model.I_1_set,
            self.model.B_2_set,
            rule=self.constraint_1i_rule)
        self.model.constraint_1j = pe.Constraint(
            self.model.I_1_set,
            self.model.B_2_set,
            rule=self.constraint_1j_rule)
        self.model.constraint_1k = pe.Constraint(
            self.model.I_1_set,
            self.model.B_2_set,
            rule=self.constraint_1k_rule)
        self.model.constraint_1l = pe.Constraint(
            self.model.I_1_set,
            self.model.B_2_set,
            rule=self.constraint_1l_rule)
        self.model.constraint_1m = pe.Constraint(
            self.model.I_1_set,
            self.model.B_2_set,
            rule=self.constraint_1m_rule)

    def run(self, input_dict: dict = None, filepath: str = None, solver: str = 'gurobi', solver_options: dict = None,
            debug: bool = False, skip_instantiation: bool = False):
        """This method is used to start an optimization.

        To start an optimization it is necessary to provide a file with the necessary input data.
        This file includes all information required to instantiate the optimization problem.

        Args:
            input_dict (dict): Input dictionary that contains all the necessary data for the optimization
            filename (str): Filename of the previously generated input file
            solver (str): Name of the solver to be used. Standard is 'gurobi'
        """
        #debug = True
        
        self.mysolver = pe.SolverFactory(solver, options=solver_options)

        if not skip_instantiation:
            if input_dict:
                self.model_instance = self.model.create_instance(data=input_dict)
            elif filepath:
                self.model_instance = self.model.create_instance(filename=filepath)
            else:
                raise AttributeError(
                    "Please provide input data either as input dictionary for pyomo or as filepath to an input file.")

        if (solver == "gurobi_persistent"):
            self.mysolver.set_instance(self.model_instance, symbolic_solver_labels=True)
            self.results = self.mysolver.solve(tee=debug)
        else:
            self.results = self.mysolver.solve(self.model_instance, tee=debug)

        if self.results.solver.termination_condition == TerminationCondition.optimal:
            self.model_instance.solutions.load_from(self.results)

    # Evaluation methods
    @staticmethod
    def get_dataframe_from_result(model_instance_variable, unstack: list = None):
        """Returns a dataframe with the results from the optimization model."""

        # Create empty dictionary
        variable = {}
        # for all indices in the model_instance_variable put its key-value pair in the dictionary
        for index in model_instance_variable:
            variable[index] = model_instance_variable[index].value

        # Create pandas Series from
        variable = pd.Series(variable)
        if unstack:
            variable = variable.unstack(unstack)

        return variable

    @staticmethod
    def get_dataframe_from_parameter(model_instance_variable, unstack: list = None):
        """Returns a dataframe with the results from the optimization model."""

        # Create empty dictionary
        variable = {}
        # for all indices in the model_instance_variable put its key-value pair in the dictionary
        for index in model_instance_variable:
            variable[index] = model_instance_variable[index]

        # Create pandas Series from
        variable = pd.Series(variable)
        if unstack:
            variable = variable.unstack(unstack)

        return variable

    def get_objective(self):
        return self.model_instance.Objective()
    
    def get_solution_time(self):
        try: 
            return self.results.Solver._list[0]['Time']
        except: #solver == 'gurobi_persistent':
            return self.mysolver.get_model_attr('Runtime')
    
    @staticmethod
    def bounds_c_rule(model, b: int) -> "pyomo rule":
        """
        Bounds for variable c.
        """
        return (model.C_lower[b], model.C_upper[b])

    @staticmethod
    def bounds_d_rule(model, b: int) -> "pyomo rule":
        """
        Bounds for variable d.
        """
        return (model.D_lower[b], model.D_upper[b])

    @staticmethod
    def bounds_xi_rule(model, i: int) -> "pyomo rule":
        """
        Bounds for variable xi.
        """
        return (0, model.M_a[i])

    @staticmethod
    def objective_rule(model) -> "pyomo rule":
        """This method is used as an pe.Objective for the optimization, using weight factors for all impact categories.

        Args:
            model: The equivalent of "self" in pyomo optimization models
        """
        return model.z
    
    @staticmethod
    def constraint_1a_rule(model) -> "pyomo rule":
        """
        Provides the rule for the objective in order to achieve linearity in the objective.
        """
        return (
                model.z ==
                sum(model.xi[i]**2
                    for i in model.I_set)
                )
        
    @staticmethod
    def constraint_1a_linear_rule(model) -> "pyomo rule":
        """
        Provides the rule for the objective in order to achieve linearity in the objective.
        """
        return (
            model.z ==
            sum(model.xi[i]
                for i in model.I_set)
                ) 

    @staticmethod
    def constraint_1b_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Together with constraint 4c: define the value of xi[i] as the
        absolute difference of Y[i] and its associated segment's b function value
        p[X[i]] = c[b]*X[i]+d[b]
        """
        return (
                model.Y[i] - (model.c[b] * model.X[i] + model.d[b]) <=
                model.xi[i] + model.M_a[i] * (1 - model.delta[i, b])
        )

    @staticmethod
    def constraint_1c_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Together with constraint 4b: defines the value of xi[i] as the
        absolute difference of Y[i] and its associated segment's b function value
        p[X[i]] = c[b]*X[i]+d[b]
        """
        return (
                (model.c[b] * model.X[i] + model.d[b]) - model.Y[i] <=
                model.xi[i] + model.M_a[i] * (1 - model.delta[i, b])
        )
    
    @staticmethod
    def constraint_1d_rule(model, i: int) -> "pyomo rule":
        """Enforces that each data point is associated with exactly one affine segment."""
        return (
                sum(model.delta[i, b]
                    for b in model.B_1_set) == 1
        )
    
    @staticmethod
    def constraint_1e_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Together with constraints 1f and 1g: enforces the ordering of the
        breakpoints.
        Point i+1: to be associated with line segment b+1 the left adjacent point
        must be either be associated with the same segment or the previous segment b
        """
        return (
                model.delta[i + 1, b + 1] <=
                model.delta[i, b] + model.delta[i, b + 1]
        )

    @staticmethod
    def constraint_1f_rule(model, i: int) -> "pyomo rule":
        """
        Together with constraints 1e and 1g: enforces the ordering of the
        breakpoints.
        Point i+1: to be associated with line segment b=1 the left adjacent point
        must be associated with the segment 1, too.        """
        return (
                model.delta[i + 1, 1] <=
                model.delta[i, 1]
        )

    @staticmethod
    def constraint_1g_rule(model, i: int) -> "pyomo rule":
        """
        Together with constraints 1e and 1f: enforces the ordering of the
        breakpoints.
        Point i+1: to be associated with line segment b=B-1 the right adjacent point
        must be associated with segment b=B-1, too.
        """
        return (
                model.delta[i, max(model.B_set) - 1] <=
                model.delta[i + 1, max(model.B_set) - 1]
        )

    @staticmethod
    def constraint_1h_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Constraints 1h-m enforce continuity of the PWL segments.
        Together with 1i: if delta[i,b] == 1 and delta[i+1,b+1] == 1 (breakpoint between i and i+1)
        --> Either delta_up[i,b] or delta_down[i,b] is forced to one.
        """
        return (
                model.delta[i, b] + model.delta[i + 1, b + 1] + model.gamma[b] - 2 <=
                model.delta_up[i, b]
        )

    @staticmethod
    def constraint_1i_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Constraints 1h-m enforce continuity of the PWL segments.
        Together with 1h: if delta[i,b] == 1 and delta[i+1,b+1] == 1 (breakpoint between i and i+1)
        --> Either delta_up or delta_down is forced to one.
        """
        return (
                model.delta[i, b] + model.delta[i + 1, b + 1] + (1 - model.gamma[b]) - 2 <=
                model.delta_down[i, b]
        )

    @staticmethod
    def constraint_1j_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Constraints 1h-m enforce continuity of the PWL segments.
        1j-k: active if delta_up[i,b] == 1 -> decreasing slopes between segment b and b+1
        """
        return (
                model.d[b + 1] - model.d[b] >=
                model.X[i] * (model.c[b] - model.c[b + 1]) - model.M[i] * (1 - model.delta_up[i, b])
        )

    @staticmethod
    def constraint_1k_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Constraints 1h-m enforce continuity of the PWL segments.
        1j-k: active if delta_up[i,b] == 1 -> decreasing slopes between segment b and b+1
        """
        return (
                model.d[b + 1] - model.d[b] <=
                model.X[i + 1] * (model.c[b] - model.c[b + 1]) + model.M[i + 1] * (1 - model.delta_up[i, b])
        )

    @staticmethod
    def constraint_1l_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Constraints 1h-m enforce continuity of the PWL segments.
        1l-m: active if delta_down[i,b] == 1 -> increasing slopes between segment b and b+1
        """
        return (
                model.d[b + 1] - model.d[b] <=
                model.X[i] * (model.c[b] - model.c[b + 1]) + model.M[i] * (1 - model.delta_down[i, b])
        )

    @staticmethod
    def constraint_1m_rule(model, i: int, b: int) -> "pyomo rule":
        """
        Constraints 1h-m enforce continuity of the PWL segments.
        1l-m: active if delta_down[i,b] == 1 -> increasing slopes between segment b and b+1
        """
        return (
                model.d[b + 1] - model.d[b] >=
                model.X[i + 1] * (model.c[b] - model.c[b + 1]) - model.M[i + 1] * (1 - model.delta_down[i, b])
        )