class MIQP_model(object):
    """ Interface to the Pyomo model """
    def __init__(self, x: list, y: list, 
                 formulation = 'rebennack_krasko',
                 debug_optimization: bool = False,
                 solver: str = "gurobi",
                 solver_options: dict = {
                     "LogToConsole": 1,
                     "NumericFocus": 3,
                     "ScaleFlag": 0,
                     }
                 ):
        """ Constructor of the class """
        
        assert formulation in ['rebennack_krasko', 'kong_maravelias']
        assert len(x) == len(y)
        self.x = list(x)
        self.y = list(y)
        
        self.formulation = formulation
        self.debug_optimization = debug_optimization
        self.solver = solver
        if solver=='glpk':
            self.solver_options={}
        else:
            self.solver_options = solver_options
        
    def fit(self, z: int):
        """ Fit function of the model """
        assert z <= len(self.x)-1
        # import data processing
        try:
            from .data_processing import generate_input_dictionary
        except:
            from data_processing import generate_input_dictionary
        
        # import model
        if self.formulation == 'rebennack_krasko':
            try:
                from .rebennack_and_kasko import rebennack_krasko_model as model
            except:
                from rebennack_and_kasko import rebennack_krasko_model as model
        elif self.formulation == 'kong_maravelias':
            try:
                from .kong_and_maravelias import kong_maravelias_model as model
            except:
                from kong_and_maravelias import kong_maravelias_model as model
        
        # create abstract pyomo model
        if self.solver=='glpk':
            m = model(quadratic=False)
        else:
            m=model()
        # specify the number of sections
        self.z = z
        # generate input dictionary for optimization
        self.input_dictionary = generate_input_dictionary(self.x, self.y, self.z)
        # instantiate abstract pyomo model with data and solve resultingg concrete pyomo model 
        m.run(input_dict=self.input_dictionary, 
              solver=self.solver, 
              solver_options=self.solver_options, 
              debug=self.debug_optimization)
        
        # save results
        slopes     = m.model_instance.c
        slopes     = list(m.get_dataframe_from_result(slopes))
        intercepts = m.model_instance.d
        intercepts = list(m.get_dataframe_from_result(intercepts))
        self.slopes = slopes 
        self.intercepts = intercepts
        
        # get objective value of the solved concrete model
        self.model_error = m.get_objective()   
        self.sol_time = m.get_solution_time()
        
if __name__ == '__main__':
    ### TEST
    x = [0,1,2]
    y = [0,1,4]
    z = 2
    model = MIQP_model(x,y,
                       formulation = 'kong_maravelias')
    model.fit(z)
    
    print('model_error: ' + str(model.model_error))
    
    import numpy as np
    import matplotlib.pyplot as plt
    plt.scatter(x,y)
    plt.xlim((min(x), max(x)))
    for slope,intercept in zip(model.slopes,model.intercepts):
        plot_x = np.array([min(x), max(x)])
        plot_y = plot_x * slope + intercept
        plt.plot(plot_x, plot_y)
   