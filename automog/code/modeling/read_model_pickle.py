import os
import pickle

if __name__ == "__main__":
    data_path = '../../data/'
    model_path = 'Goderbauer2016 + case4_GUD/optimization/models/example_model.pkl'
    
    with open(data_path + model_path, 'rb') as f:
        model = pickle.load(f)
    