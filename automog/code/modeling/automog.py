import numpy as np
import os
import pandas as pd
from datetime import datetime
import logging
import copy

# modeling
from modeling import linearizer
# optimization
from optimization.interface import optimizer, model, instance

logger = logging.getLogger('AutoMoG_functions.automog')
logger.setLevel(logging.DEBUG)

class AutoMoG(object):
    def __init__(self, mainframe, operating_points, wdir):
        # =====================================================================
        # initialize main attributes
        self.mainframe         = mainframe
        self.operating_points  = operating_points
        self.working_directory = wdir
        
        # =====================================================================
        # create components
        # create MOG_Component object for each component
        # reset so far stored components (reset static class attributes)
        MOG_Component.reset()
        
        # get in- and output names of component
        for comp in sorted(self.operating_points):
            inputs  = []
            params  = []
            outputs = []
            
            # get any operating state
            state = list(self.operating_points[comp].keys())[0]
            for c in self.operating_points[comp][state]:
                ### first reading of file
                if '(in)' in c or '(param)' in c:
                    inputs.append(c)
                    if '(param)' in c:
                        params.append(c)
                elif '(out)' in c:
                    outputs.append(c)
                ### later reading (after pressing 'Select -> Components')
                else:
                    if c in MOG_Component.__all__[comp].inputs:
                        inputs.append(c)
                        if c in MOG_Component.__all__[comp].params:    
                            params.append(c)
                    elif c in MOG_Component.__all__[comp].outputs:
                        outputs.append(c)
                    else:
                        logger.info('Can not specify whether \'{}\' is input, param or output of \'{}\''.format(c, comp))
                
            
            # create MOG_Component, object will be appended to static list MOG_Component.components
            MOG_Component(name = comp,
                          inputs = inputs, 
                          params = params,
                          outputs = outputs,
                          operating_points = self.operating_points[comp])
            
            if hasattr(self.mainframe,'settings'): 
                for col in self.mainframe.settings:
                    eval('MOG_Component.components[MOG_Component.names[-1]].setter({} = self.mainframe.settings.loc[comp, \'{}\'])'.format(col,col))
            else:
                MOG_Component.components[MOG_Component.names[-1]].setter(sections =  1, select = 1)

        MOG_Component.names = sorted(MOG_Component.names)
        self.components = copy.deepcopy(MOG_Component.components)
        
        # =====================================================================
        # initialize mode availability (boolean) and read available data
        self.mode_availability = {'WEIGHTED ERROR': 1, 
                                  'OBJECTIVE FUNCTION': 1}
        self.read() 
        
        # =====================================================================
        # final setup
        # setup options
        self.options = {'icrit'     : None,
                        'lin_method': None,
                        'mode'      : None,
                        'acc'       : None}
    
        self.set_storages()
        self.setup()
    
    def read(self):
        # =====================================================================
        # OBJECTIVE FUNCTION MODE
        # read params_opt
        params_opt_file = self.working_directory + '/timeseries.xlsx'
        
        if os.path.isfile(params_opt_file):
            params_opt_xlsx = pd.ExcelFile(params_opt_file)
            params_opt = []
            
            for sheet_name in params_opt_xlsx.sheet_names :
                params = pd.read_excel(params_opt_xlsx, sheet_name)
                params = params.rename(columns={col_old: sheet_name + '_' + col_old 
                                                for col_old in params})
                params_opt.append(params) 
            
            self.params_opt = pd.concat(params_opt, axis=1)
            
        else:
            print('File \'timeseries.xlsx\' is not available. Therfore, OBJECTIVE FUNCTION Mode is not available.')
            self.mode_availability['OBJECTIVE FUNCTION'] = 0
            self.params_opt = None
            
        # =====================================================================
        # WEIGHTED ERROR MODE
        # read WEIGHTED ERRORs
        self.weighting_factors = {}

        error_message = None
        
        reader      = {'.xlsx': pd.read_excel, 
                       '.csv':  pd.read_csv}
        availability = [os.path.isfile(self.working_directory + f'/weighting_factors{read_format}') 
                        for read_format in sorted(reader)]

        if any(availability):
            read_format = sorted(reader)[availability.index(1)]
            read_fcn = reader[read_format]
            weighting_factors = read_fcn(self.working_directory + f'/weighting_factors{read_format}')
            
            for comp in self.components.values():
                try:
                    comp.weighting_factors[comp.inputs[0]] = weighting_factors[comp.inputs[0].split(' ')[0]].values[0]
                    self.weighting_factors[comp.name,comp.inputs[0]] = weighting_factors[comp.inputs[0].split(' ')[0]].values[0]
                except:
                    error_message = f'Error while reading \'weighting_factors.{read_format}\'.'
        else: 
            error_message = '\'weighting_factors\' file is not available. Either .csv or .xlsx file is needed.'
                
        if error_message != None:
            print(error_message + '\nReading \'weighting_factors\' failed. WEIGHTED ERROR Mode is not available.')
            self.mode_availability['WEIGHTED ERROR'] = 0
        
    def reset(self, hard = 0, current_section=0):
        if not current_section:
            delattr(self,'curves')
        else:
            for comp in self.components:
                for state in self.curves[comp]:
                    if self.sections[comp][state] in self.curves[comp][state]:
                        del self.curves[comp][state][self.sections[comp][state]]
        
        if not current_section:
            self.set_storages(hard=hard)
            self.setup()
        
    def setup(self):
        if not hasattr(self,'curves'):
            self.curves = {comp_name : {state : {} 
                                        for state in comp.operating_points} 
                           for (comp_name,comp) in self.components.items()}
       
    def update(self, **kwargs):
        for k in kwargs: 
            assert k in ['icrit', 'lin_method', 'mode', 'acc'], KeyError('AutoMoG has no option \'{}\'.')
            self.options[k] = kwargs[k]
        # initialize linearizer
        self.linearizer = linearizer(self)
    
    def set_storages(self, hard=0):
        # =====================================================================
        self.store = {'target'          : None,
                      'ref_target'      : None,
                      'delta_target'    : None,
                      'delta_ref_target': None,
                      'solution_time'   : None}
        
        # =====================================================================
        # initialize sections and frames 
        self.sections = {}
        no_states = 0
        columns = []
        for comp in sorted(self.components):
            self.sections[comp] = {}
            for state in self.components[comp].operating_points:
                if hasattr(self.components[comp], 'sections') and not hard:
                    self.sections[comp][state] = self.components[comp].sections[state]
                else:
                    self.sections[comp][state] = 1  
                no_states += 1
                columns.append(comp+'_'+str(state))
        
        
        for frame in self.store:
            if not 'ref' in frame:
                # init size of storages for one iteration    
                self.store[frame] = pd.DataFrame(data = np.zeros((1, no_states)),
                                                 columns = sorted(columns))
                self.store[frame].index.name = 'iteration'
            else:
                self.store[frame] = np.zeros((1, 1))
        
        
        # =====================================================================
        # initialize main automog variables
        self.iter = -1
        
        self.component_finished = {}
        for comp in self.components.keys():
            for state in self.components[comp].operating_points:
                self.component_finished[comp+'_'+str(state)] =  0 
    
    def extend_storages(self):
        """
        Adds rows and columns to storages in order to be flexible regarding the
        number of iterations.

        """
        for frame in self.store:
            if not 'ref' in frame:
                self.store[frame] = self.store[frame].append(
                    pd.DataFrame(data=np.zeros((1, len(self.store[frame].columns))), 
                                 columns = self.store[frame].columns),
                    ignore_index=True)
            elif 'ref' in frame:
                self.store[frame] = np.append(self.store[frame],[[0]],axis=1)
        
    def converge(self):
        """ Converges the automog method until the information criterium indicates
        overfitting of models.  """
        self.reset(hard=1)
        while not self.abort():
            self.next_iter()
            
    def next_iter(self):
        """ Performs the next iteration of the automog process."""
        
        self.iteration_completed = False
        
        # initialize iteration
        self.output("Iteration {}: {}/{} components finished.\n".format(self.iter+1,
                                                                        sum(list(self.component_finished.values())),
                                                                        len(self.component_finished))
                    )
        # first iteration
        if self.iter == -1:
            # hard reset automog --> all sections to 1
            self.reset(hard=1)
            # linearize all components
            self.output('Linearize all components.')
            self.linearizer.linearize_all()
            self.store['ref_target'][0][0] = self.get_target()
            
        # further iterations
        else:
            self.extend_storages()
            for comp in sorted(self.components):
                for state in  self.components[comp].operating_points:
                    comp_state = comp + '_' + str(state)
                    # if component not finished yet
                    if self.component_finished[comp_state] == 0:
                        # model component i with one linear section more 
                        self.sections[comp][state] += 1
                        self.output("Linearize Component : {}_{} - {} sections.".format(comp, state,
                                                                                        self.sections[comp][state]))
                        self.linearizer.linearize_all()
                    else:
                        continue
                        
                    # if no overfitting
                    if not self.overfit(comp,state):
                        self.store['target'][comp_state][self.iter]       = self.get_target(comp_state = comp_state)
                        self.store['delta_target'][comp_state][self.iter] = abs(self.store['target'][comp_state][self.iter]-
                                                                                self.store['ref_target'][0][self.iter])
                        
                        self.output("DELTA {}: {} - {}".format(self.options['mode'],
                                                               comp_state,
                                                               round(self.store['delta_target'][comp_state][self.iter],1)))
                        self.sections[comp][state] -= 1 
                        self.mainframe.iteration_frame.update()
                       
            # finish iteration
            # check for overfitting
            if not self.abort(output=0):
                max_delta_component = self.get_maxdelta_component(self.store['delta_target'], self.iter)
                self.store['ref_target'][0][self.iter+1] = self.store['target'][max_delta_component][self.iter]
                self.store['delta_ref_target'][0][self.iter] = self.store['ref_target'][0][self.iter+1] - self.store['ref_target'][0][self.iter]
                self.output('\nNEW REFERENCE VALUE: {}'.format(round(self.store['ref_target'][0][self.iter+1],2)))
                comp_max  = '_'.join(max_delta_component.split('_')[:-1])
                state_max = int(max_delta_component.split('_')[-1])
                self.sections[comp_max][state_max] += 1
                self.output("REFINED {}.".format(max_delta_component))
            else:
                self.output("NO REFINEMENT.")
                
        self.output("ITERATION FINISHED.")
        self.output('-------------------------------------------------------\n')
        self.iter += 1
        self.iteration_completed = True
        self.mainframe.iteration_frame.update()
            
    def get_target(self, comp_state = None):
        if self.options['mode'] == 'OBJECTIVE FUNCTION':
            # create energy system in dependency of part load behavior linearization
            m = model(self.components, self.curves, self.sections)
            i = instance(self.params_opt)
            results = optimizer(m=m, inst=i, 
                                solver=self.mainframe.stringVars['solver'].get().lower()).optimize()
            
            # calculate operational values
            operational_component = results['obj']
            
            if comp_state != None:
                self.store['solution_time'][comp_state][self.iter] = results['time']
            return operational_component
        
        elif self.options['mode'] == 'WEIGHTED ERROR':
            total_model_error=0 
            for comp in self.components.values():
                for state in comp.operating_points:
                    curves = self.curves[comp.name][state]
                    sect   = self.sections[comp.name][state]
                    total_model_error += np.sqrt(curves[sect]['mse'])*comp.weighting_factors[comp.inputs[0]]
            
            return total_model_error
            
    def overfit(self, comp, state):
        """
        Checks for overfitting models.

        Parameters
        ----------
        comp : str
            component to check.
        state : int
            operating state to check.
        
        Returns
        -------
        overfit : boolean
            True, if model for given component and operating state is overfitted.

        """
        comp_state = comp+'_'+str(state)
        if self.component_finished[comp_state] == 1:
            overfit = 1
        else:
            curves = self.curves[comp][state]
            sect = self.sections[comp][state]
            overfit = curves[sect]['ic'] > curves[sect-1]['ic']
        
        if overfit:
            self.store['solution_time'][comp_state][self.iter+1]=0
            self.store['target'][comp_state][self.iter]=self.store['ref_target'][0][self.iter]
            self.store['delta_target'][comp_state][self.iter]=0
            
            if self.component_finished[comp_state] == 0:
                self.sections[comp][state] -= 1
                self.component_finished[comp_state] = 1
                self.output("Component finished : {}.".format(comp_state))
        return overfit
    
    def abort(self, output: bool = 1):
        if self.iter == -1:
            return 0
        else:
            all_overfitted = all(list(self.component_finished.values()))
            
            accuracy = self.get_accuracy()
            accuracy_reached = 100*accuracy <= self.options['acc']
            abort_criterion = all_overfitted or accuracy_reached
            
            if output:
                self.output('Check accuracy: {} % (Target: {} %).\n'.format(round(100*accuracy,3),
                                                                        self.options['acc']))
            if abort_criterion and output:
                if all_overfitted:
                    self.output("Overfitting reached - no further refinement. AutoMoG terminated.")
                elif accuracy_reached:
                    self.output("Accuracy reached - no further refinement. AutoMoG terminated.")
                
                self.mainframe.iteration_frame.update()
                self.write()
            return abort_criterion
    
    def get_accuracy(self):
        if self.options['mode'] == 'OBJECTIVE FUNCTION':
            if not (self.iter == 0 and self.iteration_completed):
                if self.iteration_completed:
                    it = self.iter - 1
                else:
                    it = self.iter
                    
                current_delta   = self.store['delta_target'].max(axis=1)[it]
                reference_value = self.store['ref_target'][0][it]
                accuracy = current_delta / reference_value
            else:
                accuracy = float("inf")
            
        elif self.options['mode'] == 'WEIGHTED ERROR':
            weight_error_model = sum(sum(comp.weighting_factors[comp.inputs[0]]*\
                                         np.sqrt(self.curves[comp.name][state][self.sections[comp.name][state]]['mse'])
                                         for state in comp.operating_points)
                                     for comp in self.components.values())
            
            weights_data = sum(sum(sum(comp.weighting_factors[comp.inputs[0]]*data_point[comp.inputs[0]]
                                       for _,data_point in comp.operating_points[state].iterrows())
                                   for state in comp.operating_points)
                               for comp in self.components.values())
            
            accuracy = weight_error_model / weights_data  
        
        return accuracy
    
    def output(self,txt):
        logging.info(txt)
        self.mainframe.iteration_frame.update_txt(txt)
        
    def write(self):
        # setup output directory
        time = datetime.now().strftime("%Y_%m_%d_%H-%M-%S")
        self.output_dir = self.mainframe.output_dir + '/AutoMoG/' + time
        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)
            
        # Write setting-file
        with open(self.output_dir + '/settings.txt', 'w') as f:
            for k,v in self.options.items():
                f.write(str(k) + ': ' + str(v) + '\n\n')
                
        # delete zero-entries and save frames
        for frame_name in self.store:
            frame = self.store[frame_name]
            if not isinstance(frame, pd.DataFrame):
                frame = pd.DataFrame(frame)
                frame = frame.transpose()
            non_zero_frame = frame.loc[(frame !=0).any(axis=1), :]
            non_zero_frame.index = non_zero_frame.index.rename('iteration')
            self.store[frame_name] = non_zero_frame
            non_zero_frame.to_csv(self.output_dir + '/{}.csv'.format(frame_name))
        
        # Write linearization stats
        for cat in ['sol_time', 'model_error']:
            write_curves=copy.deepcopy(self.curves)
            for comp in self.curves:
                for state in self.curves[comp]:
                    for nsect in self.curves[comp][state]:
                        write_curves[comp][state][nsect] = self.curves[comp][state][nsect][cat]
                  
            frame_dict = {}
            for comp in write_curves:
                frame_dict[comp] = {}
                for state in write_curves[comp]:   
                    for nsect in write_curves[comp][state]:
                        frame_dict[comp][state,nsect] = write_curves[comp][state][nsect]

            write_curves_frame = pd.DataFrame.from_dict(frame_dict)            
            
            write_curves_frame.to_excel(self.output_dir + '/pwlf_{}.xlsx'.format(cat),
                                        index_label=('state','sections'))
        
        # read supplementary input if available
        self.mainframe.read_supplementary_input()
        
        # Write model  
        m = model(self.components, self.curves, self.sections, storages = self.mainframe.storages, 
                  name = os.path.basename(self.mainframe.working_directory))
        
        self.model_dir = self.mainframe.working_directory + '/optimization/models'
        if not os.path.exists(self.model_dir):
            os.makedirs(self.model_dir)
        for dir_ in [self.model_dir, self.output_dir]:
            m.to_file(dir_ + '/automog_{}.pkl'.format(time))
    
    @staticmethod
    def write_model(handler, results):
        handler.m.solutions.load_from(results)
        with open("model.txt", "w") as f:
            import sys
            sys.stdout = f
            handler.m.pprint()
            
    @staticmethod
    def get_maxdelta_component(df, row=None):
        """ Returns component with maximum value in row. """
        if isinstance(df, pd.Series):
            if row != None:
                raise ValueError('Parameter \'row\' can not be applied for pd.Series.')
            component = df.idxmax()
            
        elif isinstance(df, pd.DataFrame):
            component = df.loc[row].idxmax()
        return component
        
class MOG_Component(object):
    # list of all components
    __all__ = {}
    # list of all names
    __names__ = []
    # list of selected components
    components = {}
    # list of names
    names = []
    
    def __init__(self, name: str, inputs: list, outputs: list, 
                 operating_points: dict, params: list = [], 
                 opt_params: dict = {}, units = None):
        """
        Initializes object of MOG_Component class.

        Parameters
        ----------
        name : str
            name of component.
        inputs : list
            input products of component.
        outputs : list
            output products of component.
        operating_points : pd.Series
            operating points of component.

        """
        
        # initialize attributes
        self.name    = name
        self.units   = units
        self.inputs  = inputs
        self.params  = params
        self.outputs = outputs
        self.operating_points  = operating_points
        self.weighting_factors = {}
        self.opt_params        = opt_params
        
        # update static attributes
        MOG_Component.components[self.name] = self
        MOG_Component.names.append(self.name)
        
        if not self.name in MOG_Component.__all__:
            MOG_Component.__all__[self.name] = self
            MOG_Component.__names__.append(self.name)
    
    @classmethod
    def from_dict(cls, init_dict: dict):
        return cls(**init_dict)
    
    def to_dict(self): 
        export_attrs = ['name', 'inputs', 'params', 'outputs', 
                        'operating_points','opt_params', 'units']
        return {attr: getattr(self, attr) 
                for attr in export_attrs}
    
    @property
    def inputs(self):
       return self._inputs
   
    @inputs.setter
    def inputs(self, inputs: list): 
        get_units = True if len(self.units['inputs']) == 0 else False
        for idx, product in enumerate(inputs):
            inputs[idx] = product.split(' ')[0]
            if get_units:
                try:
                    self.units['inputs'][product.split(' ')[0]] = product.split(' ')[2]
                except IndexError:
                    self.units['inputs'][product.split(' ')[0]] = '[-]'

        self._inputs = inputs
        
    @property
    def params(self):
        return self._params
    
    @params.setter
    def params(self, params: list):
        get_units = True if len(self.units['params']) == 0 else False
        for idx, param in enumerate(params):
            params[idx] = param.split(' ')[0]
            if get_units:
                try:
                    self.units['params'][param.split(' ')[0]] = param.split(' ')[2]
                except IndexError:
                    self.units['params'][param.split(' ')[0]] = '[-]'

        self._params = params
        
    @property
    def outputs(self):
        return self._outputs
        
    @outputs.setter
    def outputs(self, outputs: list):
        get_units = True if len(self.units['outputs']) == 0 else False
        for idx, product in enumerate(outputs):
            outputs[idx] = product.split(' ')[0]
            if get_units:
                try:
                    self.units['outputs'][product.split(' ')[0]] = product.split(' ')[2]
                except IndexError:
                    self.units['outputs'][product.split(' ')[0]] = '[-]'
                
        self._outputs = outputs
        
    @property
    def operating_points(self):
        return self._operating_points
        
    @operating_points.setter
    def operating_points(self, operating_points: dict):
        # downwards compatible for models without different operating states
        if type(operating_points) != dict:
            operating_points = {0: operating_points}

        for state in operating_points:
            operating_points[state].columns = [col.split(' ')[0] 
                                               for col in operating_points[state].columns]
        self._operating_points = operating_points
        
    @property
    def sections(self):
        return self._sections
    
    @sections.setter
    def sections(self, sections: dict):
        if type(sections) == dict:
            self._sections = sections
        else:
            try:
                sections = int(sections)
                sections = {state: sections 
                            for state in self.operating_points}
            except:
                raise TypeError('sections parameter has uncompatible type ({})'.format(type(sections)))
        
            self.sections = sections
    
    @property 
    def units(self):
        return self._units
    
    @units.setter
    def units(self, units: dict):
        if units == None:
            self._units  = {'inputs':  {},
                            'params':  {},
                            'outputs': {}}
        else:
            self._units = units
            
    @classmethod
    def reset(cls):
        """ Resets static attributes. """
        cls.components = {}
        cls.names = []
        
    def setter(self, **kwargs):
        """ sets further attributes. """
        for attr in kwargs:
            if not attr == 'sections':
                setattr(self, attr, kwargs[attr])
            else:
                self.sections = kwargs[attr]
