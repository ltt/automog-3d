# -*- coding: utf-8 -*-
"""
Created on Fri Mar  5 13:29:35 2021

@author: niels
"""
from graphviz import Digraph
import copy

class FlowChart(object):
    """This class creates a Flow Chart for a given model"""
    def __init__(self,model,directory):
        #get all components, inputs and outputs and init stuff
        self.components=copy.deepcopy(model.components)
        self.products=copy.deepcopy(model.products)
        self.storages=copy.deepcopy(model.storages)
        self.params=copy.deepcopy(model.input_params)
        self.inputs=list()
        self.outputs=list()
        self.edge_count_d=dict()
        self.edge_count_c=dict()
        self.edge_count_s=dict()
        self.edge_count_f=dict()
        self.edge_count_p=dict()
        subcomponents=list()
        self.directory=directory
        self.colors=['green','black','orange','purple','cyan']
        self.edge_colors=dict()
       
        #delete subcomponents and add their in and outputs to actual component 
        for comp in self.components:
            if "_" in comp:
                if comp.split("_")[0] in self.components:
                    for Input in self.components[comp].inputs:
                        if Input not in self.components[comp.split("_")[0]].inputs:
                            self.components[comp.split("_")[0]].inputs.append(Input)
                    for output in self.components[comp].outputs:
                        if output not in self.components[comp.split("_")[0]].outputs:
                            self.components[comp.split("_")[0]].outputs.append(output)
                    subcomponents.append(comp)
                
        for subcomp in subcomponents:
            del(self.components[subcomp])
        for comp in self.components:
            for Input in self.components[comp].inputs:
                self.inputs.append(Input)
            for output in self.components[comp].outputs:
                self.outputs.append(output)
        
        #predefined colors for certain products;
        self.edges=list(set(self.inputs+self.outputs))
    
        for edge in self.edges:
            if edge=='Heat':
                self.edge_colors[edge]='red'
            elif edge=='Gas':
                self.edge_colors[edge]='grey'
            elif edge=='Power':
                self.edge_colors[edge]='yellow'
            elif edge=='Chill':
                self.edge_colors[edge]='blue'
            else:
                self.edge_colors[edge]=self.colors[0]
                del self.colors[0]
                
                
    def create_FlowChart(self, include_legend=False):
        
        chart=Digraph('FlowChart',format='png',directory=self.directory)
        
        #create all necessary nodes;subgraphs for those in same height; points as work around for joined edges
        with chart.subgraph(name='supply') as sup_nodes:
            sup_nodes.attr(rank='same')
            sup_nodes.attr('node',shape='circle', width='1')
            for Input in self.inputs:
                if Input in self.products.keys() and self.products[Input]['price'] is not None:
                    sup_nodes.attr('node', color=self.edge_colors[Input])
                    sup_nodes.node(Input+"\nSupply")
                    self.edge_count_s[Input]=0
                elif Input in self.params:
                    sup_nodes.attr('node', color=self.edge_colors[Input])
                    sup_nodes.node(Input)
                    self.edge_count_s[Input] = 0
                else:
                    self.edge_count_s[Input]=1
        
        with chart.subgraph(name='demand') as dem_nodes:
            dem_nodes.attr(rank='same')
            dem_nodes.attr('node', shape='doublecircle', width='1')
            for output in self.outputs:
                if self.products[output]['demand'] is not None:
                    dem_nodes.attr('node', color=self.edge_colors[output])
                    dem_nodes.node(output+"\nDemand")
                    self.edge_count_d[output]=0

        with chart.subgraph(name='Feed Option') as feed_nodes:
            feed_nodes.attr(rank='same')
            feed_nodes.attr('node', shape='doublecircle', width='1')
            for output in self.outputs:
                if self.products[output]['compensation'] is not None:
                    feed_nodes.attr('node', color=self.edge_colors[output])
                    feed_nodes.node(output+"\nFeed Option")
                    self.edge_count_f[output]=0

        for product in self.products:
            chart.attr('node',shape='point', width='0.01',height='0.01',color=self.edge_colors[product])
            chart.node(product+"point")
        for param in self.params:
            #chart.node(param)
            chart.attr('node', shape='point', width='0.01', height='0.01', color=self.edge_colors[param])
            chart.node(param + "point")
        chart.attr("node",shape='box',color='black')
        for comp in self.components:
            chart.node(comp)
            self.edge_count_c[comp]=0
        for stor in self.storages:
            chart.node(stor)

            
        #create all edges
        for comp in self.components:
            
            for Input in self.components[comp].inputs:
                if self.edge_count_s[Input]==0 and self.edge_count_c[comp]==0:
                    chart.edge(Input+"\nSupply",Input+'point', color=self.edge_colors[Input], dir='none')
                    chart.edge(Input+'point',comp, color=self.edge_colors[Input])
                    self.edge_count_s[Input]+=1
                    self.edge_count_c[comp]+=1
                elif self.edge_count_c[comp]==0:
                    chart.edge(Input+'point',comp,color=self.edge_colors[Input])
                    self.edge_count_c[comp]+=1
                elif Input in self.params:
                    chart.edge(Input, Input+'point', color=self.edge_colors[Input], dir='none')
                    chart.edge(Input+'point', comp, color=self.edge_colors[Input])
                    self.edge_count_s[Input] += 1
                elif self.edge_count_s[Input]==0:
                    chart.edge(Input + "\nSupply", Input + 'point', color=self.edge_colors[Input], dir='none')
                    chart.edge(Input + 'point', comp, color=self.edge_colors[Input])
                    self.edge_count_s[Input]+=1
            
            for output in self.components[comp].outputs:
                if self.products[output]['demand'] is not None and self.products[output]['compensation'] is not None:
                    chart.edge(comp,output+'point',dir='none', color=self.edge_colors[output])
                    if self.edge_count_d[output]==0:
                        chart.edge(output+'point',output+"\nDemand", color=self.edge_colors[output])
                        self.edge_count_d[output]+=1
                    if self.edge_count_f[output]==0:
                        chart.edge(output+'point',output+"\nFeed Option", color=self.edge_colors[output])
                        self.edge_count_f[output]+=1
                elif self.products[output]['demand'] is not None:
                    chart.edge(comp,output+'point',dir='none', color=self.edge_colors[output])
                    if self.edge_count_d[output]==0:
                        chart.edge(output+'point',output+"\nDemand", color=self.edge_colors[output])
                        self.edge_count_d[output]+=1
                elif self.products[output]['compensation'] is not None:
                    chart.edge(comp,output+'point',dir='none', color=self.edge_colors[output])
                    if self.edge_count_f[output]==0:
                        chart.edge(output+'point',output+"\nFeed Option", color=self.edge_colors[output])
                        self.edge_count_f[output]+=1
                elif output in self.inputs:
                    chart.edge(comp,output+'point',dir='none', color=self.edge_colors[output])
                else:
                    chart.edge(comp, comp, color=self.edge_colors[output])

        for stor in self.storages:
            chart.edge(stor, self.storages[stor]['storage_product']+'point', color=self.edge_colors[self.storages[stor]['storage_product']])
            chart.edge(self.storages[stor]['storage_product']+'point', stor, color=self.edge_colors[self.storages[stor]['storage_product']])
        
        #legend 
        if include_legend==True:
            legend_count=0
            letters=['a','b','c','d','e','f','g','h','i','j','k']
            with chart.subgraph(name='cluster_legend') as legend:
                legend.attr(label='Legend')
                legend.attr('node',shape='point', style='invis')
                for i,edge in enumerate(self.edges):
                    if legend_count==0:
                        with legend.subgraph() as f:
                            f.attr(rank='same')
                            f.edge(str(legend_count),str(legend_count+1), label=edge, color=self.edge_colors[edge], rank='same')
                    else:
                        with legend.subgraph() as letters[i]:
                            letters[i].attr(rank='same')
                            letters[i].edge(str(legend_count),str(legend_count+1), label=edge, color=self.edge_colors[edge], rank='same')
                        legend.edge(str(legend_count),str(legend_count-2), style='invis')
                        legend.edge(str(legend_count+1),str(legend_count-1), style='invis')
                    legend_count+=2
        
        chart.render('FlowChart',format='png',directory=self.directory)

    #Not yet working method
    def get_node_max(self,digraph):
        import re
        print(str(digraph))
        heights = [height.split('=')[1] for height in re.findall('height=[0-9.]+', str(digraph))]
        widths = [width.split('=')[1] for width in re.findall('width=[0-9.]+', str(digraph))]
        heights.sort(key=float,reverse=True)
        widths.sort(key=float, reverse=True)  
        print(heights)
        print(widths)
        return heights[0], widths[0]