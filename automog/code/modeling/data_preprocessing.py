import pandas as pd 
from sklearn_extra.cluster import KMedoids
from sklearn.svm import OneClassSVM
from sklearn.linear_model import LinearRegression
from sklearn.mixture import GaussianMixture
from sklearn.cluster import KMeans, AgglomerativeClustering
import numpy as np
import copy
import time

def clean_operating_points(operating_points):
    numeric_row_list = []

    for row_idx, row in operating_points.iterrows():
        try:
            converted_row = row.astype(float)
            numeric_row_list.append(converted_row)
        except:
            pass
    
    numeric_operating_points = pd.concat(numeric_row_list, axis= 1).transpose()
    
    return numeric_operating_points 

def outlier_detection(operating_points):
    X = operating_points.values
    clf = OneClassSVM(kernel = 'linear').fit_predict(X)
    X = X[clf == 1]
    filtered_operating_points =  pd.DataFrame(columns=operating_points.columns, 
                                              data = X)
    return filtered_operating_points

def reduce_operating_points(operating_points, 
                            n_clusters, 
                            filter_outliers: bool = False):
    """ Performs outlier detection (optional) followed by kmedoids clustering. """

    
    if filter_outliers:
        operating_points = outlier_detection(operating_points)
    
    X = operating_points.values

    if len(X) > n_clusters:
        kmedoids = KMedoids(n_clusters=n_clusters,
                        init = 'k-medoids++').fit(X)
        centers = kmedoids.cluster_centers_
        
        clustered_operating_points =  pd.DataFrame(columns=operating_points.columns, 
                                                   data = centers)
    else:
        clustered_operating_points = operating_points
        
    return clustered_operating_points

