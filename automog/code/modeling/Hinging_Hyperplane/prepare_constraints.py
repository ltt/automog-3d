from scipy.spatial import ConvexHull

from modeling.Hinging_Hyperplane.hinging_hyperplane import HHmodel, calculate_corner_points

def calc_convex_hull(model: HHmodel):
    for lin_element in model.milp.points:
        conv_hull = ConvexHull(model.milp.points[lin_element]['X'])
        corner_points = calculate_corner_points(conv_hull)
        model.milp.hull_lin_elements[lin_element] = ConvexHull(corner_points)
        