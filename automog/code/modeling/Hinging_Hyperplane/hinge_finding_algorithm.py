import numpy as np
from copy import deepcopy
from modeling.Hinging_Hyperplane import hinging_hyperplane as hh


def hingeFindingAlgorithm(data):
    """
    The hinge finding algorithm divides the data set so that the minimum errors with a linear regression over the
    resulting sub data sets is created.

    Principle of the Hinge Finding Algorithm based on the paper "On the Hinge-Finding Algorithm for Hinging Hyperplanes"
    by P. Pucar and J. Sjöberg. The damped Newton algorithm is used to check the convergence of algorithm.

    Parameters
    ----------
    data:       data set, must contain data['X'] and data['Y']

    Returns
    -------
    output:     contains the data that was divided into data points on the positive side of the hinge and on the
                negative side of the hinge, the linear parameters of the resulting linear elements, the sign,
                in_dataset to denote whether the hinge divides the data set so that both sub data sets contain data
                (if no hinge was found: in_datase = False, no other parameters are returned)

    """
    if len(data['Y']) < 2*len(data['X'][0, :]):
        raise Exception("Error in hinge_finding_algorithm. Data set is to small.")

    # determine first hinge
    data_plus, data_minus, delta = arbitraryHinge(data)

    # setup for outer while loop
    break_var = 0  # variable to avoid too many iterations
    flag = False  # interrupt outer loop if no suitable Newton step found
    non_conv = True  # variable that indicates whether the algorithm is converged
    while non_conv:  # outer loop for iterative determination of a new hinge
        # Setup for the inner loop. in_dataset shows whether the current hinge in this iteration divides the data set so
        # that data exists in both sub data sets
        in_dataset = False
        delta_old = deepcopy(delta)  # save the hinge from the last iteration for the newton step
        break_var += 1  # count iteration
        # print(break_var)

        # delete column with ones
        if all(data_plus['X'][:, 0] == 1) and len(data_plus['X'][0, :]) == len(delta):
            data_plus['X'] = data_plus['X'][:, range(1, len(data_plus['X'][0, :]))]
        if all(data_minus['X'][:, 0] == 1) and len(data_minus['X'][0, :]) == len(delta):
            data_minus['X'] = data_minus['X'][:, range(1, len(data_minus['X'][0, :]))]

        #  linear regression over data in S+
        hhModel_plus = hh.HHmodel(data_plus, normalize=False)
        hhModel_plus.hingingHyperplaneModel(numOfHinges=0)

        #  linear regression over data in S-
        hhModel_minus = hh.HHmodel(data_minus, normalize=False)
        hhModel_minus.hingingHyperplaneModel(numOfHinges=0)

        delta_newton = hhModel_plus.theta - hhModel_minus.theta  # calculate the hing for a full Newton step

        count = 0  # variable to count the iterations in the inner loop

        while not in_dataset:
            count += 1
            mu = 1 / count  # parameters for damping the newton steps

            delta = delta_old + mu * (delta_newton - delta_old)  # damped newton step

            # separation of the data using the new hing
            data_plus, data_minus = dataSeparation(data, delta)

            if len(data_plus['Y']) < len(data['X'][0, :]) or len(data_minus['Y']) < len(data['X'][0, :]):
                # if the number of data points is less
                in_dataset = False
            else:
                in_dataset = True

            if count >= 50:  # abort criterion the damped Newton method should not find a suitable solution
                in_dataset = False
                flag = True
                break

        if flag:
            # if the damped Newton algorithm does not find a suitable solution, the whole algorithm is aborted
            break

        non_conv, worst = conv_check_cos(delta_old, delta_newton)

        if break_var > 70:
            # cancel if the number of iterations is too high
            break

    # save the output data
    if in_dataset:
        # delete column with ones
        if all(data_plus['X'][:, 0] == 1):
            data_plus['X'] = data_plus['X'][:, range(1, len(data_plus['X'][0, :]))]
        if all(data_minus['X'][:, 0] == 1):
            data_minus['X'] = data_minus['X'][:, range(1, len(data_minus['X'][0, :]))]

        # linear regression over data in S+
        hhModel_plus = hh.HHmodel(data_plus, normalize=False)
        hhModel_plus.hingingHyperplaneModel(numOfHinges=0)
        Vn_plus = hhModel_plus.Vn
        sum_err_plus = hhModel_plus.sum_err

        # linear regression over data in S-
        hhModel_minus = hh.HHmodel(data_minus, normalize=False)
        hhModel_minus.hingingHyperplaneModel(numOfHinges=0)
        Vn_minus = hhModel_minus.Vn
        sum_err_minus = hhModel_minus.sum_err

        theta_plus = hhModel_plus.theta
        theta_minus = hhModel_minus.theta

        sign_plus = 1
        sign_minus = -1

        output = {'theta_plus': theta_plus, 'theta_minus': theta_minus, 'sign_plus': sign_plus,
                  'sign_minus': sign_minus, 'delta': delta, 'Vn_plus': Vn_plus, 'Vn_minus': Vn_minus,
                  'sum_err_plus': sum_err_plus, 'sum_err_minus': sum_err_minus,
                  'data_plus': data_plus, 'data_minus': data_minus, 'in_dataset': in_dataset}

    else:
        # delete column with ones
        if all(data['X'][:, 0] == 1):
            data['X'] = data['X'][:, range(1, len(data['X'][0, :]))]

        hhModel = hh.HHmodel(data, normalize=False)
        hhModel.hingingHyperplaneModel(numOfHinges=0)

        theta = hhModel.theta

        output = {'theta': theta, 'data': data, 'in_dataset': in_dataset, 'delta': delta}

    return output


def arbitraryHinge(data):
    """
    Function for determining a first hinge
    In general, a random first hinge is determined. If no random first hinge is found, the data area is divided into two
    sub data spaces with the same number of data points.
    """

    if any(data['X'][:, 0] != 1):
        data['X'] = np.column_stack((np.ones(len(data['X'][:, 0])), data['X']))

    # setup for the loop to determine random hinge
    no_suitable_hinge = True  # logical condition for the while loop
    count = 0  # variable to count the passes of the loop
    flag = 0  # flag to be set if no random first hinge can be found
    while no_suitable_hinge:  # while loop for random first hints
        # arbitrary hinge
        if count > 100:  # try determine first hinge with only positive values
            Delta = np.random.rand(len(data['X'][0, :]))

        else:  # random values with positive and negative signs
            Delta = np.random.rand(len(data['X'][0, :]))-0.5

        count = count + 1  # counting the number of passes

        if count > 200:
            # if no random first hinge can be determined, flag is set to 1 and the second method is selected
            flag = True
            break

        # separation in 2 data sets with the first random hints
        [data_plus, data_minus] = dataSeparation(data, Delta)

        if len(data_plus['Y']) >= len(data['X'][0, :]) and len(data_minus['Y']) >= len(data['X'][0, :]):
            break

    if flag:  # if no random first hinge can be found
        try:  # try catch condition in case no first hinge can be found
            # split into 2 data sets of equal size
            data_plus, data_minus, Delta = initialHinge(data)
        except:
            # Error statement for the arbitrary hinge algorithm
            raise Exception('No initial data separation was found with the current settings. Error in arbitraryHinge '
                            'in hinge_finding_algorithm.py')

    return data_plus, data_minus, Delta


def initialHinge(data):
    """
    Data split in 2 Gleichgroße Subdatenräume. Dabei wird der beste Split
    entlang der verschiedenen Dimensionen für den output verwendet.

    Parameters
    ----------
    data

    Returns
    -------

    """
    data_plus, data_minus = dataSeparation_hd(data)

    # delete column with ones
    if all(data_plus['X'][:, 0] == 1):
        data_plus['X'] = data_plus['X'][:, range(1, len(data_plus['X'][0, :]))]
    if all(data_minus['X'][:, 0] == 1):
        data_minus['X'] = data_minus['X'][:, range(1, len(data_minus['X'][0, :]))]

    # linear regression over data in S+
    hhModel_plus = hh.HHmodel(data_plus, normalize=False)
    hhModel_plus.hingingHyperplaneModel(numOfHinges=0)

    # linear regression over data in S-
    hhModel_minus = hh.HHmodel(data_minus, normalize=False)
    hhModel_minus.hingingHyperplaneModel(numOfHinges=0)

    initial_Delta = hhModel_plus.theta - hhModel_minus.theta
    dim_data = len(data['X'][0, :]) - 1

    error = [hhModel_plus.Vn + hhModel_minus.Vn]  # calculating the error of a diagonal split

    # nsp: no split possible
    nsp = [False]

    for i in range(dim_data):
        data_plus, data_minus, nsp_temp = dataSeparation_halfdim(data, i)
        nsp.append(nsp_temp)

        if not nsp[i + 1]:
            # delete column with ones
            if all(data_plus['X'][:, 0] == 1):
                data_plus['X'] = data_plus['X'][:, range(1, len(data_plus['X'][0, :]))]
            if all(data_minus['X'][:, 0] == 1):
                data_minus['X'] = data_minus['X'][:, range(1, len(data_minus['X'][0, :]))]

            # linear regression over data in S+
            hhModel_plus = hh.HHmodel(data_plus, normalize=False)
            hhModel_plus.hingingHyperplaneModel(numOfHinges=0)

            # linear regression over data in S-
            hhModel_minus = hh.HHmodel(data_minus, normalize=False)
            hhModel_minus.hingingHyperplaneModel(numOfHinges=0)

            error.append(hhModel_plus.Vn + hhModel_minus.Vn)

        else:
            error.append(1)

    # delete all values where no split is possible
    error = np.asarray(error)[~np.array(nsp)]

    pos_best_split = np.argmin(error)

    if pos_best_split == 0:
        data_plus, data_minus = dataSeparation_hd(data)  # recalculation of the split

        # delete column with ones
        if all(data_plus['X'][:, 0] == 1):
            data_plus['X'] = data_plus['X'][:, range(1, len(data_plus['X'][0, :]))]
        if all(data_minus['X'][:, 0] == 1):
            data_minus['X'] = data_minus['X'][:, range(1, len(data_minus['X'][0, :]))]

        # linear regression over data in S+
        hhModel_plus = hh.HHmodel(data_plus, normalize=False)
        hhModel_plus.hingingHyperplaneModel(numOfHinges=0)

        # linear regression over data in S-
        hhModel_minus = hh.HHmodel(data_minus, normalize=False)
        hhModel_minus.hingingHyperplaneModel(numOfHinges=0)

        initial_Delta = hhModel_plus.theta - hhModel_minus.theta

    else:
        # recalculation of the split in the dimension of the best split
        data_plus, data_minus, nsp_temp = dataSeparation_halfdim(data, pos_best_split)

        # delete column with ones
        if all(data_plus['X'][:, 0] == 1):
            data_plus['X'] = data_plus['X'][:, range(1, len(data_plus['X'][0, :]))]
        if all(data_minus['X'][:, 0] == 1):
            data_minus['X'] = data_minus['X'][:, range(1, len(data_minus['X'][0, :]))]

        # linear regression over data in S+
        hhModel_plus = hh.HHmodel(data_plus, normalize=False)
        hhModel_plus.hingingHyperplaneModel(numOfHinges=0)

        # linear regression over data in S-
        hhModel_minus = hh.HHmodel(data_minus, normalize=False)
        hhModel_minus.hingingHyperplaneModel(numOfHinges=0)

        initial_Delta = hhModel_plus.theta - hhModel_minus.theta

    return data_plus, data_minus, initial_Delta


def dataSeparation(data, Delta):
    # data['X'] is defined as x = [1, x1, x2, ...., xm] ^ T and (theta ^ + -theta ^ -) is the hinge
    if any(data['X'][:, 0] != 1):
        data['X'] = np.column_stack((np.ones(len(data['X'][:, 0])), data['X']))

    data_minus = {'X': data['X'][data['X'].dot(Delta) < 0], 'Y': data['Y'][data['X'].dot(Delta) < 0]}
    data_plus = {'X': data['X'][data['X'].dot(Delta) >= 0], 'Y': data['Y'][data['X'].dot(Delta) >= 0]}

    return data_plus, data_minus


def dataSeparation_hd(data):
    """
    Function to calculate a split diagonal to the axes of the data set divided into S+ and S-.
    The input for the function are the data points in data. In data are the
    values of data points (in data.X for input values and data.Y for
    output values).
    """
    var_number = len(data['X'][0, :])  # number of dimensions
    mess_number = len(data['X'][:, 0])  # number data points

    maximum_var = []  # vector of the maximum values of the variables in each dimension
    minimum_var = []  # vector of the minimum values of the variables in each dimension
    data_points = data['X']

    data_plus = {'X': [], 'Y': []}
    data_minus = {'X': [], 'Y': []}

    for i in range(var_number):
        maximum_var.append(np.max(data_points[:, i]))
        minimum_var.append(np.min(data_points[:, i]))

    var = np.empty((len(data_points[:, 0]), len(data_points[0, :])))
    var[:, :] = np.NaN

    iterate = True  # further iterate
    nu = 1  # variable for iterations
    flag = True  # flag to indicate if there are already more data points in data_minus than in data_plus
    const = 1  # constant for iteration
    break_var = 0  # variable to interrupt the iterations

    # In this loop an iteration is used, which converts the data into data into 2 sets of equal size.
    # In the first run nu is set ot 0.1, in the next iteration to 0.2 until there are more elements in S- than in S+.
    # If this is the case, the first value 0.x is fixed and the second decimal place is iterated first with 0.x1 and in
    # the next pass of the loop with 0.x2.
    # The process ends when about half (0.49<relative number of data points in S+ < 0.51) of the data points are in S+
    # or the maximum number of iterations was exceeded.
    while iterate:
        if flag:
            nu = nu - const
            const = const / 10

        break_var += 1
        nu += const

        for j in range(var_number):
            if maximum_var[j] == minimum_var[j]:
                # if minimum value is equal to maximum value, the value is set to 1 for all
                var[:, j] = np.ones(len(data['X'][:, j]))
                continue

            var[:, j] = (data_points[:, j] - minimum_var[j]) / (
                    maximum_var[j] - minimum_var[j])  # normalised data points

        # If the sum of the normalised variables divided by the number of variables is less than sdvssvd, the data point
        # is assigned to S-. Thus a diagonal split can be created

        # assign the data points to S- und S+
        data_minus['X'] = data['X'][np.sum(var, axis=1) / var_number < nu, :]
        data_minus['Y'] = data['Y'][np.sum(var, axis=1) / var_number < nu]

        data_plus['X'] = data['X'][np.sum(var, axis=1) / var_number > nu, :]
        data_plus['Y'] = data['Y'][np.sum(var, axis=1) / var_number > nu]

        flag = np.sum(np.ones(len(data_plus['Y']))[~np.isnan(data_plus['Y'])]) < np.sum(
            np.ones(len(data_minus['Y']))[~np.isnan(data_minus['Y'])])
        num_above = np.sum(np.ones(len(data_plus['Y']))[~np.isnan(data_plus['Y'])])
        if break_var > 50:
            break
        if 0.49 < num_above / mess_number < 0.51:
            # data are sufficiently well separated into S+ and S-
            iterate = False  # cancel condition for while loop

    return data_plus, data_minus


def dataSeparation_halfdim(data, dim):
    """
    Function to divide data along one dimension into 2 sub-data spaces of equal size.
    A split should always be found as long as there was no problem with the data.
    If all data along one dimension have the same value, no split can be found in this dimension.

    Parameters
    ----------
    data        data['X'] input data, data['Y'] output data
    dim         dimension along which the data set should be divided

    Returns
    -------
    data_plus           data points from S+
    data_minus          data points from S-
    no_split_possible   indicates whether a split was possible or not

    """

    var_number = len(data['X'][0, :])  # number of dimensions
    mess_number = len(data['X'][:, 0])  # number of data points

    maximum_var = []  # vector of the maximum values of the variables in each dimension
    minimum_var = []  # vector of the minimum values of the variables in each dimension
    data_points = data['X']

    data_plus = {'Y': [], 'X': []}
    data_minus = {'X': [], 'Y': []}

    for i in range(var_number):
        maximum_var.append(np.max(data_points[:, i]))
        minimum_var.append(np.min(data_points[:, i]))

    var = np.empty((len(data_points[:, 0]), len(data_points[0, :])))
    var[:, :] = np.NaN

    no_split_possible = False  # variable which indicates whether a split can be found
    iterate = True  # further iterate
    nu = 1  # variable for iterations
    flag = True  # flag to indicate if there are more data points in data_minus than in data_plus
    const = 1  # constant for iterations
    break_var = 0  # variable to interrupt the iterations

    # Flag indicating whether the minimum value is equal to the maximum value in the dimension.
    # If this is the case, no split can be found in this dimension.
    flag2 = 0

    # In this loop an iteration is used, which converts the data into data into 2 sets of equal size.
    # In the first run nu is set ot 0.1, in the next iteration to 0.2 until there are more elements in S- than in S+.
    # If this is the case, the first value 0.x is fixed and the second decimal place is iterated first with 0.x1 and in
    # the next pass of the loop with 0.x2.
    # The process ends when about half (0.49<relative number of data points in S+ < 0.51) of the data points are in S+
    # or the maximum number of iterations was exceeded.
    while iterate:
        if flag:
            nu = nu - const
            const = const / 10

        break_var += 1
        nu += const

        if maximum_var[dim] == minimum_var[dim]:
            # if this condition is met, the data set cannot be split in this dimension
            flag2 = True
            break

        # scaling of all data points in one dimension between 0 and 1
        var = (data_points[:, dim] - minimum_var[dim]) / (maximum_var[dim] - minimum_var[dim])

        # Split the data points along one dimension. The data points var are used which are scaled between 0 and 1.
        data_minus['X'] = data['X'][var < nu, :]
        data_minus['Y'] = data['Y'][var < nu]

        data_plus['X'] = data['X'][var > nu, :]
        data_plus['Y'] = data['Y'][var > nu]

        flag = np.sum(np.ones(len(data_plus['Y']))[~np.isnan(data_plus['Y'])]) < np.sum(
            np.ones(len(data_minus['Y']))[~np.isnan(data_minus['Y'])])
        num_above = np.sum(np.ones(len(data_plus['Y']))[~np.isnan(data_plus['Y'])])
        if break_var > 50:
            break
        if 0.49 < num_above / mess_number < 0.51:
            # data are sufficiently well separated into S+ and S-
            iterate = False  # cancel condition for while loop

    if flag2:
        # If no split can be found
        no_split_possible = True

    return data_plus, data_minus, no_split_possible


def conv_check_cos(var_alt, var_neu):
    """
    Function to check if the Hinge Finding Algorithm is converged or
    not.
    Input are the values of Delta from the previous iteration and from the
    current iteration. non_conv is a binary value that indicates whether the
    function is already Converged or not. Worst returns the value at the
    changes the most.
    Parameters
    ----------
    var_alt     values of delta (hinge) from the previous iteration
    var_neu     values of delta (hinge) from the current iteration

    Returns
    -------
    non_conv    boolean that indicates whether the function is converged or not

    """

    # calculation of the change in the individual values of Delta
    y = np.cos(np.divide((abs(var_neu) - abs(var_alt)), abs(var_alt)))

    if np.min(y) >= 0.99:
        non_conv = False  # algorithm is converged
    else:
        non_conv = True  # algorithm is not converged

    worst = np.min(y)  # output of the maximum change

    return non_conv, worst
