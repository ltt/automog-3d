# -*- coding: utf-8 -*-
"""
Created on Wed Jan 27 08:25:07 2021

@author: niels
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Jan 16 09:54:04 2021

@author: niels
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 08:37:33 2021

@author: niels
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Dec 11 10:58:32 2020

@author: niels
"""

import numpy as np
from sklearn.linear_model import LinearRegression
import pandas as pd
from sklearn.mixture import GaussianMixture
import copy

i=1 #number of iterations for Regression Clustering 
patience=10 #patience for Regression Clustering; algorithm finishes after not getting better patience times
operating_points_1=pd.read_csv(r"C:\Users\niels\Documents\LTT\RegressionClustering\GuDkomplett.CSV", delimiter=";",decimal=",")
operating_points_2=pd.read_csv(r"C:\Users\niels\Documents\LTT\RegressionClustering\operating_points_testLarge.CSV",delimiter=";",decimal=",")
operating_points_0=pd.read_csv(r"C:\Users\niels\Documents\LTT\RegressionClustering\GuDonetwo.CSV", delimiter=";",decimal=",")
operating_points_3=pd.read_csv(r"C:\Users\niels\Documents\LTT\RegressionClustering\GuD_3.CSV", delimiter=";",decimal=",")
all_components={"component_0": {"state_0": operating_points_1}, "component_1": {"state_0": operating_points_2}}
two_states={"state_0": operating_points_0, "state_1:":operating_points_3}
colors=['red', 'blue','green','black','orange','cyan']

#CombiClustering carries out given clustering method for one state of one component of all operating points an returns the operating points with new states after clustering
def CombiClustering(operating_points,component,state, method, number_of_clusters,i=10,patience=10,preformatted=False):
    new_states=list()
    new_operating_points=copy.deepcopy(operating_points)
    if method=="gm":
        new_states=GM(new_operating_points[component],state,number_of_clusters,preformatted)
    if method=="rc":
        new_states=MultiRC(new_operating_points[component],state,number_of_clusters,i,patience,preformatted)
    for i in range(len(new_states)):
        if i==0:    #replaces old state
            new_operating_points[component][state]=new_states[i]
        else:   #adds as many new states as needed
            new_operating_points[component]["state_"+str(len(operating_points[component])+i-1)]=new_states[i]
    return new_operating_points
                            
#Gaussian Mixture implementation
def GM(component,state,number_of_clusters, preformatted=False):
    x=[]
    y=[]
    if preformatted==False:
        for p in component[state]:   #get Input as x array, Ouput as y array by header
            if '(in)' in p:
                for i in component[state][p]:
                    x.append(i)
        for p in component[state]:
            if '(out)' in p:
                for i in component[state][p]:
                    y.append(i)
        if len(x)<1:    #if header is misspelled or similar, use first column as Input and second column as Ouput data
            for i in component[state].iloc[:,0]:
                x.append(i)
        if len(y)<1:
            for i in component[state].iloc[:,1]:
                y.append(i)
    if preformatted==True:
        for i in component[state].iloc[:,0]:
            x.append(i)
        for i in component[state].iloc[:,1]:
            y.append(i)
    clusters= list()
    df_clusters=list()
    for i in range(number_of_clusters):
        clusters.append(list())
    gm = GaussianMixture(n_components=number_of_clusters)
    X=np.reshape(x,(-1,1))
    labels=gm.fit_predict(X,y)
    for i in range(len(x)):
        clusters[labels[i]].append([x[i],y[i]])
    for cluster in clusters:
        df=pd.DataFrame(cluster, columns=[0,1])
        df_clusters.append(df)
    return df_clusters


def MultiRC(component,state,number_of_clusters,i=50,patience=10,maxIterations=5000, preformatted=False): 
    temp_TRS=0
    TRS=0
    clusters=list()
    for j in range(i):
        temp_clusters, temp_TRS= RC(component,state, number_of_clusters,patience,maxIterations,preformatted)
        if temp_TRS>TRS:
            TRS=temp_TRS
            clusters=temp_clusters
        print("Iteration",j," completed")
    print("Clustering completed")
    return clusters

def RC (component, state, number_of_clusters,patience=10,maxIterations=5000, preformatted=False):
    x=[]
    y=[]
    if preformatted==False:
        for p in component[state]:   #get Input as x array, Ouput as y array by header
            if '(in)' in p:
                for i in component[state][p]:
                    x.append(i)
        for p in component:
            if '(out)' in p:
                for i in component[state][p]:
                    y.append(i)
        if len(x)<1:    #if header is misspelled or similar, use first column as Input and second column as Ouput data
            for i in component[state].iloc[:,0]:
                x.append(i)
        if len(y)<1:
            for i in component[state].iloc[:,1]:
                y.append(i)
    if preformatted==True:
        for i in component[state].iloc[:,0]:
            x.append(i)
        for i in component[state].iloc[:,1]:
            y.append(i)
    clusters= list() #list with all clusters
    df_clusters=[] #list with all clusters as pandas DataFrames
    functions= list() #list with a foundational function for each cluster
    TRS=0 #Total Regression Squared
    CTRS=0 #Compared Total Regression Squared
    patience_count=0
    patientTRS=[]
    patientClusters=[]
    #initialize clusters with random assignment of datapoints to clusters
    X = np.reshape(x,(-1,1)) #reshaping for regression function
    for i in range(number_of_clusters):
        clusters.append(list())
    for i in range(len(X)):
        clusters[np.random.randint(0,number_of_clusters)].append([X[i],y[i]])
    #initial linear regression for each cluster
    for cluster in clusters:
        cx=list()#structure all data points in a cluster as x and y lists for regression function
        cy=list()
        for i in cluster:
            cx.append(i[0])
            cy.append(i[1])
        function=LinearRegression().fit(cx,cy)
        functions.append(function)
        TRS+=function.score(cx,cy)
    for i in range(maxIterations): #repeats until either best clusters are returned or maximum Iterations are reached to prevent running endlessly
        #calculates minimal distanced cluster for each data point and assigns data point to it
        for c_idx, cluster in enumerate(clusters):
            for i in cluster:
                minDistance=(max(y)-min(y))**2+1
                bestfct=0
                for f_idx,function in enumerate(functions):
                    distance= (abs(function.predict(np.reshape(i[0],(-1,1)))-i[1]))**2
                    if distance < minDistance :
                        minDistance=distance
                        bestfct=f_idx
                if bestfct!=c_idx:
                    cluster.remove(i)
                    clusters[bestfct].append(i)
        #New regression with changed clusters
        functions.clear()
        for cluster in clusters:
            cx=list()
            cy=list()
            for i in cluster:
                cx.append(i[0])
                cy.append(i[1])
            function=LinearRegression().fit(cx,cy)
            functions.append(function)
            CTRS+=function.score(cx,cy)
        if CTRS<=TRS : #if Total Regression Squared is maximized, regression clustering is complete
            patience_count+=1
            patientTRS.append(TRS)
            patientClusters.append(clusters)
            if patience_count >= patience: #only finishes if TRS does not improve patience times
                TRS=max(patientTRS)
                clusters=patientClusters[np.argmax(patientTRS)]
                print("TRS:", TRS)
                for cluster in clusters: #Bringing clusters in pandas format
                    for i in cluster:
                        i[0]=i[0][0]
                    df=pd.DataFrame(cluster, columns=[0,1])
                    df_clusters.append(df)
                return df_clusters, TRS
        TRS=CTRS
        CTRS=0
    print("No solution found in maximum number of Iterations.")
    


new_points=CombiClustering(all_components,"component_0","state_0","gm",2,i=1,preformatted=True)
for idx, state in enumerate(all_components["component_0"]):
    print("old",state,":")
    all_components["component_0"][state].plot.scatter(0,1, c=colors[idx])
for idx, state in enumerate(new_points["component_0"]):
    print("new",state,":")
    new_points["component_0"][state].plot.scatter(0,1, c=colors[idx])