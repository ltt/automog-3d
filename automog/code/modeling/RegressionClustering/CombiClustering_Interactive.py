# -*- coding: utf-8 -*-
"""
Created on Sat Jan 16 09:54:04 2021

@author: niels
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 08:37:33 2021

@author: niels
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Dec 11 10:58:32 2020

@author: niels
"""

import numpy as np
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.mixture import GaussianMixture


i=1 #number of iterations for Regression Clustering Single Use
number_of_clusters_gm=2 #number of clusters for Gaussian Mixture Single Use
number_of_clusters_rc=2 #number of clusters for Regression Clustering Single Use
patience=10 #patience for Regression Clustering; algorithm finishes after not getting better patience times
operating_points_1=pd.read_csv(r"C:\Users\niels\Documents\LTT\RegressionClustering\GuDkomplett.CSV", delimiter=";",decimal=",")
colors=['red', 'blue','green','black','orange','cyan']

#Clustering with maximum 2 steps with conscole interaction
def CombiClustering(operating_points):
    final_clusters=[]
    clustering_wish="Empty"
    method=input("Which Clustering method would you like to use first? \nType in 'gm' for Gaussian Mixture or 'rc' for Regression Clustering.")
    if method=="gm":
        number_of_clusters_gm=int(input("How many Clusters do you want to be used for the Gaussian Mixture?"))
        gm_clusters=GM(operating_points,number_of_clusters_gm)
        for i in range(number_of_clusters_gm):
            plt.scatter(gm_clusters[i][0], gm_clusters[i][1], color=colors[i])
            plt.show()
        clustering_wish=input("Type in the colors of the clusters you want to cluster further")
        for idx,color in enumerate(colors):
            if color in clustering_wish:
                print("\nWhich Clustering method would you like to use for the",color,"Cluster?")
                method2=input("Type in 'gm' for Gaussian Mixture or 'rc' for Regression Clustering.")
                if method2=="gm":
                    number_of_clusters_gm=int(input("How many Clusters do you want to be used for the Gaussian Mixture?"))
                    gm_clusters_2=GM(gm_clusters[idx],number_of_clusters_gm, preformatted=True)
                    for gm_cluster in  gm_clusters_2:
                        final_clusters.append(gm_cluster)
                if method2=="rc":
                    number_of_clusters_rc=int(input("How many Clusters do you want to be used for Regression Clustering?"))
                    i=int(input("How many Iterations?"))
                    rc_clusters, TRS=MultiRC(gm_clusters[idx],number_of_clusters_rc,i,preformatted=True)
                    for rc_cluster in rc_clusters:
                            final_clusters.append(rc_cluster)
        for idx,color in enumerate(colors):
            if color not in clustering_wish:
                if idx in range(len(gm_clusters)):
                    final_clusters.append(gm_clusters[idx])
    if method=="rc":
        number_of_clusters_rc=int(input("How many Clusters would you like to be used for Regression Clustering?"))
        i=int(input("How many Iterations?"))
        rc_clusters, TRS=MultiRC(operating_points,number_of_clusters_rc,i)
        for i in range(number_of_clusters_rc):
            plt.scatter(rc_clusters[i][0], rc_clusters[i][1], color=colors[i])
            plt.show()
        clustering_wish=input("Type in the colors of the clusters you want to cluster further")
        for idx,color in enumerate(colors):
            if color in clustering_wish:
                print("\nWhich Clustering method would you like to use for the",color,"Cluster?")
                method2=input("Type in 'gm' for Gaussian Mixture or 'rc' for Regression Clustering.")
                if method2=="gm":
                    number_of_clusters_gm=int(input("How many Clusters do you want to be used for the Gaussian Mixture?"))
                    gm_clusters_2=GM(rc_clusters[idx],number_of_clusters_gm, preformatted=True)
                    for gm_cluster in  gm_clusters_2:
                        final_clusters.append(gm_cluster)
                if method2=="rc":
                    number_of_clusters_rc=int(input("How many Clusters do you want to be used for Regression Clustering?"))
                    i=int(input("How many Iterations?"))
                    rc_clusters, TRS=MultiRC(rc_clusters[idx],number_of_clusters_rc,i,preformatted=True)
                    for rc_cluster in rc_clusters:
                        final_clusters.append(rc_cluster)
        for idx,color in enumerate(colors):
            if color not in clustering_wish:
                if idx in range(len(rc_clusters)):
                    final_clusters.append(rc_clusters[idx])
    for i in range(len(final_clusters)):
        plt.scatter(final_clusters[i][0], final_clusters[i][1], color=colors[i])
    plt.show()
    return final_clusters
                            
#Gaussian Mixture implementation
def GM(operating_points,c, preformatted=False):
    x=[]
    y=[]
    if preformatted==False:
        for p in operating_points:   #get Input as x array, Ouput as y array by header
            if '(in)' in p:
                for i in operating_points[p]:
                    x.append(i)
        for p in operating_points:
            if '(out)' in p:
                for i in operating_points[p]:
                    y.append(i)
        if len(x)<1:    #if header is misspelled or similar, use first column as Input and second column as Ouput data
            for i in operating_points.iloc[:,0]:
                x.append(i)
        if len(y)<1:
            for i in operating_points.iloc[:,1]:
                y.append(i)
    if preformatted==True:
        for i in operating_points[0]:
            x.append(i)
        for i in operating_points[1]:
            y.append(i)
    clusters= list()
    for i in range(c):
        clusters.append(list())
        clusters[i].append([])
        clusters[i].append([])
    gm = GaussianMixture(n_components=c)
    X=np.reshape(x,(-1,1))
    labels=gm.fit_predict(X,y)
    for i in range(len(x)):
        clusters[labels[i]][0].append(x[i])
        clusters[labels[i]][1].append(y[i])
    return clusters


def MultiRC(operating_points,number_of_clusters,i=10,patience=10,maxIterations=5000, preformatted=False): 
    temp_TRS=0
    TRS=0
    clusters=list()
    for j in range(i):
        temp_clusters, temp_TRS= RC(operating_points,number_of_clusters,patience,maxIterations,preformatted)
        if temp_TRS>TRS:
            TRS=temp_TRS
            clusters=temp_clusters
        print("Iteration",j," completed")
    print("Clustering completed")
    return clusters, TRS

def RC (operating_points,number_of_clusters,patience=10,maxIterations=5000, preformatted=False):
    x=[]
    y=[]
    if preformatted==False:
        for p in operating_points:   #get Input as x array, Ouput as y array by header
            if '(in)' in p:
                for i in operating_points[p]:
                    x.append(i)
        for p in operating_points:
            if '(out)' in p:
                for i in operating_points[p]:
                    y.append(i)
        if len(x)<1:    #if header is misspelled or similar, use first column as Input and second column as Ouput data
            for i in operating_points.iloc[:,0]:
                x.append(i)
        if len(y)<1:
            for i in operating_points.iloc[:,1]:
                y.append(i)
    if preformatted==True:
        for i in operating_points[0]:
            x.append(i)
        for i in operating_points[1]:
            y.append(i)
    clusters= list() #list with all clusters
    functions= list() #list with a foundational function for each cluster
    TRS=0 #Total Regression Squared
    CTRS=0 #Compared Total Regression Squared
    patience_count=0
    patientTRS=[]
    patientClusters=[]
    #initialize clusters with random assignment of datapoints to clusters
    X = np.reshape(x,(-1,1)) #reshaping for regression function
    for i in range(number_of_clusters):
        clusters.append(list())
    for i in range(len(X)):
        clusters[np.random.randint(0,number_of_clusters)].append([X[i],y[i]])
    #initial linear regression for each cluster
    for cluster in clusters:
        cx=list()#structure all data points in a cluster as x and y lists for regression function
        cy=list()
        for i in cluster:
            cx.append(i[0])
            cy.append(i[1])
        function=LinearRegression().fit(cx,cy)
        functions.append(function)
        TRS+=function.score(cx,cy)
    for i in range(maxIterations): #repeats until either best clusters are returned or maximum Iterations are reached to prevent running endlessly
        #calculates minimal distanced cluster for each data point and assigns data point to it
        for c_idx, cluster in enumerate(clusters):
            for i in cluster:
                minDistance=(max(y)-min(y))**2+1
                bestfct=0
                for f_idx,function in enumerate(functions):
                    distance= (abs(function.predict(np.reshape(i[0],(-1,1)))-i[1]))**2
                    if distance < minDistance :
                        minDistance=distance
                        bestfct=f_idx
                if bestfct!=c_idx:
                    cluster.remove(i)
                    clusters[bestfct].append(i)
        #New regression with changed clusters
        functions.clear()
        for cluster in clusters:
            cx=list()
            cy=list()
            for i in cluster:
                cx.append(i[0])
                cy.append(i[1])
            function=LinearRegression().fit(cx,cy)
            functions.append(function)
            CTRS+=function.score(cx,cy)
        if CTRS<=TRS : #if Total Regression Squared is maximized, regression clustering is complete
            patience_count+=1
            patientTRS.append(TRS)
            patientClusters.append(clusters)
            if patience_count >= patience: #only finishes if TRS does not improve patience times
                TRS=max(patientTRS)
                clusters=patientClusters[np.argmax(patientTRS)]
                print("TRS:", TRS)
                for cluster in clusters: #Bringing clusters in proper format
                    X_Values=[]
                    Y_Values=[]
                    for i in cluster:
                        X_Values.append(i[0][0])
                        Y_Values.append(i[1])
                    cluster.clear()
                    cluster.append(X_Values)
                    cluster.append(Y_Values)
                return clusters, TRS
        TRS=CTRS
        CTRS=0
    print("No solution found in maximum number of Iterations.")


rc_clusters=CombiClustering(operating_points_1)
