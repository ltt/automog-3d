"""
Adds necessary packages to sys.path and starts ModEst GUI.
"""
import sys
import os

# get absolute path of this __main__.py script 
here = os.path.dirname(os.path.abspath(__file__))

# delete 'gui' from sys.modules in order to have a clear reimport
if 'code' in sys.path:
    del sys.modules['gui']  
# add all needed custom packages to sys.path
else:
    sys.path.append(here + '/code')

# import and start gui
from gui import gui
gui.main()
