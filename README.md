# AutoMoG 3D - A framework for automated data-driven modeling and optimization of multi-energy systems


## Installation


### from source

1. Clone the AutoMoG repository to any directory of your computer.

2. Open a command prompt on your computer.

3. Navigate to the toplevel directory of the AutoMoG repository (where `setup.py` is located).

4. Enter the command `pip install --user -e .` to install AutoMoG including all required packages.

Note: To solve optimization problems with AutoMoG, you have to install at least one of the three solvers `gurobi`, `cplex`, `glpk`.

## Usage

`python -m automog` in any directory starts GUI. 

## Additional info

In the `info` folder, you can find a pdf that helps you with the first steps of using the GUI, and another pdf that describes the model we use for operational optimization with the GUI.

If you use this code, please consider citing:

1. Kämper, A., Leenders, L., Bahl, B., and Bardow, A. (2021). AutoMoG: Automated Data-Driven Model Generation of Multi-Energy Systems Using Piecewise-Linear Regression. Comput. Chem. Eng. 145, 107162. doi: https://doi.org/10.1016/j.compchemeng.2020.107162 

2. Kämper, A., Holtwerth, A., Leenders, L., and Bardow, A. (2021). AutoMoG 3D: Automated Data-Driven Model Generation of Multi-Energy Systems Using Hinging Hyperplanes. Front. Energ. Res. 9, 719658. doi: https://doi.org/10.3389/fenrg.2021.719658 

If you have any questions, please contact: hendrik.schricker@ltt.rwth-aachen.de

Contributors to this repository:
Kämper, Andreas; Schricker, Hendrik; Böning, David; Hayen, Niels; Holtwerth, Alexander; Bahl, Björn; Leenders, Ludger; Bardow, André

